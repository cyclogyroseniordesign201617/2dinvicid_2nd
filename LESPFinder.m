function [LESP,LESP_dif] = LESPFinder(blade)
%Finds the LESP value of a given SingleBlade
%Type checking
if ~isa(blade,'SingleBlade')
    error('Input a SingleBlade');
end

%Initialize
LESP = 0;

%Find downwash from bound vortecies at sample locations
vel = blade.VorVelocity(false,blade.sample_loc,false);

%Find local coordinate, with 0 = front of blade
%locs = localCoord(blade.piv_loc,blade.theta,blade.sample_loc);
%locs(:,1) = locs(:,1) + blade.blade_len*blade.piv*ones(blade.N_sample,1);

for ii = 1:blade.num_vor
    %For old version
    %Note, -blade.free_stream_vel(ii,1) since U is defined in negative X
    %direction
    
    %Does the above need to be changed (to +blade.free_stream....) if the
    %blade is going backwards?
    
    %Now defining U as component of free stream veloctiy in direction of
    %pivot velocity (note: not in direction of -pivot veloctiy, or the free
    %stream at the pivot, since U is defined in the direction of movement
    U = -blade.free_stream_vel(ii,:)*blade.piv_vel'/sqrt(sum(blade.piv_vel.^2));
    
    x_cur = sum(blade.del_l(1:ii)) - 0.25*blade.del_l(ii);
    
    LESP = LESP + (vel(ii,:)*blade.norm_vec'/U)*...
        blade.del_l(ii)/sin(acos(1-(2*x_cur/blade.blade_len)));
    
    %I think this one is wrong - based on taking derivative of theta = f(x)
    %equation
    %LESP = LESP + (vel(ii)*blade.norm_vec'/(-blade.free_stream_vel(ii,1)))*...
    %    blade.del_l/sqrt(1-(1-(2*locs(ii,1)/blade.blade_len) )^2);
end

LESP = -2*LESP/(pi*blade.blade_len);

LESP_dif = abs(LESP) - blade.LESP_crit;
end