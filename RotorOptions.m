classdef RotorOptions
    %This class is a container for all the runtime options for the RotorObj
    properties          %All public, so can be easily set
        num_pt_vec      %Number of points in each blade
        len_blade       %Length of each blade
        piv             %Location of pivot point on blade as a fraction of chord from LE
        wake_setting    %What wake setting to use
        LESP_crit       %Critical LESP value
        lift_method     %Method of determining lift
        del_zone_size   %Size of deletion zone around each blade
        surf_offset     %Offset for Uhlman method
        alpha_vec       %Shape parameter for sample point distribution
        beta_vec        %Shape parameter for sample point distribution
        LE_dis          %Distance factor for leading edge vortex
        TE_dis          %Distance factor for trailing edge vortex
        
        density         %Density of the fluid
        viscosity       %Viscosity of the fluid
        
        radius          %Radius of the rotor
        arm_len         %Length of the pitching arm
        blade_piv_dis   %Distance between the pivots on the blade
        ecc_len         %Eccentricity - the offset between the two arms of the pitching mechanism
        ecc_ang         %The angle of the eccentric arm
        omega           %Rotational speed of the rotor (rad/sec)
        span            %The length of the blades
        
        LESP_crit_file  %A csv file containing the LESP critical values corresponding 
    end
    
    methods
        %Constructor method - implements defaults
        function obj_out = RotorOptions(num_blades)
            %Type checking
            if ~isscalar(num_blades)
                error('Input an integer number of blades');
            end
            %Default values
            obj_out.num_pt_vec = 10*ones(1,num_blades);
            obj_out.len_blade = 1*ones(1,num_blades);
            obj_out.piv = 0.0*ones(1,num_blades);
            obj_out.wake_setting = 3;
            obj_out.LESP_crit = 0.2*ones(1,num_blades);
            obj_out.del_zone_size = 0.525*(obj_out.len_blade)./(obj_out.num_pt_vec);
            obj_out.surf_offset = 0.25*(obj_out.len_blade)./(obj_out.num_pt_vec);
            
            %Note that with alpha = beta = 1, the beta distribution
            %degenerates into the uniform (linear) distribution
            obj_out.alpha_vec = ones(1,num_blades);
            obj_out.beta_vec = ones(1,num_blades);
            
            obj_out.LE_dis = 0.1;
            obj_out.TE_dis = 0.8;
            
            obj_out.density = 1.225;        %kg/m^3, defaults to 15 deg C, sea level air
            obj_out.viscosity = 1.983e-5;   %kg/(m*sec)
            
            %Defaults for rotor sizes
            obj_out.radius = 1*ones(1,num_blades);
            %obj_out.arm_len = 1*ones(1,num_blades);
            obj_out.blade_piv_dis = 0.25*obj_out.len_blade;
            %obj_out.ecc_len = 1*ones(1,num_blades);
            %obj_out.ecc_ang = 1*ones(1,num_blades);
            %obj_out.omega = 1;
            
            obj_out.LESP_crit_file = 'LESP_crit.csv';
        end
        
        %Setter - perform type checking
        function obj = set.num_pt_vec(obj,new_num_pts)
            %Type checking
            if ~isa(obj,'RotorOptions');
                error('Input a RotorOptions Object');
            end
            
            if any(new_num_pts < 0)
                error('The number of points must be positive');
            end
            
            if any(ceil(new_num_pts)~=floor(new_num_pts))
                error('Input an integer number of points');
            end
            %obj = obj;
            obj.num_pt_vec = new_num_pts;
        end
        
        function obj = set.len_blade(obj,new_len)
            %Type checking
            if ~isa(obj,'RotorOptions');
                error('Input a RotorOptions Object');
            end
            %{
            %No size check - it should be a vector
            if (~isscalar(new_len)) || (new_len < 0)
                error('The blade length must be a positive scalar')
            end
            %}
            obj.len_blade = new_len;
        end
        
        function obj = set.piv(obj,new_piv)
            %Type checking
            if ~isa(obj,'RotorOptions');
                error('Input a RotorOptions Object');
            end
            %{
            if (~isscalar(new_piv)) || (new_piv < 0)
                error('The pivot position must be a positive scalar')
            elseif new_piv > 1
                error('The pivot is a fraction of the blade length, and so must be less than 1');
            end
            %}
            obj.piv = new_piv;
        end
        
        function obj = set.wake_setting(obj,new_wake_set)
            %Type checking
            if ~isa(obj,'RotorOptions');
                error('Input a RotorOptions Object');
            end
            
            %Giving the different options
            switch new_wake_set
                case {0,'none'}
                    obj.wake_setting = 0;
                case {1,'TE'}
                    obj.wake_setting = 1;
                case {2,'LE'}
                    obj.wake_setting = 2;
                case {3,'LESP'}
                    obj.wake_setting = 3;
                otherwise
                    error('The wake settings are TE, LE, and LESP wake');  
            end
        end
        
        function obj = set.LESP_crit(obj,new_LESP_crit)
            %Type checking
            if ~isa(obj,'RotorOptions');
                error('Input a RotorOptions Object');
            end
            %{
            if (~isscalar(new_LESP_crit)) || (new_LESP_crit < 0)
                error('The critical LESP value must be a positive scalar')
            end
            %}
            obj.LESP_crit = new_LESP_crit;
        end
        
        function obj = set.lift_method(obj,new_lift_method)
            %Type checking
            if ~isa(obj,'RotorOptions');
                error('Input a RotorOptions Object');
            end
            
            switch new_lift_method
                case 'steady_bernoulli'
                    obj.lift_method = new_lift_method;
                case 'unsteady_bernoulli'
                    obj.lift_method = new_lift_method;
                case 'uhlman'
                    obj.lift_method = new_lift_method;
                otherwise
                    error('Unexpected lift method. Use steady_bernoulli, unsteady_bernoulli, or uhlman');
            end
        end
        
        
        %===========================================================%
        function rotor_op_out = LESPFromCSV(rotor_op)
            %A function which finds the correct critical LESP value by searching a CSV
            %file containing the LESP values corresponding to Reynold's numbers
            
            %Type checking
            if ~isa(rotor_op,'RotorOptions')
                error('Input a Rotor Options object');
            end
            if isempty(rotor_op.LESP_crit_file)
                error('Need to enter a csv file');
            end
            if ~ischar(rotor_op.LESP_crit_file)
                error('The LESP_crit_file name must be given as a string');
            end
            
            rotor_op_out = rotor_op;
            
            %Find Reynold's number
            rho = rotor_op.density;
            R = mean(rotor_op.radius);
            mu = rotor_op.viscosity;
            L = mean(rotor_op.len_blade);
            
            Re = (rho*rotor_op.omega*R*L)/mu
            
            %Reading the csv file in the range, starting at index zero:
            read_range = [1, 0, inf, 1];
            LESP_data = csvread(rotor_op.LESP_crit_file,read_range(1),read_range(2));
            
            %Could use cubic spline, but pchip seems better for
            %non-oscillatory data.
            %May need to use a polynomial fit if least-squares is needed
            %LESP_crit_val = spline(LESP_data(:,1),LESP_data(:,2),Re);
            LESP_crit_val = pchip(LESP_data(:,1),LESP_data(:,2),Re)
            
            %Adding stuff to save data for display
            Re_min = min(LESP_data(:,1));
            Re_max = max(LESP_data(:,1));
            
            Re_vec = (Re_min-0.1*Re_max):(Re_max-Re_min)/100:1.1*Re_max;
            Re_vec = [Re,Re_vec];
            LESP_vec = pchip(LESP_data(:,1),LESP_data(:,2),Re_vec);
            xlswrite('LESP_curve',[Re_vec',LESP_vec']);
            
            
            num_blades = length(rotor_op.LESP_crit);
            rotor_op_out.LESP_crit = LESP_crit_val*ones(1,num_blades);
        
        end     %End LESPFromCSV
        
        
        
    end         %End methods
end
        
                
                