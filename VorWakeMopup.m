function [new_num_wake_vor,new_wake_vor_loc,new_wake_vor_strength] = ...
    VorWakeMopup(wake_vor_loc,old_wake_vor_loc,wake_vor_strength,end_pts,num_blade,num_wake_vor)

%From old code - I think it can be removed now
%Removes any wake vortecies which move through the wing surface

%Unless some are removed, the wake vortecies remain the same
new_num_wake_vor = num_wake_vor;
new_wake_vor_loc = wake_vor_loc;
new_wake_vor_strength = wake_vor_strength;

%sets up a vector of indecies of the intersecting vortecies
intersecting = [];
%loop through the blades
for ii = 1:num_blade
    s = end_pts(2*ii,:) - end_pts(2*ii-1,:);
    %loops through the wake vortecies - find index of intersecting
    %vortecies
    for kk = 1:num_wake_vor
        %Finds intersections - based on https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect#565282
        %Setting up vectors
        r = wake_vor_loc(kk,:) - old_wake_vor_loc(kk,:);
        
        r_x_s = cross2D(r,s);
        q_minus_p = end_pts(2*ii-1,:)-old_wake_vor_loc(kk,:);
        
        %checking parallel and colinear cases
        if r_x_s == 0
            %If colinear, otherwise don't intersect
            if cross2D(q_minus_p,r) == 0
                t_0 = (q_minus_p*r')/(r*r');
                t_1 = (q_minus_p + s)*r'/(r*r');
                %checking overlap of colinear line segments
                if (t_0 >= 0) && (t_0 <= 1)
                    intersecting = [intersecting, kk];
                elseif (t_1 >= 0) && (t_1 <= 1)
                    intersecting = [intersecting, kk];
                end
            end
        %checking non-parallel cases    
        else
            %check t and u
            t_val = cross2D(q_minus_p,s)/r_x_s;
            u_val = cross2D(q_minus_p,r)/r_x_s;
            
            %if 0<= t,u <= 1, intersect, otherwise nonintersecting
            if ((t_val >= 0) && (t_val <= 1)) && ((u_val >= 0) && (u_val <= 1))
                intersecting = [intersecting, kk];
            end
        end
    end             %end going through wake vor looking for intersections
end                 %end looping through blades
    
%Remove vortecies which intersect - the vorticity will be replaced in
%the wake

intersecting = unique(intersecting);
num_remove = length(intersecting);
new_num_wake_vor = num_wake_vor - num_remove;

if num_remove ~= 0            %unless none intersect
    for jj = 1:num_remove     %go through all the intersecting ones
        remove_loc = intersecting(jj);
        %the jj's, etc, are because stuff gets shifted backwards during
        %each loop iteration
        new_wake_vor_strength(remove_loc-jj+1) = 0;
        new_wake_vor_loc(remove_loc-jj+1,:) = zeros(1,2);
        
        %Shift stuff back
        new_wake_vor_strength((remove_loc-jj+1):end-1) = new_wake_vor_strength((remove_loc-jj+2):end);
        %put in a zero to replace the end vortex
        new_wake_vor_strength(end) = 0;
        
        new_wake_vor_loc(((remove_loc-jj+1):end-1),:) = new_wake_vor_loc(((remove_loc-jj+2):end),:);
        new_wake_vor_loc(end,:) = zeros(1,2);
    end
end             %end removing intersections

end             %end function     