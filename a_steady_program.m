clear; clc; close all;
%A main program to run the steady-state case

%First, setup rotor position
num_blade = 1;
time_init = 0;
num_pts = 10;
piv = 0.5;
len_blade = 1;
num_wake = 0;           %Steady - no wake
lift_method = 'steady_bernoulli';
deletion_zone_size = 0.5*(len_blade/num_pts);

final_angle = 30;

%For display
flow_axis = [-1,1,-1,1];
c_p_axis = [0,1,-2,2];
name = 'first_steady_test';

%Initializing everything
pivLoc = @(time) [-time/10+1, -0*0.019*len_blade*sin(16*time)]; %so it doesn't move so far
pivVel = @(time) [-1, -0*0.019*16*len_blade*cos(16*time)];

%Note: the steady state only needs to run once to get data. Therefore,
%using time to parameterize the angle, to get lift at multiple angles
thetaFun = @(time) [-time*ones(num_blade,1)];
thetaDotFun = @(time) 0*(180/pi)*ones(num_blade,1);

rot_pos = RotorPosition(num_blade,time_init,pivLoc,pivVel,thetaFun,thetaDotFun);
rotor = RotorObj(num_pts,len_blade,piv,rot_pos,num_wake);

%Need a wake, but it doesn't have anything in it
wake = VortexVec([0,0],0,true,1);
old_wake = wake;

plot_obj = DispObj(rotor,wake,flow_axis,c_p_axis,name,final_angle);

num_quiv = 15;
quiv_pts = zeros(num_quiv^2,2);
x_size = flow_axis(2) - flow_axis(1);
y_size = flow_axis(4) - flow_axis(3);
num_do = 1;
for ii = 1:num_quiv
    %{
    blah = SingleBlade(num_quiv,x_size,0.5,[0,(y_size/num_quiv)*(ii-1) + flow_axis(3)],[0,0],thetaFun(10),0,0);
    quiv_pts(num_do:num_do + blah.N_sample - 1,:) = blah.sample_loc;
    num_do = num_do + blah.N_sample;
    %}
    for jj = 1:num_quiv
        
        if false && ((ii == 5) && (jj == 9))
            quiv_pts(ii*num_quiv + jj,:) = [0,0];
        else
            quiv_pts(ii*num_quiv + jj,:) = [(x_size/num_quiv)*(ii-1) + flow_axis(1), (y_size/num_quiv)*(jj-1) + flow_axis(3)];
        end
        
        quiv_pts(ii*num_quiv + jj,:) = [(x_size/num_quiv)*(ii-1) + flow_axis(1), (y_size/num_quiv)*(jj-1) + flow_axis(3)];
    end
end

%Looping through, changing angle
for new_time = 4:final_angle    %Should start at 0
    [rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,lift_method,deletion_zone_size);
    %rotor = rotor.RotorUpdate(wake,new_time,1,lift_method);
    plot_obj = plot_obj.StoreData(rotor,wake,true,new_time);
    
    
    %Getting a quiver plot
    if new_time == 10
       vel = rotor.blades{1}.VorVelocity(false, quiv_pts,false);
       %{
       for kk = 1:num_quiv^2
           vel(kk,:) = vel(kk,:) + rotor.blades{1}.free_stream_vel(1,:);
       end
       %}
       figure;
       hold on;
       %Plot the blade
       plot(rotor.blades{1}.bound_loc(:,1),rotor.blades{1}.bound_loc(:,2),'go');
       quiver(quiv_pts(:,1),quiv_pts(:,2),vel(:,1),vel(:,2));
       axis(flow_axis);
       hold off;
    end
end

%Close the movies
plot_obj.MovieClose;

%Data from PlotDigitizer, X_Ortiz_AR_9_data.csv
data = [0.124780,-0.155794;
        25.1250,0.637019;
        35.4270,0.659816;
        40.3364,0.653327;
        45.2468,0.655260;
        50.1531,0.621404;
        55.4159,0.562271;
        60.1991,0.498945;
        70.1229,0.355438;
        80.0418,0.167720;
        90.1998,-0.0221110];

    
data2 = [0.253193,0.0728033;
         2.53193,0.311297;
         4.17769,0.484519;
         6.32984,0.635146;
         7.59580,0.743096;
         8.60858,0.773222;
         10.0011,0.778243;
         11.5203,0.758159;
         12.1533,0.740586;
         14.6852,0.707950;
         16.2044,0.710460;
         18.7363,0.738075;
         21.1417,0.778243;
         23.9268,0.838494;
         25.9523,0.886192;
         28.3577,0.946443;
         30.6364,1.00920;
         35.0673,1.07197;
         39.4982,1.10460;
         44.0557,1.10962;
         48.1068,1.08452;
         53.4238,1.02678;
         58.6143,0.926360;
         61.9058,0.856067;
         65.3239,0.750628;
         69.8814,0.617573;
         74.6921,0.479498;
         79.6293,0.341423;
         84.6932,0.190795;
         90.1369,0.0527197];
%{     
%Show the lift
plot_obj = plot_obj.LiftPlot;
hold on;
ang = 0:final_angle;
ang_rad = pi*ang/180;
plot(ang, 2*pi*ang_rad,'r',data(:,1),data(:,2),'ko',data2(:,1),data2(:,2),'ks');
axis([0, 10, 0, 1]);
hold off;
legend('Steady DVM','Thin Airfoil Theory','X Ortiz AR=9 Re~10^5','NACA 0009, Re~5*10^5','Location','northwest');
ylabel('C_L');
xlabel('Angle of Attack, deg');
%}
plot_obj = plot_obj.LiftPlot;
hold on;
ang = 0:final_angle;
ang_rad = pi*ang/180;
plot(ang, 2*pi*ang_rad,'r',data2(:,1),data2(:,2),'ks');
axis([0, 10, 0, 1]);
hold off;
legend('Steady DVM','Thin Airfoil Theory','NACA 0009, Re~5*10^5','Location','northwest');
ylabel('C_L');
xlabel('Angle of Attack, deg');