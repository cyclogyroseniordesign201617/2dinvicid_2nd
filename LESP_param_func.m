function [thrust_vec,torque] = LESP_param_func(LESP_in)
%A function to test the effects of the LESP value
%First, setup rotor position
num_blade = 4;
time_init = 0;
num_pts = 10*ones(num_blade,1);
piv = 0.25*ones(num_blade,1);
len_blade = 0.05*ones(num_blade,1);
LESP_crit = LESP_in*ones(num_blade,1);
lift_method = 'unsteady_bernoulli';
deletion_zone_size = 0.05*(len_blade/num_pts)*ones(num_blade,1); %used to be 0.5*...
surf_offset = 0.25*(len_blade/num_pts)*ones(num_blade,1);

rotor_op = RotorOptions(num_blade);
%Note: I think the following are all defaults
rotor_op.num_pt_vec = num_pts;
rotor_op.len_blade = len_blade;
rotor_op.piv = piv;
rotor_op.wake_setting = 'LESP';
rotor_op.LESP_crit = LESP_crit;
rotor_op.lift_method = lift_method;
rotor_op.del_zone_size = deletion_zone_size;
rotor_op.surf_offset = surf_offset;
rotor_op.alpha_vec = 4*ones(num_blade,1);
rotor_op.beta_vec = 6*ones(num_blade,1);
rotor_op.LE_dis = 0.25*ones(num_blade,1);
rotor_op.TE_dis = 1.25*ones(num_blade,1);

rotor_op.radius = 0.1*ones(num_blade,1);
rotor_op.arm_len = 0.1028*ones(num_blade,1);
rotor_op.blade_piv_dis = 0.03*ones(num_blade,1);
rotor_op.ecc_len = 0.01981*ones(num_blade,1);
rotor_op.ecc_ang = 0.2698*ones(num_blade,1);            %radians
rotor_op.omega = 800*(2*pi/60);                         %rad/sec
rotor_op.span = 0.20;                                   %m

time_f = 3*(2*pi/rotor_op.omega);                       %about 3 revolutions
del_t = 0.0002;
num_t_step = ceil(time_f/del_t);

%For display
flow_axis = [-0.5,0.5,-0.7,0.3];
c_p_axis = [0,1,-2,2];
name = 'first_unsteady_test';

%Initializing everything

%Symmetric pitching
[pivLocFun,pivVelFun,thetaFunSym,thetaDotFunSym] = RotorBladePitchingSym(num_blade,rotor_op);
pivLoc = @(time) pivLocFun(time);
pivVel = @(time) pivVelFun(time);
thetaFun = @(time) thetaFunSym(time);
thetaDotFun = @(time) thetaDotFunSym(time);



%{
pivLoc = @(time) [-time, 0*0.019*len_blade(1)*sin(16*time)+3; -time, 0*0.019*len_blade(2)*sin(16*time)-3];
pivVel = @(time) [-1, 0*0.019*16*len_blade(1)*cos(16*time);-1, 0*0.019*16*len_blade(2)*cos(16*time)];
thetaFun = @(time) [-5;-45];
thetaDotFun = @(time) -0*ones(num_blade,1);
%}
%{
%Bad one on top
pivLoc = @(time) 4*[sin(0.25*time), -cos(0.25*time);-sin(0.25*time), cos(0.25*time)];
pivVel = @(time) 1*[cos(0.25*time), sin(0.25*time);-cos(0.25*time), -sin(0.25*time)];
thetaFun = @(time) [-25+180+0.25*(180/pi)*time; -25+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
%Bad one on bottom
pivLoc = @(time) 4*[-sin(0.25*time), cos(0.25*time); sin(0.25*time), -cos(0.25*time)];
pivVel = @(time) 1*[-cos(0.25*time), -sin(0.25*time); cos(0.25*time), sin(0.25*time)];
thetaFun = @(time) 1*[0.25*(180/pi)*time;180+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
%Start at top
pivLoc = @(time) 4*[-sin(0.25*time), cos(0.25*time)];
pivVel = @(time) 1*[-cos(0.25*time), -sin(0.25*time)];
thetaFun = @(time) [-45+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
%Start at bottom
pivLoc = @(time) 4*[sin(0.25*time), -cos(0.25*time)];
pivVel = @(time) 1*[cos(0.25*time), sin(0.25*time)];
thetaFun = @(time) [-5+180+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
pivLoc = @(time) [-time, 0];
pivVel = @(time) [-1,0];
thetaFun = @(time) [-180*time];
thetaDotFun = @(time) -180*ones(num_blade,1);
%}
%{
%Can't activate LESP this way - depends on forward motion of blade
pivLoc = @(time) [0, 0];
pivVel = @(time) [0, 0];
thetaFun = @(time) 500*[(180/pi)*time];
thetaDotFun = @(time) 500*(180/pi)*ones(num_blade,1);
%}
%{
thetaFun = @(time) -4*sin(11.62*time)*ones(num_blade,1);
thetaDotFun = @(time) -4*11.62*cos(11.62*time)*ones(num_blade,1);
%}

rot_pos = RotorPosition(num_blade,time_init,pivLoc,pivVel,thetaFun,thetaDotFun);
rotor = RotorObj(rot_pos,rotor_op);

%Initializing the wake
wake = VortexVec([0,0],0,true,1);
%Initializing old wake to the current wake
old_wake = wake;

plot_obj = DispObj(rotor,wake,flow_axis,c_p_axis,name,num_t_step,'C_p','off','air_flow','off');


%Looping through time
for new_time = 0:del_t:time_f
    [rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,rotor_op);
    old_wake = wake;
    wake = VorWakeRollup(wake,rotor,del_t);
    %bbbbbbb = wake.vor_strength
    if rotor_op.wake_setting == 3
        rotor = LastWakePosUpdater(rotor,wake);
    end
    plot_obj = plot_obj.StoreData(rotor,wake,false,new_time);
end

%Close the movies
plot_obj.MovieClose;

%Show the lift
plot_obj = LiftPlot(plot_obj,rotor_op);
t = 0:del_t:time_f;

thrust_vec = plot_obj.thrust_avg;
torque = plot_obj.torque_avg;

%Data from 
l_data = [0.0111857,0.495495;
          0.117450,0.522523;
          0.251678,0.551802;
          0.413870,0.583333;
          0.508949,0.599099;
          0.631991,0.621622;
          0.782998,0.641892;
          0.939597,0.664414;
          1.07383,0.677928;
          1.21365,0.695946;
          1.38143,0.716216;
          1.61074,0.736486;
          1.83445,0.752252;
          2.04139,0.770270;
          2.30425,0.786036;
          2.53915,0.801802;
          2.86353,0.817568;
          3.09284,0.831081;
          3.39485,0.846847;
          3.66890,0.860360;
          3.93736,0.867117;
          4.15548,0.871622;
          4.41834,0.873874;
          4.68121,0.880631;
          4.90492,0.887387;
          4.99441,0.887387];

figure;
plot(l_data(:,1),l_data(:,2),'k',t(1:25:end),plot_obj.C_L_data(1,1:25:end)/0.5476,'ko');
legend('Wagner','DVM');
xlabel('V_{inf}t/c');
ylabel('L(t)/L_{inf}');
grid on;
axis([0,5,0,1.5]);

%{
blah = plot_obj.C_L_data;
save('blah2.txt','blah','-ascii');
%}