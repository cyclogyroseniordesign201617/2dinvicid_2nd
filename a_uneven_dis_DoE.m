clear; clc;

%A main program to run through the parameter values given in a csv file and
%run the uneven_dis_param_func at the given values

param_space = csvread('LESP_DoE_1_temp.csv');

[m,~] = size(param_space)
start_spot = 1;
end_spot = m
%%
%Initialize output
res_and_param = [param_space,zeros(m,1)];

for ii = start_spot:end_spot
    res_and_param(ii,end) = uneven_dis_param_func(param_space(ii,1:end));
end
res_and_param
dlmwrite('LESP_1_res_and_param.csv',res_and_param);

%{
%%
%param_vec = [alpha,beta,LE_dis,TE_dis,del_zone_size];
param_vec = [5,5,0.25,2,0.25];
%}
%err_opt = uneven_dis_param_func(param_vec)