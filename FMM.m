function [phi_vel,phi_DIR]=FMM(vortices,targets,epsilon)
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% PURPOSE
%   this function uses a single level Fast Multipole Method to calculate
%   the stream function and velocity at target points due to given source
%   vortices
% INPUTS
%   vortices:     a N X 3 matrix where N is the number of the source
%                 vortices and the 3 columns are the location coordinates
%                 and the strength of each vortex
%   targets:      a M X 3 matrix where M is the number of the target
%                 points where stream function and velocity are to be
%                 found. The 3 columns are the location of each point
%                 and an original index of the points                                                                        %
%   epsilon:      the error bound
% OUTPUTS
%   phi_vel:      a M X 3 matrix where the 3 columns are the
%                 velocity components and point index
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Step 1: set up needed parameters
% Number of sources vortices, N.
N = length(vortices);
% Number of target points, M.
M = length(targets);
% Number of terms of multipole expansion
p = ceil(-log2(epsilon));
p_vec = (0:1:p);
% Number of boxes, N_b.
N_b = 2+sqrt(4+9*(N)/p);
%N_b=round(sqrt(N));
% Rounding the box size.
L = round(sqrt(N_b));
% Now the real number of generated boxes after rounding the box size is
% N_b = L^2;

% Create the list of interation boxes and neighboring boxes for each box
[pr,qr] = meshgrid(1:L,1:L);
pairs = [qr(:) pr(:)];

ind = cell(L,L);
flag = cell(L,L);
int_list = cell(L,L);
neigh_list = cell(L,L);
for ii = 1:L
    for jj = 1:L
        ind{ii,jj} = abs(kron([ii,jj],ones(length(pairs),1))-pairs);
        flag{ii,jj} = sum((ind{ii,jj} >= kron([2,2], ...
            ones(length(pairs),1))),2);
        % Vector containing the box number of boxes in the interaction
        % list of box{ii,jj}.
        int_list{ii,jj} = find(flag{ii,jj});
        % Vector containing the box number of near neighbors of box {ii,jj}.
        neigh_list{ii,jj} = find(flag{ii,jj}==0);
    end
end

% Sources x-position vector.
xs = vortices(:,1);
% Sources y-position vector.
ys = vortices(:,2);
% Targets x-position vector.
xt = targets(:,1);
% Targets y-position vector.
yt = targets(:,2);
% Minimum x grid point.
x_min = min([xs;xt])-0.01;
% Minimum y grid point.
y_min = min([ys;yt])-0.01;
% Maximum x grid point.
x_max = max([xs;xt])+0.01;
% Maximum y grid point.
y_max = max([ys;yt])+0.01;
% Mesh generation.
x_edg = linspace(x_min,x_max,L+1);
y_edg = linspace(y_min,y_max,L+1);
x_cen1 = zeros(1,L);
y_cen1 = zeros(1,L);

% Indexing box centers.
for ibox = 1:L
    x_cen1(ibox) = 0.5*(x_edg(ibox)+x_edg(ibox+1));
end
for ibox = 1:L
    y_cen1(ibox) = 0.5*(y_edg(ibox)+y_edg(ibox+1));
end

[x_cen2,y_cen2] = meshgrid(x_cen1,y_cen1);
xy_cen = [x_cen2(:) y_cen2(:)];

centers = cell(L,L);
count = 1;
for cc = 1:L
    for dd = 1:L
        centers{cc,dd} = xy_cen(count,1:2);
        count = count+1;
    end
end

% Indexing boxes and source positions.
[~,M_x] = histc(xs,x_edg);
[~,M_y] = histc(ys,y_edg);
vor_sort = sortrows([vortices M_x M_y],[4,5]);
boxes = cell(L,L);
for x_index = 1:L
    for y_index = 1:L
        boxes{x_index,y_index} = ...
            vor_sort(vor_sort(:,4)==x_index & vor_sort(:,5)==y_index,1:3);
    end
end
% Indexing boxet and target positions.
[~,M_x] = histc(xt,x_edg);
[~,M_y] = histc(yt,y_edg);
tar_sort = sortrows([targets M_x M_y],[4,5]);

boxet = cell(L,L);
for x_index = 1:L
    for y_index = 1:L
        boxet{x_index,y_index} = ...
            tar_sort(tar_sort(:,4)==x_index & tar_sort(:,5)==y_index,1:3);
    end
end
tic
% Step 2: Constructing the multipole expansion due to vortices in each box
%         about each box center
dist_pow = cell(L,L);
Q_v = cell(L,L);
for ind_x = 1:L
    for ind_y = 1:L
        % Distance of all sources in the box wrt the box center.
        idist = bsxfun(@minus,boxes{ind_x,ind_y}(:,1:2),...
            centers{ind_x,ind_y}(1:2));
        idist = complex(idist(:,1),idist(:,2));
        % All p-powers of "dist" times qj.
        dist_pow{ind_x,ind_y} = bsxfun(@times, kron(idist,ones(1,p+1))...
            .^kron(p_vec,ones(length(idist),1)), boxes{ind_x,ind_y}(:,3));
        % coefficients of multipole expansion wrt the box center.
        Q_v{ind_x,ind_y} = sum(dist_pow{ind_x,ind_y},1);
    end
end
% Step 3: Evaluation of the stream function and velocity using multipole
%         expansion at all target points
phi_INT = cell(L,L);
for ind_x = 1:L
    for ind_y = 1:L
        n_particles = size(boxet{ind_x,ind_y},1);
        n_int_list = size(int_list{ind_x,ind_y},1);
        phi_INT{ind_x,ind_y}=zeros(n_particles,3);
        phi_INT{ind_x,ind_y}(:,3) = boxet{ind_x,ind_y}(:,3);
        for ibox = 1:n_int_list
            idist = boxet{ind_x,ind_y}(:,1:2) - ...
                bsxfun(@times,ones(n_particles,2), ...
                centers{int_list{ind_x,ind_y}(ibox)});
            idist = complex(idist(:,1),idist(:,2));
            dist_pow = bsxfun(@power,idist,p_vec+1);
            temp = sum(kron(ones(n_particles,1),...
                Q_v{int_list{ind_x,ind_y}(ibox)})./dist_pow,2);
            phi_INT{ind_x,ind_y}(:,1) = phi_INT{ind_x,ind_y}(:,1)+ ...
                real(temp);
            phi_INT{ind_x,ind_y}(:,2) = phi_INT{ind_x,ind_y}(:,2)- ...
                imag(temp);
        end
    end
end
% The cell phi_INT contains the field induced by all sources within the
% interaction list of box containing the specific target point.
phi_NN = cell(L,L);
for ind_x=1:L
    for ind_y=1:L
        n_part = size(boxet{ind_x,ind_y},1);
        n_neigh = size(neigh_list{ind_x,ind_y},1);
        phi_NN{ind_x,ind_y} = zeros(n_part,3);
        for ibox = 1:n_neigh
            for point = 1:n_part
                idist = bsxfun(@times,...
                    ones(size(boxes{neigh_list{ind_x,ind_y}(ibox)},1),2), ...
                    boxet{ind_x,ind_y}(point,1:2))-boxes{neigh_list ...
                    {ind_x,ind_y}(ibox)}(:,1:2);
                idist = complex(idist(:,1),idist(:,2));
                velocity = boxes{neigh_list{ind_x,ind_y}(ibox)}(:,3)./idist;
                
                velocity(isinf(velocity)) = 0;
                velocity = sum(velocity,1);
                phi_NN{ind_x,ind_y}(point,1) = phi_NN{ind_x,ind_y}(point,1)+ ...
                    real(velocity);
                phi_NN{ind_x,ind_y}(point,2) = phi_NN{ind_x,ind_y}(point,2)- ...
                    imag(velocity);
            end
        end
    end
end
% The cell phi_NN contains the field induced by all sources within the
% near neighbors of box {kk,ll} containing the specific target {qq}.
phi_FMM=cell(L,L);
phi_mat = zeros(M,3);
count1 = 1;
for ind_x=1:L
    for ind_y=1:L
        phi_FMM{ind_x,ind_y}=phi_INT{ind_x,ind_y}+phi_NN{ind_x,ind_y};
        count2 = count1 + size(phi_FMM{ind_x,ind_y},1) - 1;
        phi_mat(count1:count2,1:3) = phi_FMM{ind_x,ind_y};
        count1 = count2 + 1;
    end
end
% Indexing target points.
phi_vel = sortrows(phi_mat,3);
toc

% Computation via direct sum


disp(' ')
display('Using direct summation now...');
phi_DIR = zeros(M,2);
tic
for pnt = 1:M
    dxdy = bsxfun(@times,ones(N,2),targets(pnt,1:2))-...
        vortices(:,1:2);
    pair_dist2 = complex(dxdy(:,1),dxdy(:,2));
    pair_used2 = vortices(:,3)./(pair_dist2);
    pair_used2(isinf(pair_used2)) = 0;
    pair_used2 = sum(pair_used2,1);
    phi_DIR(pnt,1) =  phi_DIR(pnt,1)+ ...
        real(pair_used2);
    phi_DIR(pnt,2) =  phi_DIR(pnt,2)- ...
        imag(pair_used2);
end

toc
% The matrix phi_DIR is the total field induced by all sources at the
% specific target using direct summations.


