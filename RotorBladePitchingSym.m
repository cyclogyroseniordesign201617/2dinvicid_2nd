function [pivLocFun,pivVelFun,thetaFun,thetaDotFun] = RotorBladePitchingSym(num_blade,rotor_options)
%A function generates function handles which give the pivot locations,
%velocities, angle, and angular velocities of the blades. Symmetrical
%pitching angle

%Spitting out the function handles
pivLocFun = @pivLocSymPitch;
pivVelFun = @pivVelSymPitch;
thetaFun = @thetaSymPitch;
thetaDotFun = @thetaDotSymPitch;


%An Internal function which calculates everything - see https://www.mathworks.com/help/matlab/matlab_prog/nested-functions.html
    function [piv_loc,piv_vel,theta_vec,theta_dot_vec] = RotorBladePitchingSymInternal(time)
        
        %Checking time
        if ~isscalar(time)
            error('The time input must be a scalar');
        end
        
        %Initialize the outputs
        piv_loc = zeros(num_blade,2);
        piv_vel = zeros(num_blade,2);
        theta_vec = zeros(num_blade,1);
        theta_dot_vec = zeros(num_blade,1);
        omega = rotor_options.omega;
        
        phi_0 = omega*time;
        
        %Defining variables in the same way as in the streamtube derivation pdf
        R = rotor_options.radius;
        l = rotor_options.arm_len;
        p = rotor_options.blade_piv_dis;
        ecc_len = rotor_options.ecc_len;
        ecc_ang = rotor_options.ecc_ang;
        
        %Iterating through blades
        for ii = 1:num_blade
            %Equally spacing out the blades
            phi = phi_0 + ( 2*pi/num_blade )*( ii - 1 );
            %Define variable a
            a = sqrt( ecc_len(ii)^2 + R(ii)^2 + 2*ecc_len(ii)*R(ii)*sin( phi + ecc_ang(ii) ) );
            
            %Find angle of blade
            theta = ( pi/2 ) - acos( ( a^2 - l(ii)^2 + p(ii)^2 )/( 2*a*p(ii) ) ) - asin( ( ecc_len(ii)/a )*cos( phi + ecc_ang(ii) ) );
            theta = -( theta - phi + ( pi/2 ) );
            %{
            %Wrap around at 2 pi
            if theta >= 2*pi
                %Wrap around positive side
                theta = theta - 2*pi;
            elseif theta <= -2*pi
                %Wrap around negative side
                theta = theta + 2*pi;
            end
            %}
            theta_vec(ii) = theta;
            
            %Find angular velocity of blade
            %Define variable c
            c = ecc_len(ii)*R(ii)*cos( phi + ecc_ang(ii) );
            
            theta_dot = ( omega*ecc_len(ii)*( a^2*sin( phi + ecc_ang(ii) ) + c*cos( phi + ecc_ang(ii) ) ) )/...
                ( ( a^3 )*sqrt( -( c/( R(ii)*a ) )^2 + 1 ) );
            
            theta_dot = theta_dot + ( omega*c*( l(ii)^2 - p(ii)^2 + a^2 ) )/...
                ( p(ii)*( a^3 )*sqrt( 4 - ( ( -l(ii)^2 + p(ii)^2 + a^2 )^2 )/( a^2*p(ii)^2 ) ) );
            
            theta_dot_vec(ii) = theta_dot - omega;
            
            %Pivot quantities
            piv_loc(ii,:) = R(ii)*[ cos(phi), sin(phi) ];
            %I think this is the correct velocity
            piv_vel(ii,:) = R(ii)*rotor_options.omega*[ -sin(phi), cos(phi) ];
        end             %End iterating through blades
    end                 %End internal calculator function
    
    
    %Adding nested functions which may be called externally
    %Location function
    function loc_vec = pivLocSymPitch(time)
      [loc_vec,~,~,~] = RotorBladePitchingSymInternal(time);
    end
    
    %Velocity function
    function vel_vec = pivVelSymPitch(time)
      [~,vel_vec,~,~] = RotorBladePitchingSymInternal(time);
    end
    
    %Theta function
    function theta_vec = thetaSymPitch(time)
      [~,~,theta_vec,~] = RotorBladePitchingSymInternal(time);
      %Convert to degrees
      theta_vec = 180*theta_vec/pi;
    end
    
    %Theta dot function
    function theta_dot_vec = thetaDotSymPitch(time)
      [~,~,~,theta_dot_vec] = RotorBladePitchingSymInternal(time);
      %Convert to degrees
      theta_dot_vec = 180*theta_dot_vec/pi;
    end

end