function [C_l_mag,C_l_phase] = TheodorsenLift(v_inf,chord,rad_freq,h_amp,alpha_amp,piv,time)

%Attempting to implement Theodorsen's function for lift. Note: much more
%reliable for pure plunging motion (alpha = 0) - that cancels out the terms
%I'm not completely sure about

%Terms I'm not sure about
%Elastic axis - from free dictionary, where load applied so that there is
%no torsion, just bending. For a flat plate, 0.5 makes sense.
a = -1 + 2*piv;
%Master's thesis says U is the horizontal kinematic velocity, which is
%seemingly separate from the free stream velocity
U = v_inf;

red_freq = rad_freq*chord/(2*v_inf);
C_theo = besselh(1,2,red_freq)/(besselh(1,2,red_freq) + 1i*besselh(0,2,red_freq));


%Plunging
h = -h_amp;
h_dot = -h_amp*rad_freq;
h_2dot = -h_amp*(rad_freq^2);

%Pitching
alpha = -alpha_amp;
alpha_dot = -alpha_amp*rad_freq;
alpha_2dot = -alpha_amp*(rad_freq^2);
%{
%Plunging
h = @(time) -h_amp*sin(rad_freq.*time);
h_dot = @(time) -h_amp*rad_freq*cos(rad_freq.*time);
h_2dot = @(time) -h_amp*(rad_freq^2)*sin(rad_freq.*time);

%Pitching
alpha = @(time) -alpha_amp*sin(rad_freq.*time);
alpha_dot = @(time) -alpha_amp*rad_freq*cos(rad_freq.*time);
alpha_2dot = @(time) -alpha_amp*(rad_freq^2)*sin(rad_freq.*time);
%}
%L = (pi*chord/2).*(h_2dot(time) + U.*alpha_dot(time) - (chord/2)*a*alpha_2dot(time)) + ...
%   2*pi*v_inf*(chord/2)*C_theo*(h_dot(time) + U*alpha(time));% + (chord/2)*(0.5 - a)*(-alpha_amp*rad_freq)

%C_l = L./(0.5*chord*v_inf^2);
%C_l = L;

C_l = pi*(h_2dot + alpha_dot - a*alpha_2dot) + 2*pi*(alpha + h_dot + alpha_dot*(0.5-a))*abs(C_theo);
%C_l = pi*(h_2dot(time) + alpha_dot(time) - a*alpha_2dot(time)) + 2*pi*(alpha(time) + h_dot(time) + alpha_dot(time)*(0.5-a))*C_theo;
C_l_mag = C_l;
C_l_phase = angle(C_theo);
end