clear; clc;
%Testing TheodorsenLift

v_inf = 1;
chord = 1;
rad_freq = 2;
piv = 0;

alpha_amp = 0;
h_amp = 0.019*chord;

time = 0:0.01:4;

[C_l,ang] = TheodorsenLift(v_inf,chord,rad_freq,h_amp,alpha_amp,piv,time)

U = v_inf;
time_nd = U*time/chord;

h = @(time) -h_amp*sin(rad_freq.*time);
figure;
subplot(2,1,1);
plot(time_nd,C_l*h_amp*sin(rad_freq.*time + ang));
grid on;
subplot(2,1,2);
plot(time_nd,h(time));
grid on;