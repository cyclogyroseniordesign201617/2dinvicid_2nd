clear; clc;
%Testing the SingleBlade class

N=3;
len=2; 
piv=0.5;
piv_loc=[0,0];
piv_vel=[0,0];
theta=0;
theta_dot=0;
num_wake = 0;

obj1 = SingleBlade(N,len,piv,piv_loc,piv_vel,theta,theta_dot,num_wake)
obj1.vor_loc

other_vor = VortexVec([1,3; 6,7],[3;8],false,0);
rhs = RHSGen(obj1,other_vor)
%{
if length(N) ~= 1
    error('The number of points should be a scalar');
end
if SameSize(N,len,piv,piv_loc,piv_vel,theta,theta_dot)
    disp('hi')
end

ang_attack = -theta;
%Determining end points of blade
x2 = -len*piv*cosd(ang_attack);
z2 =  len*piv*sind(ang_attack);

x1 =  len*(1-piv)*cosd(ang_attack);
z1 = -len*(1-piv)*sind(ang_attack);

x = linspace(x2+piv_loc(1),x1+piv_loc(1),N+1);    %vector of x loc of plate points
z = linspace(z2+piv_loc(2),z1+piv_loc(2),N+1);    %vector of z loc of plate points

dx = x(2)-x(1);             %length of segment, x
dz = z(2)-z(1);             %length of segment, z

sample_loc = [x(2:N+1)-0.25*dx; z(2:N+1)-0.25*dz]

xr = linspace(x2,x1,N+1);    %vector of relative x loc of plate points
zr = linspace(z2,z1,N+1);    %vector of relative z loc of plate points
radius = [xr(2:N+1)-0.25*dx].^2 + [zr(1:N)+0.25*dz].^2;
radius = sqrt(radius);

ii = 2;
theta_dot*radius(ii)*[-sind(theta),cosd(theta)];
%}

piv_loc1 = [1,1];
piv_vel1 = [3,4];
theta1 = 3;
theta_dot1 = 5;

obj2 = PosUpdate(obj1,piv_loc1,piv_vel1,theta1,theta_dot1)
%obj2.vor_loc
yada = obj2.wake_obj.vor_loc

blah = SingleBlade(N,len,piv,piv_loc,piv_vel,theta,theta_dot,num_wake);
blah = obj1+obj2