clear; clc; close all;
%A program to test the RotorBladePitchingSym function, especially the
%derivative of the blade angle

%Setting up the rotor
num_blade = 1;
rotor_op = RotorOptions(num_blade);
rotor_op.radius = 0.5*ones(num_blade,1);
rotor_op.arm_len = 0.5003*ones(num_blade,1);
rotor_op.blade_piv_dis = 0.0295*ones(num_blade,1);
rotor_op.ecc_len = 0.01*ones(num_blade,1);
rotor_op.ecc_ang = 0.1745*ones(num_blade,1);
rotor_op.omega = 1;

n_time = 10000;
time_tot = 10;
time = 0:time_tot/n_time:time_tot;

loc = zeros(n_time+1,2);
vel = zeros(n_time+1,2);
ang = zeros(n_time+1,1);
ang_vel = zeros(n_time+1,1);

[pivLocFun,pivVelFun,thetaFun,thetaDotFun] = RotorBladePitchingSym(num_blade,rotor_op);

for k = 1:n_time+1
    %[piv_loc,piv_vel,theta_vec,theta_dot_vec] = RotorBladePitchingSym(num_blade,rotor_op,time(k));
    loc(k,:) = pivLocFun(time(k));
    vel(k,:) = pivVelFun(time(k));
    ang(k) = thetaFun(time(k));
    ang_vel(k) = thetaDotFun(time(k));
end

num_ang_vel = (ang(2:end) - ang(1:end-1))/(time_tot/n_time);
err = ang_vel(2:end) - num_ang_vel;

figure;
plot(time,loc(:,1),time,vel(:,1),time,loc(:,2),time,vel(:,2));
legend('position x','velocity x','position y','velocity y');

figure;
plot(time,ang,time,ang_vel,time(2:end),num_ang_vel);
legend('theta','theta dot','numerical');

figure;
plot(time(2:end),err);
legend('error');

