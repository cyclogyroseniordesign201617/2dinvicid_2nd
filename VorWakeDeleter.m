function [new_num_wake_vor,new_wake_vor_loc,new_wake_vor_strength] =...
    VorWakeDeleter(num_wake_vor,wake_vor_loc,wake_vor_strength,end_pts,num_blade,piv_loc,theta,delete_range,direction)

%From old code - I think it can be removed now
%Deletes any wake vortecies within a specified region
%{
%%%%%%%%%%%%%%%%%%%%% Notes %%%%%%%%%%%%%%%%%%%
% delete_range sets the the size of the region (in local y-direction) out
of which the particles will be bounced. A vector, with a value for each blade.

% direction sets the side of the blade (relative to the normal vector the
particles will be bounced OUT of. -1 bounces out the particles if
they are on the opposite side of the blade from the normal vector, and
1 does the inverse. A vector, with a value for each blade

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}

%Initialize to original values
new_num_wake_vor = num_wake_vor;
new_wake_vor_loc = wake_vor_loc;
new_wake_vor_strength = wake_vor_strength;

%Initializing list of vortecies to delete
vor_to_delete = [];

for ii = 1:num_blade
    %Converting to local coordinates
    local_end_pts = localCoord(piv_loc(ii,:),theta(ii),end_pts((2*ii-1):2*ii,:));
    local_wake_loc = localCoord(piv_loc(ii,:),theta(ii),wake_vor_loc);
    %Should be [0,1];
    %local_norm_vec = localCoord(piv_loc(ii),theta(ii),norm_vec(ii,:));
    
    %Finding x-direction range
    x_range = sort(local_end_pts(:,1));
    
    %Finding the vortecies within the bouncing region and inverting their
    %local y-coordinate values. Otherwise, they stay as initialized
    for jj = 1:num_wake_vor
        %Checking if in range in y-direction - has to be in right
        %direction, and also within the size bound
        if (direction(ii)*local_wake_loc(jj,2) > 0) &&...
                (abs(local_wake_loc(jj,2)) < delete_range(ii))
            
            %Checking if in range in x-direction
            if (local_wake_loc(jj,1) > x_range(1)) && (local_wake_loc(jj,1) < x_range(2))
                %Within range in both x and y-directions - delete
                vor_to_delete = [vor_to_delete,jj];
            end     %end x-check
        end         %end y-check
    end             %end stepping through vortecies
end                 %end going through blades

%%%%%%%%%%%%%%%%%%%%% Deleting Stuff %%%%%%%%%%%%%%%%%%%%%%%%%%
vor_to_delete = unique(vor_to_delete);
num_remove = length(vor_to_delete);
new_num_wake_vor = num_wake_vor - num_remove;

if num_remove ~= 0            %unless none intersect
    for jj = 1:num_remove     %go through all the intersecting ones
        remove_loc = vor_to_delete(jj);
        %the jj's, etc, are because stuff gets shifted backwards during
        %each loop iteration
        new_wake_vor_strength(remove_loc-jj+1) = 0;
        new_wake_vor_loc(remove_loc-jj+1,:) = zeros(1,2);
        
        %Shift stuff back
        new_wake_vor_strength((remove_loc-jj+1):end-1) = new_wake_vor_strength((remove_loc-jj+2):end);
        %put in a zero to replace the end vortex
        new_wake_vor_strength(end) = 0;
        
        new_wake_vor_loc(((remove_loc-jj+1):end-1),:) = new_wake_vor_loc(((remove_loc-jj+2):end),:);
        new_wake_vor_loc(end,:) = zeros(1,2);
    end
end             %end removing intersections

end             %End function