classdef RotorPosition
    %This class allows for simple grouping of the stuff needed to update
    %the position of a RotorObj
    properties (SetAccess = private)
        %These following properties are at the current time
        time                    %Current time (for position properties)
        piv_locs                %num_blades x 2 matrix of locations
        piv_vels                %num_blades x 2 matrix of velocities
        theta_vec               %num_blades x 1 vector of angles between absolute and local coordinates (degrees)
        theta_dot_vec           %num_blades x 1 vector of angular velocities (deg/sec)
    end
    properties (SetAccess = immutable)      %Don't change after object is constructed
        %Number of blades in the RotorObj that this object has the position info for
        num_blades              %Immutable since the functions are
        
        %Function handles for corresponding properties
        p_fun
        p_vel_fun
        t_fun
        t_dot_fun
    end
    methods
        %Constructor
        function obj = RotorPosition(num,time_init,p_loc_fun,p_vel_fun,theta_fun,theta_dot_fun)
            
            %Error messages
            if isscalar(num) && isscalar(time_init)
                obj.num_blades = num;
                obj.time = time_init;
            else
                error('The number of blades and the initial time must be scalars')
            end
            
            %All of the inputs must be functions
            if ~ isa(p_loc_fun, 'function_handle')
                error('p_loc_fun must be a function handle');
            end
            if ~ isa(p_vel_fun, 'function_handle')
                error('p_vel_fun must be a function handle');
            end
            if ~ isa(theta_fun, 'function_handle')
                error('theta_fun must be a function handle');
            end
            if ~ isa(theta_dot_fun, 'function_handle')
                error('theta_dot_fun must be a function handle');
            end
            
            %All functions must be functions of time only
            if SameSize(nargin(p_loc_fun),nargin(p_vel_fun),nargin(theta_fun),nargin(theta_dot_fun))
                if nargin(p_loc_fun) ~= 1
                    error('All of the input functions must be functions of time only');
                end
            else
                error('All of the input functions must be functions of time only');
            end
            
            %All functions must have outputs which are the same size
            %(= num_blades)
            if SameSize((p_loc_fun(time_init)),(p_vel_fun(time_init)),(theta_fun(time_init)),(theta_dot_fun(time_init)))
                [m,~] = size(p_loc_fun(time_init));
                if m ~= num
                    error('All of the function outputs must have the same height');
                end
            else
                error('All of the function outputs must have the same height');
            end
            
            %Now, store function handles
            obj.p_fun = p_loc_fun;
            obj.p_vel_fun = p_vel_fun;
            obj.t_fun = theta_fun;
            obj.t_dot_fun = theta_dot_fun;
            
            %Initialize all the stored values
            obj = obj.NewTime(time_init);
        end         %End constructor
        
        
        
        %Updates to new time
        function obj_out = NewTime(obj,new_time)
            if isa(obj,'RotorPosition')
                if isscalar(new_time)
                    %Initialize
                    obj_out = obj;
                    
                    %Set stuff at new time
                    obj_out.time = new_time;
                    obj_out.piv_locs = obj.p_fun(new_time);
                    obj_out.piv_vels = obj.p_vel_fun(new_time);
                    obj_out.theta_vec = obj.t_fun(new_time);
                    obj_out.theta_dot_vec = obj.t_dot_fun(new_time);
                else
                    error('The time must be a scalar');
                end
            else
                error('Input a RotorPosition object');
            end         %End checking object type
        end             %End function
    end                 %End methods
end                     %End Class