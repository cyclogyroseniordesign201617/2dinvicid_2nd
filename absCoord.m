function abs_coord = absCoord(piv_loc,theta,local_coord)

%Converts from local to absolute coordinates
[N,~] = size(local_coord);

A = [cosd(theta), -sind(theta); sind(theta), cosd(theta)];
abs_coord = (A*local_coord') + [piv_loc(1)*ones(1,N); piv_loc(2)*ones(1,N)];
abs_coord = abs_coord';
end