function fit_err = uneven_dis_param_func(param_vec)

close all;
tic;
%A function to check when separation is necessary. Varies the paramerter
%values of the uneven distribution, and then determines the error from the
%NACA data.
%Note: assumes reaches steady state after 8 seconds, then averages over 
%remaining 10 seconds. Does not include hysteresis effects of separation
param_vec
%First, check if the parameters are greater than zero
if (param_vec(1) <=0) || (param_vec(2) <=0)
    fit_err = 1e6;
    return
end

%First, setup rotor position
num_blade = 1;
time_init = 0;
num_pts = 10;
piv = 0.0;
len_blade = 1;
num_wake = 3;           %Steady - no wake
LESP_crit = 0.11;
lift_method = 'unsteady_bernoulli';
deletion_zone_size = param_vec(5)*(len_blade/num_pts);
surf_offset = 0.25*(len_blade/num_pts);

rotor_op = RotorOptions(num_blade);
%Note: I think the following are all defaults
rotor_op.num_pt_vec = num_pts;
rotor_op.len_blade = len_blade;
rotor_op.piv = piv;
rotor_op.wake_setting = num_wake;
rotor_op.wake_setting = 'LESP';
rotor_op.LESP_crit = LESP_crit;
rotor_op.lift_method = lift_method;
rotor_op.del_zone_size = deletion_zone_size;
rotor_op.surf_offset = surf_offset;
rotor_op.alpha_vec = param_vec(1);
rotor_op.beta_vec = param_vec(2);
rotor_op.LE_dis = param_vec(3);
rotor_op.TE_dis = param_vec(4);

time_f = 18;
del_t = 0.01;
num_t_step = time_f/del_t;

%For display
flow_axis = [-18,2,-2,2];
c_p_axis = [0,1,-2,2];
name = 'first_unsteady_test';

%AoA step size
AoA_step = 5;

%Data storage
data_start = 8; %seconds
num_data = 90/AoA_step + 1;
%Collecting mean and standard deviation data
TE_wake_CL = zeros(3,num_data);
LE_wake_CL = zeros(3,num_data);
LESP_wake_CL = zeros(3,num_data);



%Initializing everything that doesn't change
pivLoc = @(time) [-time, 0*0.019*len_blade*sin(16*time)];
pivVel = @(time) [-1, 0*0.019*16*len_blade*cos(16*time)];
%{
thetaFun = @(time) -4*sin(11.62*time)*ones(num_blade,1);
thetaDotFun = @(time) -4*11.62*cos(11.62*time)*ones(num_blade,1);
%}

%thetaFun = @(time) -5*ones(num_blade,1);
thetaDotFun = @(time) -0*ones(num_blade,1);

%Looping through AoA
for AoA = 0:AoA_step:90
    %Initialization
    thetaFun = @(time) -AoA*ones(num_blade,1);
    rot_pos = RotorPosition(num_blade,time_init,pivLoc,pivVel,thetaFun,thetaDotFun);
    %rotor = RotorObj(num_pts,len_blade,piv,rot_pos,num_wake,LESP_crit);
    rotor = RotorObj(rot_pos,rotor_op);
    
    %Initializing the wake
    wake = VortexVec([0,0],0,true,1);
    %Initializing old wake to the current wake
    old_wake = wake;
    
    plot_obj = DispObj(rotor,wake,flow_axis,c_p_axis,name,num_t_step,'C_p','off','air_flow','off');
    
    
    %Looping through time
    for new_time = 0:del_t:time_f
        %[rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,lift_method,deletion_zone_size,surf_offset,num_wake);
        [rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,rotor_op);
        old_wake = wake;
        wake = VorWakeRollup(wake,rotor,del_t);
        if num_wake == 3
            rotor = LastWakePosUpdater(rotor,wake);
        end
        plot_obj = plot_obj.StoreData(rotor,wake,false,new_time);
    end
    
    %Close the movies
    plot_obj.MovieClose;
    
    %Show the lift
    plot_obj = plot_obj.LiftPlot;
    t = 0:del_t:time_f;
    
    
    %--------------- Collecting data ------------------%
    close all;          %All those graphs get annoying
    
    %Need to multiply by the normal vector to get lift coefficient
    data_temp = plot_obj.C_L_data((data_start/del_t):end);
    insert_spot = AoA/AoA_step + 1;
    switch num_wake
        case 1
            TE_wake_CL(1,insert_spot) = mean(data_temp);
            TE_wake_CL(2,insert_spot) = std(data_temp);
            TE_wake_CL(3,insert_spot) = median(data_temp);
        case 2
            LE_wake_CL(1,insert_spot) = mean(data_temp);
            LE_wake_CL(2,insert_spot) = std(data_temp);
        case 3
            LESP_wake_CL(1,insert_spot) = mean(data_temp);
            LESP_wake_CL(2,insert_spot) = std(data_temp);
            LESP_wake_CL(3,insert_spot) = median(data_temp);
        otherwise
            error('Need either 1 or 2 wake vortecies');
    end
end

%Data from PlotDigitizer, X_Ortiz_AR_9_data.csv
X_Ortiz_AR_9_data = [0.124780,-0.155794;
        25.1250,0.637019;
        35.4270,0.659816;
        40.3364,0.653327;
        45.2468,0.655260;
        50.1531,0.621404;
        55.4159,0.562271;
        60.1991,0.498945;
        70.1229,0.355438;
        80.0418,0.167720;
        90.1998,-0.0221110];

    
NACA_0009_data = [0.253193,0.0728033;
         2.53193,0.311297;
         4.17769,0.484519;
         6.32984,0.635146;
         7.59580,0.743096;
         8.60858,0.773222;
         10.0011,0.778243;
         11.5203,0.758159;
         12.1533,0.740586;
         14.6852,0.707950;
         16.2044,0.710460;
         18.7363,0.738075;
         21.1417,0.778243;
         23.9268,0.838494;
         25.9523,0.886192;
         28.3577,0.946443;
         30.6364,1.00920;
         35.0673,1.07197;
         39.4982,1.10460;
         44.0557,1.10962;
         48.1068,1.08452;
         53.4238,1.02678;
         58.6143,0.926360;
         61.9058,0.856067;
         65.3239,0.750628;
         69.8814,0.617573;
         74.6921,0.479498;
         79.6293,0.341423;
         84.6932,0.190795;
         90.1369,0.0527197];
     
l_data_spline_x = 0:AoA_step:90;
l_data_spline_y = spline(NACA_0009_data(:,1),NACA_0009_data(:,2),l_data_spline_x);
%{
figure;
plot(l_data(:,1),l_data(:,2),'k',t(1:25:end),plot_obj.C_L_data(1:25:end)/0.5476,'ko');
legend('Wagner','DVM');
xlabel('V_{inf}t/c');
ylabel('L(t)/L_{inf}');
grid on;
axis([0,5,0,1.5]);
%}
theta = 0:AoA_step:90;
figure;
plot(theta,TE_wake_CL(1,:),'ko',theta,TE_wake_CL(3,:),'go',theta,LESP_wake_CL(1,:),'ks',theta,LESP_wake_CL(3,:),'gs',NACA_0009_data(:,1),...
    NACA_0009_data(:,2),'r^',X_Ortiz_AR_9_data(:,1),X_Ortiz_AR_9_data(:,2),'r*',l_data_spline_x,l_data_spline_y,'b-');
legend('TE wake','TE wake median','LESP wake','LESP wake median','NACA 0009, Re~5*10^5','X Ortiz AR=9 Re~10^5','NACA cubic spline');
title('Comparison of Wake & Separation Models to Experiments');
xlabel('Angle of Attack, degrees');
ylabel('C_L');
grid on;

figure;
plot(theta,TE_wake_CL(2,:),'ko',theta,LESP_wake_CL(2,:),'ks');
legend('TE wake','LESP wake');
title('Wake Model Standard Deviation');
xlabel('Angle of Attack, degrees');
ylabel('C_L Standard Deviation');
grid on;

elapsed_time = toc
%Relative error
fit_err = sum(((LESP_wake_CL(1,:) - l_data_spline_y)./l_data_spline_y).^2)
end