classdef RotorObj
    %This class defines a rotor
    properties (SetAccess = private)
        blades              %A vector of SingleBlade's
        blades_t1           %The blades one timestep back
        blades_t2           %The blades two timesteps back
        influence           %The influence matrix of the rotor
        RHS_tot             %The total RHS for the influence matrix of the rotor
        rotor_position      %A RotorPosition object
        old_total_vel       %For the uhlman lift method
        blade_placement_vec %A vector which gives the indicies of where the blades are placed
        wake_placement_vec  %A vector which gives the indicies of where the wakes are placed
        num_bound_tot       %Total number of bound vortecies
        num_wake_tot        %Total number of wake vortecies
        
        lift_vec            %Overall thrust vector of the rotor
        torque              %Air drag torque
    end
    properties (SetAccess = private, Dependent)     %These properties are calculated on demand
        %The data in these is stored elsewhere, these are just alternative 
        %names to make stuff clearer
        num_blades          %The number of blades in the rotor
    end
    methods
        %Constructor method
        function obj = RotorObj(rot_pos_obj,rot_opt)
            %varargin is LESP_crit
            if ~ isa(rot_pos_obj,'RotorPosition');
                error('Input a RotorPosition object');
            end
            if ~isa(rot_opt,'RotorOptions')
                error('Enter a RotorOptions object');
            end
            %getting data from the rotor options object
            N_pt_vec = rot_opt.num_pt_vec;
            len_vec = rot_opt.len_blade;
            piv_vec = rot_opt.piv;
            num_wake = rot_opt.wake_setting;
            LESP_crit_vec = rot_opt.LESP_crit;
            
            obj.lift_vec = [0,0];
            obj.torque = 0;
            
            %Original inputs
            %N_blade,N_pt_vec,len_vec,piv_vec,piv_loc,piv_vel,theta,theta_dot
            
            if SameSize(N_pt_vec,len_vec,piv_vec,rot_pos_obj.theta_vec)
                %Checkin if the rotor position object is the same size as
                %the rest of the stuff
                num = length(N_pt_vec);
                N_blade = rot_pos_obj.num_blades;
                if num == N_blade
                    %Setting rotor position
                    obj.rotor_position = rot_pos_obj;
                    
                    %Initializing blades as a cell array
                    obj.blades = cell(N_blade,1);
                    
                    %Getting data from the RotorPosition object
                    piv_loc = rot_pos_obj.piv_locs;
                    piv_vel = rot_pos_obj.piv_vels;
                    theta = rot_pos_obj.theta_vec;
                    theta_dot = rot_pos_obj.theta_dot_vec;
                    
                    if num_wake == 3
                        %LESP wake
                        for ii = 1:N_blade
                            obj.blades{ii} = SingleBlade(N_pt_vec(ii),len_vec(ii),piv_vec(ii),...
                                piv_loc(ii,:),piv_vel(ii,:),theta(ii),theta_dot(ii),num_wake,...
                                rot_opt.alpha_vec(ii),rot_opt.beta_vec(ii),LESP_crit_vec(ii),rot_opt.LE_dis(ii),rot_opt.TE_dis(ii));
                        end
                    else
                        %Everything else
                        for ii = 1:N_blade
                            obj.blades{ii} = SingleBlade(N_pt_vec(ii),len_vec(ii),piv_vec(ii),...
                                piv_loc(ii,:),piv_vel(ii,:),theta(ii),theta_dot(ii),num_wake,...
                                rot_opt.alpha_vec(ii),rot_opt.beta_vec(ii));
                        end
                    end
                    
                    %Initializing the first past
                else
                    error('The number of blades must match the input data');
                end     %End right number of blades if
            else
                error('The data must have the same number of rows');
            end         %End SameSize if
        end             %End constructor
        
        
        
        
        
        
        
        %========================================================%
        %Getter for dependent variables
        function n_blade = get.num_blades(obj)
            n_blade = obj.rotor_position.num_blades;
        end
        
        
        
        
        
        %=========================================================%
        %influence generator
        function obj_out = FindInfluence(obj,del_t,num_wake)
            if ~isa(obj,'RotorObj')
                error('Only RotorObj have influence coefficients');
            end
            %Initializing output object
            obj_out = obj;
            %Initializing influence matrix to size of the sample
            %points of the (single) blade
            N_wake = obj.num_wake_tot;
            N_pts = obj.num_bound_tot;
            A = zeros(N_pts+N_wake);
            
            [~,bound_pt,sample_pt,big_norm_vec] = RotorBladeCombiner(obj);
            %sample_pt = obj.blades{1}.sample_loc;
            %bound_pt = [obj.blades{1}.bound_loc; obj.blades{1}.wake_obj.vor_loc(1:N_wake,:)];
            
            for ii = 1:N_pts                    %Collection points
                for jj = 1:N_pts+N_wake         %Bound vortex points
                    [u,w] = VorInfluence2D(sample_pt(ii,1),sample_pt(ii,2),bound_pt(jj,1),bound_pt(jj,2));
                    %Need to find the normal vector
                    A(ii,jj) = [u,w]*(big_norm_vec(ii,:))';
                end
            end
            %A = -A;            %This does flip which side messes up
            %Checks if it should add the wake vortex line
            switch num_wake
                case 0
                    %Do nothing
                case 1
                    %TE wake only
                    %Want to include all of the points in given
                    %blade
                    
                    %Iterate through blades
                    for jj = 1:obj.num_blades
                        %For TE, placing ones on wake vortex row,
                        %at the locations corresponding to the
                        %bound vortecies of that blade and also all
                        %of the wake vortecies of that blade. Note
                        %that the locations of the bound vortecies
                        %and the wake vortecies are disjoint.
                        
                        %A(obj.wake_placement_vec(jj),obj.wake_placement_vec(jj)) = 1;
                        
                        A(obj.wake_placement_vec(jj),[obj.blade_placement_vec(jj):obj.blade_placement_vec(jj+1)-1,...
                            obj.wake_placement_vec(jj):obj.wake_placement_vec(jj+1)-1]) =...
                            ones(1,obj.blades{jj}.num_vor + obj.blades{jj}.wake_obj.num_vor);
                        
                    end
                    %A(end,:) = ones(1,N_pts+N_wake);
                case 2
                    %LE and TE wake
                    warning('May not work well with non-uniform distributions');
                    %Iterate through blades
                    for jj = 1:obj.num_blades
                        %For TE
                        A(obj.wake_placement_vec(jj),[obj.blade_placement_vec(jj):obj.blade_placement_vec(jj+1)-1,...
                            obj.wake_placement_vec(jj):obj.wake_placement_vec(jj+1)-1]) =...
                            ones(1,obj.blades{jj}.num_vor + obj.blades{jj}.wake_obj.num_vor);
                        %A(end-1,:) = ones(1,N_pts+N_wake);
                        
                        %For LE
                        free_stream_mag = sqrt(sum(obj.blades{jj}.free_stream_vel(1,:).^2));
                        stren_factor = (del_t/mean(obj.blades{jj}.del_l))*free_stream_mag;
                        %LE wake vortex
                        A(obj.wake_placement_vec(jj+1)-1,obj.wake_placement_vec(jj+1)-1) = 1;
                        %Corresponding LE bound vortex
                        A(obj.wake_placement_vec(jj+1)-1,obj.blade_placement_vec(jj)) = -stren_factor;
                    end
                case 3
                    %LESP Wake
                    %Iterate through blades
                    for jj = 1:obj.num_blades
                        %TE Kutta condition as before
                        A(obj.wake_placement_vec(jj),[obj.blade_placement_vec(jj):obj.blade_placement_vec(jj+1)-1,...
                            obj.wake_placement_vec(jj):obj.wake_placement_vec(jj+1)-1]) =...
                            ones(1,obj.blades{jj}.num_vor + obj.blades{jj}.wake_obj.num_vor);
                        
                        %Note that the strength must be iteratively
                        %solved for LESP wake due to the LE strength
                        %condition, so circulation = RHS
                        A(obj.wake_placement_vec(jj+1)-1,obj.wake_placement_vec(jj+1)-1) = 1;
                    end
                otherwise
                    error('Use TE wake (1), LE and TE wake (2), or LESP wake (3)');
            end
            
            %Stores influence matrix
            obj_out.influence = A;
        end         %End FindInfluence
        
        
        
        
        %========================================================%
        %RHS generator
        %Note: assumes all RHS of individual blades updated
        function obj_out = FindTotRHS(obj,other_vor,num_wake,varargin)
            %varargin is for LESP
            %Format: varargin{1} = partial update? - true means only update
            %RHS of the wake
            
            %varargin{2} = vector of new strengths for wake
            %Type checking
            if ~isa(obj,'RotorObj')
                error('RHS_tot is a property of a RotorObj. If you want the RHS of a single blade, use RHS or RHS_wake');
            end
            %Initializing output object
            obj_out = obj;
            rhs_tot = zeros(obj.num_bound_tot + obj.num_wake_tot,1);
            
            %Iterate through the blades
            for ii = 1:obj.num_blades
                %Determine the blade RHS vectors
                if (~isempty(varargin)) && varargin{1}
                    %Partial update
                    if num_wake == 3
                        %LESP wake - only update the wake portions of the blade
                        %RHS - not anymore
                        %rhs_blade = obj.blades{ii}.RHSGen(other_vor);
                        %rhs_blade = rhs_blade.RHSWakeGen(obj.blades_t1{ii},varargin{2}(ii));
                        %This may work now
                        rhs_blade = obj.blades{ii}.RHSWakeGen(obj.blades_t1{ii},varargin{2}(ii));
                        obj_out.blades{ii} = rhs_blade;
                        
                        %Sticking into blade
                        %obj_out.RHS_tot = [rhs_blade.RHS; rhs_blade.RHS_wake];
                    else
                        %It's not LESP
                        error('Use this for LESP wake only');
                    end
                else
                    %Full update
                    %Generates RHS for bound vortecies
                    rhs_blade = obj.blades{ii}.RHSGen(other_vor);
                    rhs_blade = rhs_blade.RHSWakeGen(obj.blades_t1{ii});
                    %{
                    %Generates RHS for wake vortecies
                    if num_wake == 3
                        %LESP wake- but no partial update, so no provided LE
                        %strength
                        rhs_blade = rhs_blade.RHSWakeGen(obj.blades_t1{1});
                    else
                        rhs_blade = rhs_blade.RHSWakeGen(obj.blades_t1{1});
                    end
                    %}
                    %Sticking into blade
                    obj_out.blades{ii} = rhs_blade;
                    %obj_out.RHS_tot = [rhs_blade.RHS; rhs_blade.RHS_wake];
                end     %End generating RHS's for blade
                
                %Stick into temporary vector
                rhs_tot(obj.blade_placement_vec(ii):obj.blade_placement_vec(ii+1)-1) = rhs_blade.RHS;
                rhs_tot(obj.wake_placement_vec(ii):obj.wake_placement_vec(ii+1)-1) = rhs_blade.RHS_wake;
            end         %End iterating through blades
            
            %Stick into object
            obj_out.RHS_tot = rhs_tot;
        end             %End FindTotRHS
        
        
        
        %==========================================================%
        %Solves for bound vortex values
        %Spits out answer in a (solved) RotorObj
        %Note: assumes that the RHS and influence matrix have already been
        %generated
        function [rotor_out,other_vor_out] = SolveForBound(rotor_in,other_vor,num_wake)
            %Type checking
            if ~isa(rotor_in,'RotorObj')
                error('Input a RotorObj');
            end
            if ~isa(other_vor,'VortexVec')
                error('Input a VortexVec object');
            end
            
            %Initialize output
            rotor_out = rotor_in;
            
            rhs = rotor_in.RHS_tot;
            A = rotor_in.influence;
            
            %Special case for LESP method
            if num_wake == 3
                %In this case, we may to need to solve this
                %repeatedly, so going to LU factorize A
                [L,U,p] = lu(A,'vector');
                %bound_tot = U\(L\(rhs(p,:)));
                
                %Now check LESP condition
                first_run = true;
                [~,LESP,LE_stren_guess] = LESPMinHelpter(0,rotor_in,other_vor,L,U,p,0,first_run,false);
                
                %If no LESP conditions met, strength should be
                %zero
                LE_stren = zeros(length(LESP),1);
                if sum(LE_stren_guess) > 0
                    %If true, LESP condition met for at least
                    %one blade
                    first_run = false;
                    f = @(LE_wake_stren) LESPMinHelpter(LE_wake_stren,rotor_in,other_vor,L,U,p,LESP,first_run,true);
                    
                    %Check it, then update strengths using fminunc
                    %Add a note about using Newton root finding or
                    %something if don't have fminunc
                    options = optimoptions(@fsolve,'Display','off');
                    LE_stren = fsolve(f,LE_stren_guess,options);
                end
                [err,~,~,bound_tot] = LESPMinHelpter(LE_stren,rotor_in,other_vor,L,U,p,LESP,first_run,false);
                
                %Checking the error
                if max(err) > 1E-2
                    err
                    %This should be an error
                    warning('Did not find correct LE strengths (> 1E-2 error)');
                elseif max(err) > 1E-6
                    err
                    warning('High error in LE strengths (> 1E-6 error)');
                end
                
                %Putting in old LE strength
                for ii = 1:rotor_out.num_blades
                    rotor_out.blades{ii}.old_LE_stren = LE_stren(ii);
                end
                
            else
                %Not LESP
                bound_tot = A\rhs;
            end     %end if for LESP method
            
            %Separating out the wake vortecies
            rotor_out = RotorBoundStrengthSet(rotor_out,bound_tot);
            
            %Sticking new vortecies into wake
            %initializing new wake
            other_vor_out = other_vor;
            if num_wake == 3
                %Iterating through blades
                for kk = 1:rotor_out.num_blades
                    rotor_out.blades{kk}.last_wake = rotor_in.blades{kk}.wake_obj;
                    %LESP wake - hacky
                    if LE_stren(kk) > 1E-6
                        %LE wake has a vortex
                        other_vor_out = other_vor_out + rotor_out.blades{kk}.wake_obj;
                        num = other_vor_out.num_vor;
                        rotor_out.blades{kk}.last_wake_index = [num-1,num];
                    else
                        %No LE vortex
                        wake_temp = VortexVec(rotor_out.blades{kk}.wake_obj.vor_loc(1,:),rotor_out.blades{kk}.wake_obj.vor_strength(1),false,0);
                        other_vor_out = other_vor_out + wake_temp;
                        num = other_vor_out.num_vor;
                        rotor_out.blades{kk}.last_wake_index = [num,-1];
                    end
                end
            else
                for kk = 1:rotor_out.num_blades
                    other_vor_out = other_vor_out + rotor_out.blades{kk}.wake_obj;
                end
            end
        end             %End SolveForBound
        
        
        
        
        %=============== RotorLift ===========================%
        %Finds the lift on each blade
        function rotor_out = RotorLift(rotor_in,wake,del_t,method,surf_offset,density)
            %======== Type Checking ===================%
            if ~isa(rotor_in,'RotorObj')
                error('Enter a RotorObj to rotor_in');
            end
            if ~isa(wake,'VortexVec')
                error('Enter a VortexVec object');
            end
            if ~isscalar(del_t)
                error('Enter a scalar for del_t');
            end
            %{
            if ~isscalar(surf_offset)
                error('Enter a scalar for surf_offset');
            end
            %}
            
            %Find the size of the sample points thing I need
            num_sample_tot = 0;
            for ii = 1:rotor_in.num_blades
                num_sample_tot = num_sample_tot + rotor_in.blades{ii}.N_sample;
            end
            
            switch method
                %If the lift method is either steady or unsteady
                %bernoulli
                case {'steady_bernoulli', 'unsteady_bernoulli'}
                    %Initialize
                    rotor_out = rotor_in;
                    %Find velocity at the sample points due to the
                    %vortecies
                    sample_tot = zeros(num_sample_tot,2);
                    
                    %Now, get all the sample points
                    %all_bound_vor = VortexVec([0,0],0,true,num_sample_tot);
                    fill_spot = 1;          %Location to stick them in
                    for ii = 1:rotor_in.num_blades
                        end_spot = fill_spot + rotor_in.blades{ii}.N_sample - 1;
                        sample_tot(fill_spot:end_spot,:) = rotor_in.blades{ii}.sample_loc;
                        fill_spot = fill_spot + rotor_in.blades{ii}.N_sample;
                        
                        %all_bound_vor = all_bound_vor + rotor_in.blades{ii};
                    end
                    
                    %Find the velocity from each of the blades at all of
                    %the sample points
                    %Note that the velocity from the bound vortecies must
                    %all be evaluated separately, so that it isn't double
                    %counted in the lift program
                    vel_bound = cell(1,rotor_in.num_blades);
                    sum_vel_bound = zeros(num_sample_tot,2);
                    for ii = 1:rotor_in.num_blades
                        vel_bound{ii} = rotor_in.blades{ii}.VorVelocity(false,sample_tot,false);
                        sum_vel_bound = sum_vel_bound + vel_bound{ii};
                    end
                    
                    %Find the velocity from all of the vortecies
                    %vel = wake.VorVelocity(all_bound_vor,sample_tot,false);
                    vel = wake.VorVelocity(false,sample_tot,false);
                    
                    %Adding in all of the bound vortecies
                    vel = vel + sum_vel_bound;
                    %vel_tot = vel + obj_out.blades{1}.free_stream_vel
                    
                    %Reset rotor lift and torque
                    rotor_out.lift_vec = [0,0];
                    rotor_out.torque = 0;
                    
                    %Update the lift of all the blades
                    fill_spot = 1;          %Location to stick them in
                    for ii = 1:rotor_in.num_blades
                        end_spot = fill_spot + rotor_in.blades{ii}.N_sample - 1;
                        if isempty(rotor_in.blades_t1{ii})           %If there is nothing in the old spot
                            %Subtract out velocity due to self
                            %rotor_out.blades{ii} = BladeLift(rotor_out.blades{ii},...
                             %   (vel(fill_spot:end_spot,:)),del_t,method);
                            
                            rotor_out.blades{ii} = BladeLift(rotor_out.blades{ii},...
                                (vel(fill_spot:end_spot,:)-vel_bound{ii}(fill_spot:end_spot,:)),del_t,method,density);
                        else
                            %rotor_out.blades{ii} = BladeLift(rotor_out.blades{ii},...
                             %   (vel(fill_spot:end_spot,:)),...
                              %  del_t,method,rotor_out.blades_t1{ii});
                            
                            rotor_out.blades{ii} = BladeLift(rotor_out.blades{ii},...
                                (vel(fill_spot:end_spot,:)-vel_bound{ii}(fill_spot:end_spot,:)),...
                                del_t,method,density,rotor_out.blades_t1{ii});
                        end
                        fill_spot = fill_spot + rotor_out.blades{ii}.N_sample;
                        
                        %Add to overall lift vector
                        rotor_out.lift_vec = rotor_out.lift_vec + rotor_out.blades{ii}.lift;
                        %Note: this torque is assuming the rotor is
                        %centered at zero: T = r x F, r = piv_loc - 0
                        rotor_out.torque = rotor_out.torque + cross2D(rotor_out.blades{ii}.piv_loc,rotor_out.blades{ii}.lift);
                    end
                    
                    
                    
                    
                    
                case 'uhlman'
                    warning('May not work with non-uniform distributions');
                    warning('Almost certainly will not work with multiple blades');
                    %==== Initialization ============%
                    I = zeros(2*num_sample_tot);                %Enthalpy influence coefficients
                    vol_int = zeros(2*num_sample_tot,1);        %Volume integral
                    %visc_int = zeros(2*num_sample_tot,1);       %surface intregal - viscosity
                    temporal_int = zeros(2*num_sample_tot,1);   %surface intregal - temporal
                    u_surface = zeros(num_sample_tot,2);        %upper surface points
                    l_surface = zeros(num_sample_tot,2);
                    total_vel = zeros(2*num_sample_tot,2);      %Total velocity at each surface pt
                    C_p_vec = zeros(2*num_sample_tot,1);        %Coefficient of pressure at each surface pt
                    
                    %====== Finding offset surface points =======%
                    fill_spot = 1;
                    %Lists where it switches from one blade to the next
                    blade_locs = zeros(1,rotor_in.num_blades+1);
                    all_bound_vor = VortexVec([0,0],0,true,num_sample_tot);
                    free_stream_tot = zeros(2*num_sample_tot,2);
                    for ii = 1:rotor_in.num_blades
                        end_spot = rotor_in.blades{ii}.N_sample + fill_spot -1;
                        blade_locs(ii) = fill_spot;
                        
                        norm_temp = rotor_in.blades{ii}.norm_vec;
                        offset = ones(rotor_in.blades{ii}.N_sample,2);
                        offset = [norm_temp(1)*offset(:,1), norm_temp(2)*offset(:,2)];
                        
                        u_surface(fill_spot:end_spot,:) = rotor_in.blades{ii}.sample_loc + offset;
                        l_surface(fill_spot:end_spot,:) = rotor_in.blades{ii}.sample_loc - offset;
                        
                        all_bound_vor = all_bound_vor + rotor_in.blades{ii};
                        free_stream_tot(fill_spot:end_spot,:) = rotor_in.blades{ii}.free_stream_vel;
                        fill_spot = fill_spot + rotor_in.blades{ii}.N_sample;
                    end
                    blade_locs(end) = num_sample_tot+1;
                    free_stream_tot((num_sample_tot+1):end,:) = free_stream_tot(1:num_sample_tot,:);
                    surf_pts = [u_surface;l_surface];
                    
                    all_vor = all_bound_vor + wake;
                    %To speed things up (possibly)
                    all_vor_loc = all_vor.vor_loc;
                    all_vor_strength = all_vor.vor_strength;
                    
                    %===== Finding velocity at the surface points =====%
                    surf_vel = wake.VorVelocity(all_bound_vor,surf_pts,false);
                    
                    %========== Enthalpy Influence Matrix =======%
                    for ii = 1:2*num_sample_tot         %Steps through all the surface points
                        for jj = 1:2*num_sample_tot     %Stepping through them again
                            %Finding normal vector
                            norm_temp = zeros(1,2);
                            for kk = 1:rotor_in.num_blades
                                if (jj >= blade_locs(kk)) && (jj < blade_locs(kk+1))
                                    %upper side
                                    norm_temp = rotor_in.blades{kk}.norm_vec;
                                    del_l_temp = rotor_in.blades{kk}.del_l;
                                elseif (jj >= blade_locs(kk)+num_sample_tot) && (jj < blade_locs(kk+1)+num_sample_tot)
                                    %lower side
                                    norm_temp = -rotor_in.blades{kk}.norm_vec;
                                    del_l_temp = rotor_in.blades{kk}.del_l;
                                end
                            end
                            if norm_temp == zeros(1,2);
                                error('Did not find the correct normal vector');
                            end
                            
                            %Filling matrix
                            if ii == jj
                                I(ii,jj) = pi;
                            else
                                r_i_j = surf_pts(ii,:) - surf_pts(jj,:);
                                I(ii,jj) = -del_l_temp*(norm_temp*r_i_j')/(r_i_j*r_i_j'); %should have *del_l
                            end
                        end         %End inner surface point step-thru
                        
                        %============ Volume Intergal ==========%
                        %NOTE: ASSUMING IN EQUATION 4-7 OF MASTERS THESIS THAT |r_ij| is |r|, where
                        %r (vector) = r_surf_pt - r_vortex
                        
                        %Stepping through all vortecies
                        for ww = 1:all_vor.num_vor
                            r_surf_source = surf_pts(ii,:) - all_vor_loc(ww,:);
                            
                            total_vel(ii,:) = free_stream_tot(ii,:) + surf_vel(ii,:);
                            
                            vol_int(ii) = vol_int(ii) + ((all_vor_strength(ww)*...       %Change from + to -?, del_l is for the dS
                                cross2D((total_vel(ii,:)),-r_surf_source))/...              %Added '-' to r_surf_source
                                (r_surf_source*r_surf_source'));                         %Should be U_temp+surf_vel(ii,:)
                        end         %End stepping through vortecies
                    end             %End stepping thru surface pts.
                    
                    
                    %======== Surface Integrals =========%
                    if ~isempty(rotor_in.old_total_vel)
                        %Note that the empty case is taken care of, since
                        %it is initialize to zero
                        
                        %Time derivative of total velocity
                        total_vel_rate_change = (total_vel - rotor_in.old_total_vel)/del_t;
                        
                        
                        %Stepping through the surface points again
                        for ii = 1:2*num_sample_tot
                            for jj = 1:2*num_sample_tot
                                %Finding normal vector
                                norm_temp = zeros(1,2);
                                for kk = 1:rotor_in.num_blades
                                    if (jj >= blade_locs(kk)) && (jj < blade_locs(kk+1))
                                        %upper side
                                        norm_temp = rotor_in.blades{kk}.norm_vec;
                                        del_l_temp = rotor_in.blades{kk}.del_l;
                                    elseif (jj >= blade_locs(kk)+num_sample_tot) && (jj < blade_locs(kk+1)+num_sample_tot)
                                        %lower side
                                        norm_temp = -rotor_in.blades{kk}.norm_vec;
                                        del_l_temp = rotor_in.blades{kk}.del_l;
                                    end
                                end
                                if norm_temp == zeros(1,2);
                                    error('Did not find the correct normal vector');
                                end
                                %{
                            if jj <= num_sample_tot
                                norm_offset = 0;
                            else
                                norm_offset = num_sample_tot;
                            end
                            blade_number = floor((jj-norm_offset-1)/N) + 1;
                            norm_temp = norm_vec(blade_number,:);
                                %}
                                
                                %ASSUMING THE r's IN THE TEMPORAL INTEGRAL ARE BETWEEN SURFACE
                                %POINTS
                                if ii ~= jj
                                    r_i_j = surf_pts(ii,:) - surf_pts(jj,:);
                                    sqr_mag_r_i_j = r_i_j*r_i_j';
                                    
                                    %%%%% Temporal Intregal %%%%%%%%
                                    temporal_int(ii) = temporal_int(ii) +...
                                        (norm_temp*total_vel_rate_change(ii,:)')*del_l_temp*log(1/sqrt(sqr_mag_r_i_j));
                                    
                                    %%%%% Viscous Integral %%%%%%%%%
                                    %Not added yet, since the kinematic viscosity should be 0, and
                                    %also because the notation in the Master's thesis appears to be
                                    %messed up
                                end
                            end             %End stepping thru jj
                        end                 %end ii
                    end                     %End ifempty
                    
                    
                    %========= Solving for Enthalpy ===========%
                    %Right hand side
                    %RHS = vol_int + visc_int + temporal_int;
                    %RHS = vol_int + temporal_int;
                    RHS = vol_int;
                    
                    %Solving matrix for enthalpy
                    enthalpy = I\RHS;
                    
                    
                    %============ Finding Outputs =============%
                    %Finding pressure coefficients
                    %Stepping through surface points
                    for ii = 1:2*num_sample_tot
                        %Stagnation pressure
                        %NOTE: SHOULD THIS JUST BE THE PIVOT POINT FREE STREAM?
                        %p_stag = 0.5*density*(U_temp*U_temp');
                        %C_p_vec(ii) = density*(enthalpy(ii) - 0.5*((total_vel(ii)*total_vel(ii)') -...
                        %    (U_temp*U_temp')))/p_stag;
                        %Can remove density, since it cancels out
                        p_stag = 0.5*(free_stream_tot(ii,:)*free_stream_tot(ii,:)');
                        
                        C_p_vec(ii) = (enthalpy(ii) - 0.5*((total_vel(ii,:)*total_vel(ii,:)') -... %Should be total_vel(ii,:)*total_vel(ii,:)')
                            (free_stream_tot(ii,:)*free_stream_tot(ii,:)')))/p_stag;
                    end
                    
                    %======= Entering data into blade =======%
                    rotor_out = rotor_in;
                    rotor_out.old_total_vel = total_vel;
                    
                    for nn = 1:rotor_in.num_blades
                        C_p_upper = C_p_vec(blade_locs(nn):(blade_locs(nn+1)-1));
                        C_p_lower = C_p_vec((blade_locs(nn)+num_sample_tot):(blade_locs(nn+1)+num_sample_tot-1));
                        
                        %Note: vel and del_t inputs shouldn't matter
                        rotor_out.blades{nn} = rotor_out.blades{nn}.BladeLift([0,0],del_t,method,C_p_upper,C_p_lower);
                    end
                    
                otherwise
                    error('Unexpected lift method entered. Use steady_bernoulli, unsteady_bernoulli, or uhlman');
            end         %End of switching
        end             %End of RotorLift
        
        
        
        
        
        
        
        
        %========================================================%
        %Deletes vortecies which move through the rotor during the time 
        %step movement and into the exclusion zone
        %Note: if the deletion looks funny, try changing this to two
        %deletion steps - one after the wake rollup, another after the
        %blade movement
        function [wake_vor_out,del_index] = FlowThruDelete(rotor,new_wake,old_wake,zone_size)
            %Note: new_wake is wake after rotor position update. old_wake
            %is wake before the previous time step's wake rollup
            %del_index is the index in the wake of the deleted vortecies
            
            %Type checking - note that the other inputs are checked in the
            %SingleBlade methods
            if ~ isa(rotor,'RotorObj')
                error('The rotor input to the FlowThruDelete method of RotorObj must be a RotorObj object');
            end
            
            %Initializing vector to capture the vortecies to delete
            vor_to_delete = [];
            %Initialize del_index
            del_index = -2;
            
            %Checking if it is the initial run
            if new_wake.num_vor == 0
                initial_run = true;
            else
                initial_run = false;
            end
            
            %Stepping through all the blades
            for ii = 1:rotor.num_blades
                %Removing the ones from the exclusion zone
                vor_del_temp = VorExclusionZone(rotor.blades{ii},new_wake,zone_size);
                vor_to_delete = [vor_to_delete,vor_del_temp];
                
                %-----------------------------------------------%
                %Removing the ones that pass through the blade due to blade
                %movement and wake movement from wake rollup
                
                %------ local coordinates of wake
                
                if initial_run
                    %For initial run, there was no previous wake rollup -
                    %all movement is due to the blade moving
                    old_wake_temp = old_wake;
                else
                    %local coordinate of new wake relative to new blade
                    %position - except that it is "converted back"
                    %local_new_wake_loc = localCoord(rotor.blades{ii}.piv_loc,rotor.blades{ii}.theta,new_wake.vor_loc);
                    
                    %local coordinate of old wake relative to the old blade
                    %position
                    local_old_wake_loc = localCoord(rotor.blades_t1{ii}.piv_loc,rotor.blades_t1{ii}.theta,old_wake.vor_loc);
                    
                    %Instead of converting to a local version of blades,
                    %"convert back" the local coordinates to "absolute", taking
                    %the new blade position as the standard
                    %abs_new_wake_loc = absCoord(rotor.blades{ii}.piv_loc,rotor.blades{ii}.theta,local_new_wake_loc);
                    abs_old_wake_loc = absCoord(rotor.blades{ii}.piv_loc,rotor.blades{ii}.theta,local_old_wake_loc);
                    
                    %Makeing a temporary VortexVec with the "absolute" old wake
                    %locations
                    %Putting the vortecies in
                    old_wake_temp = VortexVec(abs_old_wake_loc((1:old_wake.num_vor),:),zeros(old_wake.num_vor,1),false,0);
                    %Making the location lists the same size
                    old_wake_temp = old_wake_temp + VortexVec([0,0],0,true,old_wake.space_size);
                end
                
                vor_del_temp = VorLineIntersect(rotor.blades{ii},new_wake,old_wake_temp);
                vor_to_delete = [vor_to_delete,vor_del_temp];
            end             %end stepping through blades
            
            if ~isempty(vor_to_delete)
                del_index = vor_to_delete;
            end
            %Deleting the vortecies
            %Note that the deletion function makes sure that no wake vortex
            %is "deleted twice"
            wake_vor_out = VorDelete(new_wake,vor_to_delete);
        end                 %end FlowThruDelete
        
        
        
        
        
        %=========================================================%
        %========= updater (with old blade storage) ==============%
        %Note: does not perform wake rollup - that is handled in the main
        %program currently - but it does delete wake vortecies
        %Also note that the newest wake vortecies are never deleted, due to
        %the position of the deletion function in the algorithm
        function [obj_out,other_vor_out] = RotorUpdate(obj,other_vor,old_other_vor,time,del_t,rot_opt)
            %Type checking
            if ~isa(obj,'RotorObj')
                error('obj input to RotorUpdate must be a RotorObj');
            end
            if ~isa(other_vor,'VortexVec')
                error('other_vor input to RotorUpdate must be a VortexVec Object');
            end
            if ~isa(old_other_vor,'VortexVec')
                error('old_other_vor input to RotorUpdate must be a VortexVec Object');
            end
            if ~isa(rot_opt, 'RotorOptions')
                error('Enter a RotorOptions object');
            end
            %Getting options from RotorOptions
            lift_method = rot_opt.lift_method;
            zone_size = rot_opt.del_zone_size;
            surf_offset = rot_opt.surf_offset;
            num_wake = rot_opt.wake_setting;
            
            %Initialize output to input, and move old blades
            obj_out = obj;
            obj_out.blades_t1 = obj.blades;
            obj_out.blades_t2 = obj.blades_t1;
            
            %Set the RotorPosition object to the correct time
            obj_out.rotor_position = obj.rotor_position.NewTime(time);
            
            %Update all the blades
            for ii = 1:obj.num_blades
                obj_out.blades{ii} = PosUpdate(obj.blades{ii},obj_out.rotor_position.piv_locs(ii,:),...
                    obj_out.rotor_position.piv_vels(ii,:),obj_out.rotor_position.theta_vec(ii),...
                    obj_out.rotor_position.theta_dot_vec(ii),num_wake,rot_opt.alpha_vec(ii),rot_opt.beta_vec(ii),rot_opt.LE_dis(ii),rot_opt.TE_dis(ii));
            end
            
            %Delete the vortecies which have moved through the
            %blade or into the exclusion zone.
            %Note: putting it here means that the newest wake
            %vortecies are never deleted
            [wake_temp,del_index] = FlowThruDelete(obj_out,other_vor,old_other_vor,zone_size);
            %wake_temp = other_vor;
            
            %Check if we need to be worried about the vortecies we deleted
            if num_wake == 3
                for ii = 1:obj_out.num_blades
                    wake_deleted = ismember(obj_out.blades{ii}.last_wake_index,del_index);
                    if sum(wake_deleted) > 0
                        %If some of the latest wake were deleted, then the
                        %last wake vortex does not exist
                        for jj = 1:length(wake_deleted)
                            if wake_deleted(jj) == 1
                                obj_out.blades{ii}.last_wake_index(jj) = -1;
                            end
                        end
                    end
                end
            end
            
            %Get the influence coefficient matrix and RHS
            obj_out = PlacementVecGen(obj_out);
            obj_out = FindInfluence(obj_out,del_t,num_wake);
            obj_out = FindTotRHS(obj_out,wake_temp,num_wake);
            
            %After this, the wake now has the LE and TE vortecies
            %in it
            [obj_out,other_vor_out] = SolveForBound(obj_out,wake_temp,num_wake);
            %Find velocity at the sample points due to the
            %vortecies
            
            obj_out = RotorLift(obj_out,other_vor_out,del_t,lift_method,surf_offset,rot_opt.density);
        end                 %End RotorUpdate
        
        
        
        
        
        %=================================================%
        %Updates the position of the last_wake object for LESP
        function rotor_out = LastWakePosUpdater(rotor_in,wake_in)
            %Updates the positions of the last_wake object in each SingleBlade for the
            %LESP LE wake method
            
            %Initialize
            rotor_out = rotor_in;
            
            for ii = 1:rotor_out.num_blades
                wake_index = rotor_out.blades{ii}.last_wake_index;
                num = length(wake_index);
                new_loc = zeros(num,2);
                new_stren = zeros(num,1);
                for jj = 1:length(wake_index)
                    %If the wake vortex exists
                    if wake_index(jj) > 0
                        new_loc(jj,:) = wake_in.vor_loc(wake_index(jj),:);
                        new_stren(jj) = wake_in.vor_strength(wake_index(jj));
                    end
                end
                
                new_last_wake = VortexVec(new_loc,new_stren,false,0);
                rotor_out.blades{ii}.last_wake = new_last_wake;
            end
        end     %End LastWakePosUpdater
        
        
        
        
        
        %======================================================%
        %A function to place bound strengths in the right positions
        function rotor_out = RotorBoundStrengthSet(rotor_in,bound_stren_raw)
            %bound_stren_raw is the raw output of solving the influence
            %matrix, and so includes the strengths of the new wake
            %vortecies.
            %Type check
            if ~isa(rotor_in, 'RotorObj')
                error('Enter a RotorObj');
            end
            %Intialize
            rotor_out = rotor_in;
            
            %Iterate through blades
            for ii = 1:rotor_in.num_blades
                rotor_out.blades{ii}.bound_strength = rotor_in.blades{ii}.VorStrengthSet(bound_stren_raw(rotor_in.blade_placement_vec(ii):rotor_in.blade_placement_vec(ii+1)-1),true,0);
                rotor_out.blades{ii}.wake_obj = rotor_out.blades{ii}.wake_obj.VorStrengthSet(bound_stren_raw(rotor_in.wake_placement_vec(ii):rotor_in.wake_placement_vec(ii+1)-1),true,0);
            end
            
        end     %End RotorBoundStrengthSet
        
        
        
        
        %======================================================%
        %A function to combine all of the bound and wake vortecies into
        %single vectors
        function [tot_stren,tot_loc,tot_sample_loc,big_norm_vec] = RotorBladeCombiner(rotor_in)
            %Type check
            if ~isa(rotor_in,'RotorObj');
                error('Input a RotorObj');
            end
            
            %Initialize
            tot_stren = zeros(rotor_in.num_bound_tot + rotor_in.num_wake_tot,1);
            tot_loc = zeros(rotor_in.num_bound_tot + rotor_in.num_wake_tot,2);
            tot_sample_loc = zeros(rotor_in.num_bound_tot,2);
            big_norm_vec = zeros(rotor_in.num_bound_tot,2);
            
            %Iterate through the blades
            for ii = 1:rotor_in.num_blades
                %Bound vortecies
                tot_stren(rotor_in.blade_placement_vec(ii):rotor_in.blade_placement_vec(ii+1)-1) = rotor_in.blades{ii}.bound_strength;
                tot_loc(rotor_in.blade_placement_vec(ii):rotor_in.blade_placement_vec(ii+1)-1,:) = rotor_in.blades{ii}.bound_loc;
                tot_sample_loc(rotor_in.blade_placement_vec(ii):rotor_in.blade_placement_vec(ii+1)-1,:) = rotor_in.blades{ii}.sample_loc;
                big_norm_vec(rotor_in.blade_placement_vec(ii):rotor_in.blade_placement_vec(ii+1)-1,:) = ones(rotor_in.blades{ii}.N_sample,1)*rotor_in.blades{ii}.norm_vec;
                
                %Wake vortecies
                tot_stren(rotor_in.wake_placement_vec(ii):rotor_in.wake_placement_vec(ii+1)-1) = rotor_in.blades{ii}.wake_obj.vor_strength;
                tot_loc(rotor_in.wake_placement_vec(ii):rotor_in.wake_placement_vec(ii+1)-1,:) = rotor_in.blades{ii}.wake_obj.vor_loc;
            end
        end     %End RotorBladeCombiner
        
        
        
        
        
        %======================================================%
        %A function to generate the placement vectors for the bound and
        %wake vortecies
        function rotor_out = PlacementVecGen(rotor_in)
            %Type check
            if ~isa(rotor_in, 'RotorObj');
                error('Input a RotorObj');
            end
            %Initialize
            rotor_out = rotor_in;
            %Note that since the vectors contain both the start and end
            %point of each spot (with the assumption that the end of
            %one is the beginning of the next), there is 1 more spot than
            %the number of blades
            bound_place = zeros(rotor_in.num_blades+1,1);
            wake_place = zeros(rotor_in.num_blades+1,1);
            bound_tot = 0;
            wake_tot = 0;
            
            %Bound vortex placement - starts at beginning
            bound_place(1) = 1;
            for ii = 1:rotor_in.num_blades
                bound_place(ii+1) = bound_place(ii) + rotor_in.blades{ii}.N_sample;
                bound_tot = bound_tot + rotor_in.blades{ii}.N_sample;
            end
            
            %wake vortecies after bound vortecies
            wake_place(1) = bound_place(end);
            for ii = 1:rotor_in.num_blades
                wake_place(ii+1) = wake_place(ii) + rotor_in.blades{ii}.wake_obj.num_vor;
                wake_tot = wake_tot + rotor_in.blades{ii}.wake_obj.num_vor;
            end
            
            rotor_out.blade_placement_vec = bound_place;
            rotor_out.wake_placement_vec = wake_place;
            
            %checking that the placement vectors are always increasing, and
            %contain no duplicates
            if bound_place ~= unique(bound_place)
                error('The bound placement vector has an error');
            end
            if wake_place ~= unique(wake_place)
                error('The wake placement vector has an error');
            end
            
            rotor_out.num_bound_tot = bound_tot;
            rotor_out.num_wake_tot = wake_tot;
            
        end     %End PlacementVecGen
    end         %End methods
end             %End class
    