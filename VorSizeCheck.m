function ok = VorSizeCheck(pos_vec,strength_vec)

%Checks to see if the sizes are OK for the position and strength vectors of
%vortecies (for error checking for method construction)

[pos_h,pos_w] = size(pos_vec);
[stren_h,stren_w] = size(strength_vec);
ok = false;

if pos_w ~= 2
    error('The position needs to be a vector of horizontal position vectors (n x 2 matrix)');
elseif stren_w ~= 1
    error('The strength needs to be a column vector');
elseif pos_h ~= stren_h
    error('There needs to be the same number of position vectors as strengths');
else
    ok = true;
end