clear; clc;
%Testing program for the RotorPosition class

len_blade = 1;
num_blade = 1;
num = 1;
pivLoc = @(time) [-time, -0*0.019*len_blade*sin(16*time)];
pivVel = @(time) [-1, -0*0.019*16*len_blade*cos(16*time)];
thetaFun = @(time) [-5*ones(num_blade,1)];
thetaDotFun = @(time) 0*(180/pi)*ones(num_blade,1);


pivLoc2 = @(time) [-time, -0*0.019*len_blade*sin(16*time);5,7];
fake_loc = 6;

obj1 = RotorPosition(num,1,pivLoc,pivVel,thetaFun,thetaDotFun)
%obj2 = RotorPosition(num,1,pivLoc2,pivVel,thetaFun,thetaDotFun)
%obj3 = RotorPosition(num,1,fake_loc,pivVel,thetaFun,thetaDotFun)

obj4 = obj1.NewTime(5)