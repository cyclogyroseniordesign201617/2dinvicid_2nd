clear; clc;
%Program to test the VortexVec features
format long

%First, test constructor
obj1 = VortexVec([0,0],0,true,5)
obj1.vor_strength
obj1.num_vor

obj2 = VortexVec([1,3; 6,7],[3;8],false,0)

obj3 = VorAdd(obj1,[1,3; 6,7],[3;8],2,true)

%These should be the same
obj4 = VorAdd(obj1,[1,3; 6,7],[3;8],2,false)
obj5 = obj1 + obj2

%Testing sameness
err_loc = obj4.vor_loc - obj5.vor_loc
err_stren = obj4.vor_strength - obj5.vor_strength

%Testing deleting vortecies
obj6 = VorDelete(obj5+obj2,[1,1,3])
obj6.vor_loc
obj6.vor_strength

%% Tests velocity
clear; clc;
format long
N_check = 4;
N_source = 2;

N = N_check + N_source;

pos_source = [0,0; 3,6];
stren_source = 2*pi*ones(N_source,1);
pos_check = [1,0; -1,0; 0,1; 0,-1];

pos = [pos_source;pos_check];
stren = [stren_source;zeros(N_check,1)];

%Finding with FMM
U = zfmm2dpart(4,N,pos',1i*stren',1,0,0);
vel = zeros(N,2);
vel(:,1) = real(U.pot/(2*pi));
vel(:,2) = -imag(U.pot/(2*pi));
vel

%Direct solver
vel_direct = zeros(N,2);
for ii = 1:N
    for jj = 1:N
        if ii ~= jj
            [u,w] = VorInfluence2D(pos(ii,1), pos(ii,2), pos(jj,1), pos(jj,2));
            vel_direct(ii,:) = vel_direct(ii,:) + stren(jj)*[u,w];
        end
    end
end
vel_direct
err1 = (vel - vel_direct)


%Finding with VortexVec
vel_obj1 = VortexVec(pos,stren,false,0)     %using self-interaction
vel_from_obj1 = vel_obj1.VorVelocity([0,0],true);

err2 = vel - vel_from_obj1

%Using given points
vel_obj2 = VortexVec(pos_source,stren_source,false,0)

%{
[num,vel_w] = size(pos_check);
obj_temp = VorAdd(vel_obj2,pos_check,zeros(num,1),num,false)

pos = obj_temp.vor_loc;
pos = pos(1:obj.num_vor,:)
%}
vel_from_obj2 = vel_obj2.VorVelocity(pos_check,false)

err3 = vel(N_source+1:end,:) - vel_from_obj2

%% Testing VorStrengthSet
clear; clc;
obj10 = VortexVec([1,3; 6,7;2,2],[3;8;5],false,0)

new_strength1 = [1,1,1];
new_strength2 = [2;2;2];
new_strength3 = [5;20];
new_stren_loc = [1;2];

%obj11 = obj10.VorStrengthSet(new_strength1,true,0);
obj11 = obj10.VorStrengthSet(new_strength2,true,0);
new_sten11 = obj11.vor_strength

%obj12 = obj10.VorStrengthSet(new_strength2,false,0)
%obj12 = obj10.VorStrengthSet(new_strength3,true,0)
obj12 = obj10.VorStrengthSet(new_strength3,false,new_stren_loc)
new_stren12 = obj12.vor_strength

