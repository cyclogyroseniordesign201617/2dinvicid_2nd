function [u,w] = VorInfluence2D(x_site,z_site,x_source,z_source)

%Determines the influence of a vortex source particle at a given site in 2D
%See 20160823142738179.pdf
%normalized strength

%Reduce influence within certain range?

x_err = x_site - x_source;
z_err = z_site - z_source;
sqr_dis = x_err^2 + z_err^2;

%Constant value within radius r = 0.0001
if sqr_dis > 1E-12
    
    v = 1/(2*pi*sqr_dis);
    u = v*z_err;
    w = -v*x_err;
else
    v = 1E12/(2*pi);
    u = v*z_err;
    w = -v*x_err;
end


end