function blah = cross2D(vec1, vec2)

%finds the "cross product" of 2D vectors

blah = vec1(1)*vec2(2) - vec1(2)*vec2(1);
end