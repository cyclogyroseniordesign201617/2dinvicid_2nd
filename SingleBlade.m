classdef SingleBlade < VortexVec & BladeLiftObj
    %This class defines sets of vortecies arranged into a single blade of
    %the cyclorotor
    properties              %Public - so can set them
        wake_obj            %Location of TE and LE vortecies - note: should this be another SingleBlade?
        last_wake           %Locations of the latest wake vortex released (for LESP)
        last_wake_index     %Index location of last_wake in wake (for LESP)
        old_LE_stren        %LE strength at last time step
    end
    properties (SetAccess = private)
        norm_vec            %The normal vector of the blade
        sample_loc          %Sample points for enforcing no flow-thru
        free_stream_vel     %Free stream velocity at sample points
        blade_len           %Length of the blade
        theta               %Angle to the absolute X axis, degrees
        piv_loc             %Location of pivot point
        piv_vel             %Velocity of pivot point
        piv                 %Fraction of blade from leading edge to pivot
        RHS                 %Vector of RHS elements corresponding to the bound vortecies of the blade
        RHS_wake            %Vector of RHS elements corresponding to the wake vortecies of the blade
        del_l               %Spacing of sample points & bound vortecies
        blade_end_pts       %Endpoints of the blade
        LESP_crit           %Critical LESP value for the blade
        
        %vor_loc - bound vortecies
        %vor_strength - bound vortex strength
    end
    properties (SetAccess = private, Dependent)     %These properties are calculated on demand
        %The data in these is stored, these are just alternative names to
        %make stuff clearer
        N_sample            %Number of sample points - combine with num_vor?
        bound_loc           %Location of bound vortecies
        tanj_vec            %Tangent vector
    end
    properties (Dependent)  %But public set access
        bound_strength      %Strength of bound vortecies
    end
    methods
        
        function obj = SingleBlade(N,len,piv,piv_loc,piv_vel,theta,theta_dot,num_wake,alpha_val,beta_val,varargin)
            %Constructor
            if length(N) ~= 1
                error('The number of points should be a scalar');
            end
            %Initialize it
            obj@VortexVec(0,0,true,N);
            if ~SameSize(N,len,piv,piv_loc,piv_vel,theta,theta_dot)
                [~,err_diag_vec] = SameSize(N,len,piv,piv_loc,piv_vel,theta,theta_dot,'diagnose')
                error('The inputs should be the same size');
            end
            
            obj.blade_len = len;
            obj.theta = theta;
            obj.piv = piv;
            obj.piv_loc = piv_loc;
            obj.piv_vel = piv_vel;
            
            %Initialize RHS to zeros - maybe don't initialize this
            %obj.RHS = zeros(N,1);
            %obj.RHS_wake = zeros(2,1);                    %Mess with this with wake
            
            ang_attack = -theta;                           %Defining theta from +X (abs) to +x (local)
            %Determining end points of blade
            x2 = -len*piv*cosd(ang_attack);
            z2 =  len*piv*sind(ang_attack);
            
            x1 =  len*(1-piv)*cosd(ang_attack);
            z1 = -len*(1-piv)*sind(ang_attack);
            
            obj.blade_end_pts = [x2,z2; x1,z1] + [piv_loc(1)*ones(2,1),piv_loc(2)*ones(2,1)];    %Defining endpoints of the blade
            
            %Beta distribution
            even_dis = 0:(1/N):1;
            uneven_dis = betainc(even_dis,alpha_val,beta_val);     %Cumulative beta distribution
            
            x = uneven_dis*len*cosd(ang_attack) + x2 + piv_loc(1);
            %errx = x(end) - (x1 + piv_loc(1))
            z = -uneven_dis*len*sind(ang_attack) + z2 + piv_loc(2);
            %errz = z(end) - (z1+ piv_loc(2))
            %x_blah = linspace(x2+piv_loc(1),x1+piv_loc(1),N+1);    %vector of x loc of plate points
            %z_blah = linspace(z2+piv_loc(2),z1+piv_loc(2),N+1);    %vector of z loc of plate points
            
            %x - x_blah
            %z - z_blah
            dx_uni = len*cosd(ang_attack)/N;                    %spacing if uniform
            dz_uni = len*sind(ang_attack)/N;
            dx = x(2:end)-x(1:end-1);                                   %length of segment, x
            dz = z(2:end)-z(1:end-1);                                   %length of segment, z
            
            obj.del_l = sqrt(dx.^2 + dz.^2);                    %sample point spacing
            
            obj.norm_vec = [sind(ang_attack),cosd(ang_attack)];      %Normal to plate (only 1 - flat plate)
            %t  = [rx,rz]/r;             %Tangent to plate
            
            %Note: dx vector has a length of N
            obj.sample_loc = [x(2:N+1)-0.25*dx; z(2:N+1)-0.25*dz]'; %location of collection points
            bound_vor = [x(1:N)+0.25*dx; z(1:N)+0.25*dz]'; %location of vortexes
            
            %figure;
            %plot(obj.sample_loc(:,1),obj.sample_loc(:,2),'k^',bound_vor(:,1),bound_vor(:,2),'k*',x,z,'k+');
            %Setting bound vortecies
            obj = VorAdd(obj,bound_vor,zeros(N,1),N,false);
            %VorAdd@VortexVec(obj,bound_vor,zeros(N,1),false,0);
            %obj = VorAdd(obj,bound_vor,zeros(N,1),false);
            
            %Finding free stream velocity
            %Finding radius
            xr = uneven_dis*len*cosd(ang_attack) + x2;    %vector of relative x loc of plate points
            zr = -uneven_dis*len*sind(ang_attack) + z2;    %vector of relative z loc of plate points
            radius = (xr(2:N+1)-0.25*dx).^2 + (zr(2:N+1)-0.25*dz).^2;
            radius = sqrt(radius);
            %Problem: need to distinguish between the points on the
            %different sides of the pivot
            free_stream = zeros(N,2);
            for ii = 1:N
                %if cosd(theta) and the x location have different signs
                if cosd(theta)*(xr(ii+1)-0.25*dx(ii)) < 0
                    radius(ii) = -radius(ii);
                end
                
                %Note: free stream velocity is the negative of the
                %rotor velocity
                free_stream(ii,:) = -(piv_vel + theta_dot*(pi/180)*radius(ii)*[-sind(theta),cosd(theta)]);
                
            end
            obj.free_stream_vel = free_stream;
            
            switch num_wake
                case 0              %No wake
                    obj.wake_obj = VortexVec([0,0],0,true,2);
                case 1              %TE wake
                    obj.wake_obj = VortexVec(obj.blade_end_pts(2,:)+0.25*[dx_uni,-dz_uni],0,false,0);
                case 2              %TE and LE wake
                    %Just guessing on this one - probably needs fixed
                    obj.wake_obj = VortexVec([obj.blade_end_pts(2,:)+0.25*[dx_uni,-dz_uni]; obj.blade_end_pts(1,:)-0.25*[dx_uni,-dz_uni]],[0;0],false,0);
                case 3
                    %LESP wake
                    if (~isempty(varargin)) && isscalar(varargin{1})
                        obj.LESP_crit = varargin{1};
                    end
                    %Initialize to initial location of TE
                    %and LE
                    %Note: ordered TE, LE
                    obj.wake_obj = VortexVec([obj.blade_end_pts(2,:)+varargin{3}*[dx_uni,-dz_uni]; obj.blade_end_pts(1,:)-varargin{2}*[dx_uni,-dz_uni]],[0;0],false,0);
                    obj.last_wake = obj.wake_obj;
                    %Initialize to -1 -> indicates that it doesn't
                    %exist
                    obj.last_wake_index = [-1,-1];
                    %plot(x,z,'k*',obj.bound_loc(:,1),obj.bound_loc(:,2),'go',obj.wake_obj.vor_loc(:,1),obj.wake_obj.vor_loc(:,2),'k^');
                otherwise
                    error('Use TE wake (1), LE and TE wake (2), or LESP wake (3)');
            end
        end         %End constructor
        
        
        
        
        
        %===============================================%
        %Getters for dependent properties
        function num_sample = get.N_sample(obj)
            num_sample = obj.num_vor;
        end
        
        function tanj = get.tanj_vec(obj)
            tanj = [obj.norm_vec(2),-obj.norm_vec(1)];
        end
        
        function bound = get.bound_loc(obj)
            bound = obj.vor_loc;
        end
        
        function b_stren = get.bound_strength(obj)
            b_stren = obj.vor_strength;
        end
        
        
        
        
        
        %========================================================%
        %Setter for bound strength
        function obj = set.bound_strength(obj,vor_vec_obj)
            if isa(obj,'SingleBlade')
                if isa(vor_vec_obj,'VortexVec')
                    stren = vor_vec_obj.vor_strength;
                    if SameSize(stren,obj.bound_strength)
                        obj.vor_strength = stren;
                    else
                        error('The strength input is the wrong size');
                    end
                else
                    error('Input a VortexVec object');
                end
            else
                error('Input a SingleBlade object');
            end
        end
        
        
        
        
        
        %=======================================================%
        %Updates position
        function obj_out = PosUpdate(obj,piv_loc,piv_vel,theta,theta_dot,num_wake,alpha,beta,LE_dis,TE_dis)
            N = obj.N_sample;
            len = obj.blade_len;
            pivot = obj.piv;
            %LESP_crit = obj.LESP_crit;
            obj_out = SingleBlade(N,len,pivot,piv_loc,piv_vel,theta,theta_dot,num_wake,alpha,beta,obj.LESP_crit,LE_dis,TE_dis);
            
            if num_wake == 3
                %LESP wake
                obj_out.LESP_crit = obj.LESP_crit;
                obj_out.old_LE_stren = obj.old_LE_stren;
                %Sets last wake position
                obj_out.last_wake = obj.last_wake;
                
                %Wake location 1/3 of way from separation location to last
                %vortex, unless it's too far away
                obj_out.last_wake_index = obj.last_wake_index;
                new_wake_loc = obj_out.wake_obj.vor_loc + (obj_out.last_wake.vor_loc - obj_out.wake_obj.vor_loc)/3;
                
                wake_sqr_dist = (new_wake_loc(:,1) - obj_out.wake_obj.vor_loc(:,1)).^2 +...
                    (new_wake_loc(:,2) - obj_out.wake_obj.vor_loc(:,2)).^2;
                
                %Checking if should move 1/3 of way, or if it's too far or
                %the previous vortex doesn't exist
                for ii = 1:length(wake_sqr_dist)
                    if (wake_sqr_dist(ii) > obj_out.blade_len/4) || (obj_out.last_wake_index(ii) == -1)
                        %If too far, go with default
                        new_wake_loc(ii,:) = obj_out.wake_obj.vor_loc(ii,:);
                        %Now, this last wake doesn't exist
                        obj_out.last_wake_index(ii) = -1;
                    end
                end
                obj_out.wake_obj = VortexVec(new_wake_loc,obj_out.wake_obj.vor_strength,false,0);
                
            end
        end
        
        
        
        
        
        
        
        %========================================================%
        %Finds RHS
        function new_blade_obj = RHSGen(blade_obj,other_vor)
            %Type checking
            if ~isa(blade_obj,'SingleBlade')
                error('The blade_obj input to RHSGen must be a SingleBlade');
            end
            if ~isa(other_vor,'VortexVec')
                error('The other_vor input to RHSGen must be a VortexVec');
            end
            %Preallocating
            rhs = zeros(blade_obj.N_sample,1);
            
            %Finding velocity at the blade sample points due to the other
            %vortecies
            vel_vec = other_vor.VorVelocity(false,blade_obj.sample_loc,false);
            free_stream = blade_obj.free_stream_vel;
            
            for ii = 1:blade_obj.N_sample
                rhs(ii) = -(free_stream(ii,:) + vel_vec(ii,:))*(blade_obj.norm_vec)';
            end
            new_blade_obj = blade_obj;
            new_blade_obj.RHS = rhs;
        end
        
        
        
        
        
        
        
        %=======================================================%
        %RHSWakeGen - generates RHS for the wake vortecies
        function new_blade = RHSWakeGen(blade,old_blade,varargin)
            switch blade.wake_obj.num_vor
                case 0          %There are no wake vortecies
                    %Don't generate a RHS_wake
                    new_blade = blade;
                case 1          %There is a TE wake
                    new_blade = blade;
                    %new_blade.RHS_wake = 0;
                    new_blade.RHS_wake = sum(old_blade.bound_strength);
                case 2
                    if ~isempty(blade.last_wake_index)
                        %LESP Wake
                        new_blade = blade;
                        rhs_wake_temp = zeros(2,1);
                        rhs_wake_temp(1) = sum(old_blade.bound_strength);
                        if ~isempty(varargin)
                            %This is for LESP wake
                            rhs_wake_temp(2) = varargin{1};
                        end
                        %else rhs_wake_temp(2) = 0;
                        new_blade.RHS_wake = rhs_wake_temp;
                    else
                        %TE and LE wake
                        new_blade = blade;
                        rhs_wake_temp = zeros(2,1);
                        rhs_wake_temp(1) = sum(old_blade.bound_strength);
                        %rhs_wake_temp(2) = 0;
                        new_blade.RHS_wake = rhs_wake_temp;
                    end
                otherwise
                    error('Use TE wake (1), LE and TE wake (2), or LESP wake (3)');
                    
            end
        end             %End RHSWakeGen
        
        
        
        
        
        
        %===============================================================%
        %Generates a list of vortecies in a rectangular deletion zone
        function vor_to_delete = VorExclusionZone(blade,wake_vor,zone_size)
            %Type checking
            if ~ isa(blade,'SingleBlade')
                error('The blade input to VorExclusionZone must be a SingleBlade object');
            end
            if ~ isa(wake_vor,'VortexVec')
                error('The wake_vor input to VorExclusionZone must be a VortexVec object');
            end
            
            %Checking zone_size
            %It should be either a single number (a symmetrical exclusion
            %zone around the blade) or a 1x2 vector (specifying the +-
            %dimesions of the zone)
            
            zone_size = unique(zone_size);      %Note that this also turns matricies into vectors & sorts it
            switch length(zone_size)
                case 1              %A symmetrical exclusion zone
                    if zone_size < 0
                        error('The symmetrical exclusion zone distance must be a positive number');
                    end
                    %Change into two-sided exclusion zone to reduce code
                    %in deletion zone checking
                    zone_size = [-zone_size,zone_size];
                    
                case 2              %Asymmetrical exclusion zone
                    %To avoid confusion, the exclusion zone is defined as a
                    %+ distance in the direction of the normal vector and a
                    %- distance for the other side
                    if zone_size(1) > 0
                        error('The lower side exclusion zone distance must be a negative number');
                    end
                    if zone_size(2) < 0
                        error('The upper side exclusion zone distance must be a positive number');
                    end
                   
                otherwise
                    error('The zone_size input to VorExclusionZone must be a scalar or 1x2 vector');
            end                     %End switch statement
            
            % ---------------- Begin Deletion Code -------------------%
            %Note: only generates a list of vortecies to delete - it
            %doesn't actually delete any of them.
            
            %initialization
            vor_to_delete = [];
            
            %Converting to local coordinates
            local_end_pts = localCoord(blade.piv_loc,blade.theta, blade.blade_end_pts);
            local_wake_loc = localCoord(blade.piv_loc,blade.theta,wake_vor.vor_loc);
            
            %Finding x-direction range
            x_range = sort(local_end_pts(:,1));
            
            %Marches through the wake vortecies and checks if they are
            %within the correct (local) x and y range, and if they are,
            %adds them to the list
            for jj = 1:wake_vor.num_vor
                %Checking if within y range
                %Local y coordinate must be greater than the minimum of the
                %range and less than the maximum
                if (local_wake_loc(jj,2) > zone_size(1)) && (local_wake_loc(jj,2) < zone_size(2))
                    
                    %Checking if in range in x-direction
                    if (local_wake_loc(jj,1) > x_range(1)) && (local_wake_loc(jj,1) < x_range(2))
                        %Within range in both x and y-directions - delete
                        vor_to_delete = [vor_to_delete,jj];
                    end     %end x-check
                    
                end         %End y range check
            end             %End stepping through vortecies
            
        end         %End VorExclusionZone
        
        
        
        
        %==============================================================%
        %Generates a list of vortecies which pass through the line of the
        %blade
        function intersect_vor = VorLineIntersect(blade,new_wake,old_wake)
            %--------------- Type checking -----------------------%
            if ~ isa(blade,'SingleBlade')
                error('The blade input to VorExclusionZone must be a SingleBlade object');
            end
            if ~ isa(new_wake,'VortexVec')
                error('The new_wake input to VorExclusionZone must be a VortexVec object');
            end
            if ~ isa(old_wake,'VortexVec')
                error('The old_wake input to VorExclusionZone must be a VortexVec object');
            end
            if new_wake.num_vor ~= old_wake.num_vor
                error('There must be the same number of vortecies in the new_wake and old_wake');
            end
            
            %--------------- Intersection Detection -------------%
            %Initializes a vector of indecies of the intersecting vortecies
            intersect_vor = [];
            
            %for ii = 1:num_blade       %loop through the blades - not anymore
            %s = end_pts(2*ii,:) - end_pts(2*ii-1,:);
            s = blade.blade_end_pts(2,:) - blade.blade_end_pts(1,:);
            
            %loops through the wake vortecies - find index of intersecting
            %vortecies
            for kk = 1:new_wake.num_vor
                %Finds intersections - based on https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect#565282
                %Setting up vectors
                %r = wake_vor_loc(kk,:) - old_wake_vor_loc(kk,:);
                r = new_wake.vor_loc(kk,:) - old_wake.vor_loc(kk,:);
                
                r_x_s = cross2D(r,s);
                %q_minus_p = end_pts(2*ii-1,:)-old_wake_vor_loc(kk,:);
                q_minus_p = blade.blade_end_pts(1,:)-old_wake.vor_loc(kk,:);
                
                %checking parallel and colinear cases
                if r_x_s == 0
                    %If colinear, otherwise don't intersect
                    if cross2D(q_minus_p,r) == 0
                        t_0 = (q_minus_p*r')/(r*r');
                        t_1 = (q_minus_p + s)*r'/(r*r');
                        %checking overlap of colinear line segments
                        if (t_0 >= 0) && (t_0 <= 1)
                            intersect_vor = [intersect_vor, kk];
                        elseif (t_1 >= 0) && (t_1 <= 1)
                            intersect_vor = [intersect_vor, kk];
                        end
                    end
                    %checking non-parallel cases
                else
                    %check t and u
                    t_val = cross2D(q_minus_p,s)/r_x_s;
                    u_val = cross2D(q_minus_p,r)/r_x_s;
                    
                    %if 0<= t,u <= 1, intersect, otherwise nonintersecting
                    if ((t_val >= 0) && (t_val <= 1)) && ((u_val >= 0) && (u_val <= 1))
                        intersect_vor = [intersect_vor, kk];
                    end
                end         %end checking cases
            end             %end going through wake vor looking for intersections
            %end            %end looping through blades
        end             %End VorLineIntersect
        
        
        
        
        %=========================================================%
        function obj_out = BladeLift(blade,vel,del_t,method,density,varargin)
            %Note: vel is velocity from the vortecies. It is passed in
            %(rather than calculated here) since either it or the other
            %vortecies themselves needed to be passed in, and this way the
            %FMM code can be all-in-one in the RotorObj function, and thus
            %much more efficient
            
            %Error checking
            if ~isa(blade,'SingleBlade')
                error('Input a SingleBlade object');
            end
            [~,vel_n] = size(vel);
            if vel_n ~= 2
                error('The velocity input must have 2 columns')
            end
            
            switch method
                case 'steady_bernoulli'
                    obj_out = blade;
                    
                    %Initialize a temporary variable
                    lift_vec = zeros(1,blade.N_sample);
                    for ii = 1:blade.N_sample
                        free_stream_mag = sqrt(sum(blade.free_stream_vel(ii,:).^2));
                        %The equation in Low Speed Aerodynamics, but
                        %stuff cancels
                        %lift_vec(ii) = (density*free_stream_mag*blade.bound_strength(ii)/(blade.blade_len/blade.N_sample))/(0.5*density*free_stream_mag^2*(blade.blade_len/blade.N_sample));
                        lift_vec(ii) = blade.bound_strength(ii)/(free_stream_mag);
                    end
                    
                    obj_out.C_normal = (2/blade.blade_len)*sum(lift_vec);
                    obj_out.C_drag = 0;
                    obj_out.lift_method = method;
                    
                case 'unsteady_bernoulli'
                    %Check if there is any old data for derivative
                    %approximation
                    if isempty(varargin);
                        error('At least one old blades data is needed');
                    else
                        if ~isa(varargin{1},'SingleBlade')
                            error('For the unsteady_bernoulli lift method, a SingleBlade input must be made');
                        end
                        obj_out = blade;
                        old_blade_1 = varargin{1};
                        del_p_vec = zeros(blade.N_sample,1);
                        del_l_vec = zeros(blade.N_sample,2);
                        v_inf_sqr = zeros(blade.N_sample,1);
                        %Find delta p vector/stag press (sort of)
                        %Spacing between bound vortecies (Note: there
                        %are N-1 of these)
                        %bound_del_l = 0.75*blade.del_l(1:end-1) +0.25*blade.del_l(2:end);
                        
                        for ii = 1:blade.N_sample
                            del_p_vec(ii) = (blade.free_stream_vel(ii,:)+vel(ii,:))*blade.tanj_vec'*(blade.bound_strength(ii)/blade.del_l(ii));
                            
                            %Is this sum going the right direction?
                            del_p_vec(ii) = del_p_vec(ii) + (sum(blade.bound_strength(1:ii)) - sum(old_blade_1.bound_strength(1:ii)))/del_t;
                            %del_p_vec(ii) = del_p_vec(ii) + (sum(blade.bound_strength) - sum(old_blade_1.bound_strength))/del_t;
                            
                            %Divide by V_inf^2
                            v_inf_sqr(ii) = sum(blade.free_stream_vel(ii,:).^2);
                            del_p_vec(ii) = del_p_vec(ii)*blade.del_l(ii)/v_inf_sqr(ii);
                            %This is the normal part
                            del_l_vec(ii,2) = del_p_vec(ii)*blade.del_l(ii);
                        end
                        
                        
                        %Normal force coefficient
                        %C_L = (2/blade.N_sample)*norm_vec(2)*sum(del_p_vec);
                        %Should this be divided by del_l at each step?
                        C_L = (2/blade.blade_len)*sum(del_p_vec);
                        C_D = C_L*obj_out.norm_vec(1);
                        [LESP,~] = LESPFinder(blade);
                        C_L = C_L*obj_out.norm_vec(2) + (2*pi*(LESP^2)*obj_out.norm_vec(1));
                        C_D = C_D - (2*pi*(LESP^2)*obj_out.norm_vec(2));
                        
                        u_inf_avg = mean(v_inf_sqr);
                        del_l_vec(:,1) = del_l_vec(:,2);
                        %Lift in upward direction
                        del_l_vec(:,2) = del_l_vec(:,2)*obj_out.norm_vec(2) + ...
                            pi*blade.blade_len*u_inf_avg*(LESP^2)*obj_out.norm_vec(1);
                        %Lift in + x direction
                        del_l_vec(:,1) = del_l_vec(:,1)*obj_out.norm_vec(1) - ...
                            pi*blade.blade_len*u_inf_avg*(LESP^2)*obj_out.norm_vec(2);
                        
                        
                        
                        %Now C_normal is being used as C_L
                        obj_out.C_normal = C_L;
                        obj_out.C_drag = C_D;
                        obj_out.lift = density*[sum(del_l_vec(:,1)),sum(del_l_vec(:,2))];
                        obj_out.lift_method = method;
                    end
                case 'uhlman'
                    %Type checking
                    if ~isvector(varargin{1})
                        error('Input a C_p_upper vector to uhlman method')
                    end
                    if ~isvector(varargin{2})
                        error('Input a C_p_lower vector to uhlman method')
                    end
                    %Checking if everything is the same size
                    if ~((length(varargin{1}) == blade.N_sample)&&(length(varargin{2}) == blade.N_sample))
                        error('The C_p inputs must be the same size');
                    end
                    obj_out = blade;
                    obj_out.lift_method = method;
                    obj_out.C_p_upper = varargin{1};
                    obj_out.C_p_lower = varargin{2};
                    
                    obj_out.C_normal = (1/blade.N_sample)*sum(varargin{2}-varargin{1});
                    
                otherwise
                    error('Unexpected lift method entered. Use steady_bernoulli, unsteady_bernoulli, or uhlman');
            end
        end         %End function
    end         %End methods
end             %End class