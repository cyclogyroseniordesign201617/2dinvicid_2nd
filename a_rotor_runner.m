clear; clc;
% A program to run through different rotor speeds and find the lift and
% torque at different rotational speeds.

%min_rpm = 600;
%max_rpm = 1200;
%num_step = 10;
%rpm_step = 50;

%rpm = min_rpm:(max_rpm-min_rpm)/num_step:max_rpm
%rpm = min_rpm:rpm_step:max_rpm
%%
rpm = 800;
data = zeros(length(rpm),6);
header = {'Rotational Speed (rpm)','thrust_x (N)','thrust_y (N)','thrust_mag (N)','torque (N*m)','time (min)'};
file_name = 'DVM_results';
%rpm = 1000;
for ii = 1:length(rpm)
    close all;
    [thrust,thrust_mag,torque,time] = rotor_run_func(rpm(ii));
    data(ii,1) = rpm(ii);
    data(ii,2:3) = thrust;
    data(ii,4) = thrust_mag;
    data(ii,5) = torque;
    data(ii,6) = time/60;
end

data_and_header = [header;num2cell(data)];
xlswrite(file_name,data_and_header);
    