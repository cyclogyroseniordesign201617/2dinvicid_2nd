function obj = VorAdd(obj,pos_vec,strength_vec,concat)
%Concatenate says whether to concatanate the new vortecies or
%not. If yes, it just sticks them on the end, if no, it tries
%to fit them in the preallocated space
if concat       %Stick on end
    if VorSizeCheck(pos_vec,strength_vec)
        obj.vor_loc = [obj.vor_loc; pos_vec];
        obj.vor_strength = [obj.vor_strength; strength_vec];
        [num,~] = size(pos_vec);
        obj.num_vor = obj.space_size + num;     %everything to end of new vortecies is now data
        obj.space_size = obj.space_size + num;
    end
else                            %Fit into premade space, if possible
    if VorSizeCheck(pos_vec,strength_vec)
        [num,~] = size(pos_vec);    %number of new vortecies to fit
        
        %Temporary storage variables
        loc_temp = obj.vor_loc;
        stren_temp = obj.vor_strength;
        num_fit = obj.space_size - obj.num_vor;
        
        if num <= num_fit      %if it fits...
            loc_temp((obj.num_vor+1):(obj.num_vor+1+num),:) = pos_vec;
            stren_temp((obj.num_vor+1):(obj.num_vor+1+num)) = strength_vec;
            %Note: the allocated space doesn't change
        else                   %if it doesn't all fit...
            %First fit in all that you can, the concatenate on
            %the rest
            loc_temp((obj.num_vor+1):end,:) = pos_vec(1:num_fit,:);
            loc_temp = [loc_temp; pos_vec((num_fit+1):end,:)];
            
            stren_temp((obj.num_vor+1):end) = strength_vec(1:num_fit);
            stren_temp = [stren_temp; strength_vec((num_fit+1):end)];
            
            obj.space_size = obj.space_size + num - num_fit;
        end
        
        obj.vor_loc = loc_temp;
        obj.vor_strength = stren_temp;
        obj.num_vor = obj.num_vor + num;
    end         %End if for fitting in
end             %End if concat

end         %End concatonator