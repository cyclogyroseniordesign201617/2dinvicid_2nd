function [thrust,thrust_mag,torque,time] = rotor_run_func(rpm)
%A main program to run the steady-state case
tic;

format short;
%First, setup rotor position
num_blade = 6;
time_init = 0;
num_pts = 10*ones(num_blade,1);
piv = 0.25*ones(num_blade,1);
len_blade = 0.05*ones(num_blade,1);
%LESP_crit = 0.11*ones(num_blade,1);
lift_method = 'unsteady_bernoulli';
deletion_zone_size = 0.525*(len_blade/num_pts)*ones(num_blade,1); %used to be 0.5*...
surf_offset = 0.25*(len_blade/num_pts)*ones(num_blade,1);

rotor_op = RotorOptions(num_blade);
%Note: I think the following are all defaults
rotor_op.num_pt_vec = num_pts;
rotor_op.len_blade = len_blade;
rotor_op.piv = piv;
rotor_op.wake_setting = 'LESP';
%rotor_op.LESP_crit = LESP_crit;
rotor_op.lift_method = lift_method;
rotor_op.del_zone_size = deletion_zone_size;
rotor_op.surf_offset = surf_offset;
rotor_op.alpha_vec = 4*ones(num_blade,1);
rotor_op.beta_vec = 6*ones(num_blade,1);
rotor_op.LE_dis = 0.1*ones(num_blade,1);
rotor_op.TE_dis = 0.8*ones(num_blade,1);

rotor_op.radius = 0.1*ones(num_blade,1);
rotor_op.arm_len = 0.1028*ones(num_blade,1);
rotor_op.blade_piv_dis = 0.03*ones(num_blade,1);
rotor_op.ecc_len = 0.01981*ones(num_blade,1);
rotor_op.ecc_ang = 0.2698*ones(num_blade,1);            %radians
rotor_op.omega = rpm*(2*pi/60);                         %rad/sec
rotor_op.span = 0.20;                                   %m


rotor_op = rotor_op.LESPFromCSV;                        %Find correct LESP
time_f = 5*(2*pi/rotor_op.omega);                       %about 3 revolutions
del_t = 0.0001;
num_t_step = ceil(time_f/del_t);

time_dat_start = 3*(2*pi/rotor_op.omega);
num_dat_start = ceil(time_dat_start/del_t);

%For display
flow_axis = [-0.5,0.5,-0.7,0.3];
c_p_axis = [0,1,-2,2];
name = ['flow_' num2str(rpm) '_RPM'];

%Initializing everything

%Symmetric pitching
[pivLocFun,pivVelFun,thetaFunSym,thetaDotFunSym] = RotorBladePitchingSym(num_blade,rotor_op);
pivLoc = @(time) pivLocFun(time);
pivVel = @(time) pivVelFun(time);
thetaFun = @(time) thetaFunSym(time);
thetaDotFun = @(time) thetaDotFunSym(time);



%{
pivLoc = @(time) [-time, 0*0.019*len_blade(1)*sin(16*time)+3; -time, 0*0.019*len_blade(2)*sin(16*time)-3];
pivVel = @(time) [-1, 0*0.019*16*len_blade(1)*cos(16*time);-1, 0*0.019*16*len_blade(2)*cos(16*time)];
thetaFun = @(time) [-5;-45];
thetaDotFun = @(time) -0*ones(num_blade,1);
%}
%{
%Bad one on top
pivLoc = @(time) 4*[sin(0.25*time), -cos(0.25*time);-sin(0.25*time), cos(0.25*time)];
pivVel = @(time) 1*[cos(0.25*time), sin(0.25*time);-cos(0.25*time), -sin(0.25*time)];
thetaFun = @(time) [-25+180+0.25*(180/pi)*time; -25+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
%Bad one on bottom
pivLoc = @(time) 4*[-sin(0.25*time), cos(0.25*time); sin(0.25*time), -cos(0.25*time)];
pivVel = @(time) 1*[-cos(0.25*time), -sin(0.25*time); cos(0.25*time), sin(0.25*time)];
thetaFun = @(time) 1*[0.25*(180/pi)*time;180+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
%Start at top
pivLoc = @(time) 4*[-sin(0.25*time), cos(0.25*time)];
pivVel = @(time) 1*[-cos(0.25*time), -sin(0.25*time)];
thetaFun = @(time) [-45+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
%Start at bottom
pivLoc = @(time) 4*[sin(0.25*time), -cos(0.25*time)];
pivVel = @(time) 1*[cos(0.25*time), sin(0.25*time)];
thetaFun = @(time) [-5+180+0.25*(180/pi)*time];
thetaDotFun = @(time) 0.25*(180/pi)*ones(num_blade,1);
%}
%{
pivLoc = @(time) [-time, 0];
pivVel = @(time) [-1,0];
thetaFun = @(time) [-180*time];
thetaDotFun = @(time) -180*ones(num_blade,1);
%}
%{
%Can't activate LESP this way - depends on forward motion of blade
pivLoc = @(time) [0, 0];
pivVel = @(time) [0, 0];
thetaFun = @(time) 500*[(180/pi)*time];
thetaDotFun = @(time) 500*(180/pi)*ones(num_blade,1);
%}
%{
thetaFun = @(time) -4*sin(11.62*time)*ones(num_blade,1);
thetaDotFun = @(time) -4*11.62*cos(11.62*time)*ones(num_blade,1);
%}

rot_pos = RotorPosition(num_blade,time_init,pivLoc,pivVel,thetaFun,thetaDotFun);
rotor = RotorObj(rot_pos,rotor_op);

%Initializing the wake
wake = VortexVec([0,0],0,true,1);
%Initializing old wake to the current wake
old_wake = wake;

plot_obj = DispObj(rotor,wake,flow_axis,c_p_axis,name,num_t_step,false,'C_p','off','C_L','off');
plot_obj.grid_res = [50,50];

%Looping through time
for new_time = 0:del_t:time_f
    [rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,rotor_op);
    old_wake = wake;
    wake = VorWakeRollup(wake,rotor,del_t);
    %bbbbbbb = wake.vor_strength
    if rotor_op.wake_setting == 3
        rotor = LastWakePosUpdater(rotor,wake);
    end
    plot_obj = plot_obj.StoreData(rotor,rotor_op,wake,false,new_time);
end

%Close the movies
plot_obj.MovieClose;

%Show the lift
plot_obj = plot_obj.LiftPlot(rotor_op,num_dat_start);
plot_obj.FigureSaver;

thrust = plot_obj.thrust_avg;
thrust_mag = plot_obj.thrust_mag_avg;
torque = plot_obj.torque_avg;

time = toc;
end