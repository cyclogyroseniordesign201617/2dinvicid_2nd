function fit_err = LinearLESPFunc(LESP_in)
%A function to test the effects of the LESP value
%First, setup rotor position
close all;
NACA_file = 'Re_20000.csv';
LESP_in

num_blade = 1;
time_init = 0;
num_pts = 10*ones(num_blade,1);
piv = 0.25*ones(num_blade,1);
len_blade = 0.05*ones(num_blade,1);
LESP_crit = LESP_in*ones(num_blade,1);
lift_method = 'unsteady_bernoulli';
deletion_zone_size = 0.05*(len_blade/num_pts)*ones(num_blade,1); %used to be 0.5*...
surf_offset = 0.25*(len_blade/num_pts)*ones(num_blade,1);

rotor_op = RotorOptions(num_blade);
%Note: I think the following are all defaults
rotor_op.num_pt_vec = num_pts;
rotor_op.len_blade = len_blade;
rotor_op.piv = piv;
rotor_op.wake_setting = 'LESP';
rotor_op.LESP_crit = LESP_crit;
rotor_op.lift_method = lift_method;
rotor_op.del_zone_size = deletion_zone_size;
rotor_op.surf_offset = surf_offset;
rotor_op.alpha_vec = 4*ones(num_blade,1);
rotor_op.beta_vec = 6*ones(num_blade,1);
rotor_op.LE_dis = 0.25*ones(num_blade,1);
rotor_op.TE_dis = 0.25*ones(num_blade,1);
%{
%Moving linearly
rotor_op.radius = 0.1*ones(num_blade,1);
rotor_op.arm_len = 0.1028*ones(num_blade,1);
rotor_op.blade_piv_dis = 0.03*ones(num_blade,1);
rotor_op.ecc_len = 0.01981*ones(num_blade,1);
rotor_op.ecc_ang = 0.2698*ones(num_blade,1);            %radians
rotor_op.omega = 800*(2*pi/60);                         %rad/sec
rotor_op.span = 0.20;                                   %m
%}

Re = 20000;                                             %Linear Re
vel = Re*rotor_op.viscosity/(rotor_op.density*rotor_op.len_blade);

time_f = 7*(rotor_op.len_blade/vel);                    %about 5 chords
time_dat_start = 3*(rotor_op.len_blade/vel);            %2 chords
del_t = 0.0001;
num_t_step = ceil(time_f/del_t);

num_dat_start = ceil(time_dat_start/del_t);
AoA_step = 45;

%For display
flow_axis = [-7*rotor_op.len_blade,0.1,-0.1,0.1];
c_p_axis = [0,1,-2,2];
name = 'first_unsteady_test';


%Data collection
C_L_data = zeros(1,90/AoA_step + 1);
C_D_data = zeros(1,90/AoA_step + 1);

%Initializing everything
%{
%Symmetric pitching
[pivLocFun,pivVelFun,thetaFunSym,thetaDotFunSym] = RotorBladePitchingSym(num_blade,rotor_op);
pivLoc = @(time) pivLocFun(time);
pivVel = @(time) pivVelFun(time);
thetaFun = @(time) thetaFunSym(time);
thetaDotFun = @(time) thetaDotFunSym(time);
%}

pivLoc = @(time) [-vel*time, 0];
pivVel = @(time) [-vel, 0];
%thetaFun = @(time) [-5;-45];
thetaDotFun = @(time) -0*ones(num_blade,1);

step_num = 0;
for AoA = 0:AoA_step:90
    close all;
    step_num = step_num+1;
    
    C_L_temp = zeros(1,num_t_step);
    C_D_temp = zeros(1,num_t_step);
    
    thetaFun = @(time) -AoA;
    
    rot_pos = RotorPosition(num_blade,time_init,pivLoc,pivVel,thetaFun,thetaDotFun);
    rotor = RotorObj(rot_pos,rotor_op);
    
    %Initializing the wake
    wake = VortexVec([0,0],0,true,1);
    %Initializing old wake to the current wake
    old_wake = wake;
    
    plot_obj = DispObj(rotor,wake,flow_axis,c_p_axis,name,num_t_step,false,'C_p','off');
    
    
    %Looping through time
    count = 0;
    for new_time = 0:del_t:time_f
        count = count+1;
        
        [rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,rotor_op);
        old_wake = wake;
        wake = VorWakeRollup(wake,rotor,del_t);
        %bbbbbbb = wake.vor_strength
        %if rotor_op.wake_setting == 3
            rotor = LastWakePosUpdater(rotor,wake);
        %end
        plot_obj = plot_obj.StoreData(rotor,wake,false,new_time);
        
       %Collecting data
       C_L_temp(count) = rotor.blades{1}.C_normal;
       C_D_temp(count) = rotor.blades{1}.C_drag;
    end
    
    %Close the movies
    plot_obj.MovieClose;
    
    %Show the lift
    %plot_obj = LiftPlot(plot_obj,rotor_op);
    t = 0:del_t:time_f;
    plot(t,C_L_temp,t,C_D_temp);
    
    C_L_data(step_num) = mean( C_L_temp(num_dat_start:end) );
    C_D_data(step_num) = mean( C_D_temp(num_dat_start:end) );
end

%Compare to NACA data
%Get the data
NACA_dat = csvread(NACA_file,1,0);
theta = 0:AoA_step:90;

%Put a spline through it
NACA_C_L = pchip(NACA_dat(:,1),NACA_dat(:,2),theta);
NACA_C_D = pchip(NACA_dat(:,1),NACA_dat(:,3),theta);

figure;
plot(theta,C_L_data,'ko',theta,NACA_C_L);
legend('Sim','NACA');
xlabel('Angle of Attack, degrees');
ylabel('C_L');
grid on;

figure;
plot(theta,C_D_data,'ko',theta,NACA_C_D);
legend('Sim','NACA');
xlabel('Angle of Attack, degrees');
ylabel('C_D');
grid on;

err_C_L = sum( (C_L_data - NACA_C_L).^2 );
err_C_D = sum( (C_D_data - NACA_C_D).^2 );

fit_err = err_C_L + err_C_D;

end

