function [err,varargout] = LESPMinHelpter(LE_stren,rotor_in,other_vor,L,U,p,LESP_orig,first_run,single_out)
%A helper function for fminunc
%Note: depends on known blade size, etc
%[err,LESP,LE_stren_guess,bound_tot] = LESPMinHelpter(LE_stren,rotor_in,L,U,p,LESP_orig,first_run,single_out)

%Type checking
if ~isa(rotor_in,'RotorObj')
    error('Input a RotorObj');
end
rhs = rotor_in.RHS_tot;

%Checks if it is the original run
if first_run
    %If it is the first run, it just runs it without change
    bound_tot = U\(L\(rhs(p,:)));
    
    %{
    %This is the single-blade critical part
    blade_temp = rotor_in.blades{1};
    blade_temp.bound_strength = blade_temp.VorStrengthSet(bound_tot(1:end-2,:),true,0);
    [LESP,LESP_dif] = LESPFinder(blade_temp);
    
    err = LESP_dif;
    %}
    
    %Finding LESP at each blade
    err = zeros(rotor_in.num_blades);
    LESP_vec = zeros(rotor_in.num_blades);
    rotor_temp = RotorBoundStrengthSet(rotor_in,bound_tot);
    
    for jj = 1:rotor_in.num_blades
        [LESP,LESP_dif] = LESPFinder(rotor_temp.blades{jj});
        err(jj) = LESP_dif;
        LESP_vec(jj) = LESP;
    end
    
    %Checks if any are over critical, and generates a guess for the
    %optimizer
    
    LE_stren_guess = zeros(1,length(err));
    for ii = 1:length(err)
        %If its above critical
        %if abs(err) >= 0 is wrong
        if err(ii) >= 0
            %Guess the strength as 1/2 strength of 1st bound vortex of the
            %blade
            LE_stren_guess(ii) = 0.5*rotor_temp.blades{ii}.bound_strength(1);
            %{
            if ~isempty(blade_temp.old_LE_stren)
                LE_stren_guess(ii) = blade_temp.old_LE_stren;
                %LE_stren_guess(ii) = 0.5*blade_temp.bound_strength(1);
            else
                LE_stren_guess(ii) = 0.5*blade_temp.bound_strength(1);
            end
            %}
            %Else LE_stren_guess(ii) = 0;
        end
    end
    
    %Putting out outputs
    if ~single_out
        varargout{1} = LESP_vec;
        varargout{2} = LE_stren_guess;
        varargout{3} = bound_tot;
    end
    
else
    %This is a repeated run - replace rhs with given LE_stren
    
    %Regenerate RHS with given strengths
    %No wake vortecies needed, since this is LESP, num_wake = 3, partial
    %update with given strengths
    rotor_out = rotor_in.FindTotRHS(other_vor,3,true,LE_stren);
    rhs = rotor_out.RHS_tot;
    
    bound_tot = U\(L\(rhs(p,:)));
    
    %This is the single-blade critical part
    %{
    blade_temp = rotor_in.blades{1};
    blade_temp.bound_strength = blade_temp.VorStrengthSet(bound_tot(1:end-2,:),true,0);
    [LESP,LESP_dif] = LESPFinder(blade_temp);
    %}
    %Finding LESP at each blade
    err = zeros(1,rotor_in.num_blades);
    LESP_vec = zeros(rotor_in.num_blades);
    rotor_temp = RotorBoundStrengthSet(rotor_in,bound_tot);
    
    for jj = 1:rotor_in.num_blades
        [LESP,LESP_dif] = LESPFinder(rotor_temp.blades{jj});
        LESP_vec(jj) = LESP;
        
        %Finding error
        if LESP_dif > 0
            %If magnitude of LESP is above critical
            %Want the above critical ones to stay critical
            %Also want them to say on the same side as they were
            if LESP_orig(jj) > 0
                %if original +crit
                err(jj) = ((LESP - rotor_temp.blades{jj}.LESP_crit)/rotor_temp.blades{jj}.LESP_crit);
            else
                %if original at -crit
                err(jj) = ((-LESP - rotor_temp.blades{jj}.LESP_crit)/rotor_temp.blades{jj}.LESP_crit);
            end
        else
            %Magnitude of LESP is below critical - want it to stay where it
            %was
            %Or should it be err(jj) = LE_stren(jj)
            %err(jj) = LESP - LESP_orig(jj);
            err(jj) = LE_stren(jj);
        end
    end
    
    if ~single_out
        varargout{1} = LESP_vec;
        varargout{2} = 'a';
        varargout{3} = bound_tot;
    end

end
