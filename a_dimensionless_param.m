clear; clc;
%A program to find the dimensionless parameters

%Vector is the exponents of the basic parameter which, multiplied together,
%for the parameters thought to be important. They are placed in the order:
% [Length]
% [ Mass ]
% [ Time ]

radius =                [1; 0; 0];              %m
radial_speed =          [0; 0; -1];             %rad/sec = 1/sec
air_density =           [-3; 1; 0];             %kg/m^3
blade_chord =           [1; 0; 0];              %m
air_viscosity =         [-1; 1; -1];            %kg/(m*sec)

A = sym([radius, radial_speed, air_density, air_viscosity,blade_chord])
c = rref(A)
basis = null(A)
%This works out to the Reynolds number: Rho*R^2*omega/mu
%I think instead I will use Re = Rho*R*chord*omega/mu - I think this will
%relate better to a linear Re.

%{
%Testing
A = sym([0,1,0,0,0; 1,0,1,1,1; 0,0,-2,-1,-1])
%b = colspace(A)
%c = colspace(A')
d = null(A)
%}