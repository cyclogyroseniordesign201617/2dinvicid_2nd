function [C_p_vec,blade_lift,total_vel] =...
    UhlmanMethod(num_blade,len_blade,piv_loc,piv_vel,piv,theta,theta_dot,N,del_t,sur_offset,...
    bound_vor_loc,bound_vor_strength,wake_vor_loc,wake_vor_strength,old_total_vel,density)

%========= From previous code ===================%
%Finds the pressure coefficient vector and lift (normal to blade) for all blades
%{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Notes
lift and drag are with respect to the blade. Another program is needed to
translate them to rotor quantities
The bound vortex location from rotorGeom cannot be used since it includes
the trailing edge wake vortex
The offset is in fraction of the chord (sur_offset = offset/len_blade), and
is from the infinitely thin blade surface to the offset surface points

All stuff is converted to local coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}

%%%%%%%%%%%%%%% Finding constants %%%%%%%%%%%%%%%%%%%%
offset = sur_offset*len_blade;
del_l = len_blade/N;

%Initialization     2N surface points/blade
I = zeros(2*N*num_blade);               %enthalpy influence coefficients
vol_int = zeros(2*N*num_blade,1);       %volume integral
%visc_int = zeros(2*N*num_blade,1);      %surface integral involving viscosity
%temporal_int = zeros(2*N*num_blade,1);  %surface integral involving temporal effects
total_vel = zeros(2*N*num_blade,2);     %Total velocity at each surface point
C_p_vec = zeros(2*N*num_blade,1);
blade_lift = zeros(num_blade,1);
%blade_drag = zeros(num_blade,1);
%p_dif = zeros(N*num_blade,1);
delta_C_p = zeros(N*num_blade,1);


%%%%%%%%%%%%%%% Finding offset surface points %%%%%%%%
%Finding normal vector for offset
[~,~,norm_vec,free_stream_vel,~,~] =...
    rotorGeom(num_blade,len_blade,piv_loc,piv_vel,piv,theta,theta_dot,N,del_t,sur_offset);

%Converting to local free stream velocity
local_free_stream = zeros(N*num_blade,2);
for kk = 1:num_blade
    local_free_steam1 = localCoord(piv_loc(kk,:),theta(kk),free_stream_vel);
    local_free_stream(((kk-1)*N+1):kk*N,:) = local_free_steam1(((kk-1)*N+1):kk*N,:);
end
%free_stream_vel = local_free_stream;
    
%Upper surface
[u_surface,~,~,~,~,~] =...
    rotorGeom(num_blade,len_blade,(piv_loc+offset*norm_vec),piv_vel,piv,...
    theta,theta_dot,N,del_t,sur_offset);

%Lower surface
[l_surface,~,~,~,~,~] =...
    rotorGeom(num_blade,len_blade,(piv_loc-offset*norm_vec),piv_vel,piv,...
    theta,theta_dot,N,del_t,sur_offset);

%Don't take LE, TE points
surf_pts = [u_surface;l_surface];

%%%%%%%%%%%%%%% Finding velocity at surface points %%%%%%%%
%Number of locations for FMM
%Find velocity at the location by giving the surface points 0 strength
%vortecies
M = length(bound_vor_strength) + length(wake_vor_strength) + 2*N*num_blade;

all_vor_strength = [bound_vor_strength; wake_vor_strength];
all_vor_loc = [bound_vor_loc; wake_vor_loc];

strength1 = [zeros(2*N*num_blade,1); bound_vor_strength; wake_vor_strength];
%strength2 = ones(2*N*num_blade,1);
loc1 = [surf_pts; bound_vor_loc; wake_vor_loc];

U = zfmm2dpart(4,M,loc1',1i*strength1',1,0,0);
surf_vel = zeros(M,2);
surf_vel(:,1) = real(U.pot/(2*pi));
surf_vel(:,2) = -imag(U.pot/(2*pi));

%Converting to local velocities
local_surf_vel= zeros(2*N*num_blade,2);
for kk = 1:num_blade
    local_surf_vel(((kk-1)*N+1):kk*N,:) = localCoord(piv_loc(kk,:),theta(kk),surf_vel(((kk-1)*N+1):kk*N,:));
    local_surf_vel(((kk-1)*N+1+N*num_blade):(kk*N+N*num_blade),:) = localCoord(piv_loc(kk,:),theta(kk),surf_vel(((kk-1)*N+1+N*num_blade):(kk*N+N*num_blade),:));
end
%surf_vel = local_surf_vel;


%%%%%%%%%%%%%%% Enthalpy Influence Matrix %%%%%%%%%%%%%%%%%

%Making Matrix
for ii = 1:2*N*num_blade        %Steps through all surface points
    %Now need to find the influence from the other points - find them
    %in such a way that their normal vector is known
    for jj = 1:2*N*num_blade
        if jj <= N*num_blade
            %side = 1;           %upper side
            blade_number = floor((jj-1)/N) + 1;
            norm_temp = norm_vec(blade_number,:);
        else
            %side = 2;           %lower side
            blade_number = floor((jj-N*num_blade-1)/N) + 1;
            norm_temp = -norm_vec(blade_number,:);
        end
        
        %Filling matrix
        if ii == jj
            I(ii,jj) = pi;
        else
            r_i_j = surf_pts(ii,:) - surf_pts(jj,:);
            I(ii,jj) = -del_l*(norm_temp*r_i_j')/(r_i_j*r_i_j'); %should have *del_l
        end
    end             %End stepping through other points
    
    
    
    %I think this is wrong
    %{
    for side = 1:2          %Flips side of blade -> 2 = lower
        for jjish = 1:N*num_blade
            %Finds appropriate normal vector
            %jjish_offset is the offset from jj to jjish
            blade_number = floor((jjish-1)/N) + 1;
            if side == 1        %upper side
                norm_temp = norm_vec(blade_number,:);
                jjish_offset = 0;
            else                %side == 2, lower side
                norm_temp = -norm_vec(blade_number,:);
                jjish_offset = num_blade*N;
            end
            
            jj = jjish + jjish_offset;
            %Filling matrix
            if ii == jj
                I(ii,jj) = pi;
            else
                r_i_j = surf_pts(ii,:) - surf_pts(jj,:);
                I(ii,jj) = -del_l*(norm_temp*r_i_j')/(r_i_j*r_i_j');
            end
        end         %end side
    end             %end blade stepping
    %}
    %%%%%%%%%%%%%% Volume Intergal %%%%%%%%%%%%%%%%%%%%%%%%%
    %NOTE: ASSUMING IN EQUATION 4-7 OF MASTERS THESIS THAT |r_ij| is |r|, where
    %r (vector) = r_surf_pt - r_vortex
    
    %Stepping through all vortecies
    for ww = 1:(M - 2*N*num_blade)
        r_surf_source = surf_pts(ii,:) - all_vor_loc(ww,:);
        
        %Need to select right free stream velocity
        if ii <= N*num_blade
            U_temp = free_stream_vel(ii,:);
        else
            U_temp = free_stream_vel(ii-N*num_blade,:);
        end
        total_vel(ii,:) = U_temp+surf_vel(ii,:);
        vol_int(ii) = vol_int(ii) + ((all_vor_strength(ww)*...       %Change from + to -?, del_l is for the dS
            cross2D((U_temp+surf_vel(ii,:)),r_surf_source))/(r_surf_source*r_surf_source')); %Should be U_temp+surf_vel(ii,:)
    end             %End stepping through all vortecies
    
end                 %end stepping thru surface points

%{
%%%%%%%%%%%%%%%%% Surface Integrals %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
%Time derivative of total velocity
total_vel_rate_change = (total_vel - old_total_vel)/del_t;


%Stepping through the surface points again
for ii = 1:2*N*num_blade
    for jj = 1:2*N*num_blade
        if jj <= N*num_blade
            norm_offset = 0;
        else
            norm_offset = N*num_blade;
        end
        blade_number = floor((jj-norm_offset-1)/N) + 1;
        norm_temp = norm_vec(blade_number,:);
        
        %ASSUMING THE r's IN THE TEMPORAL INTEGRAL ARE BETWEEN SURFACE
        %POINTS
        if ii ~= jj
            r_i_j = surf_pts(ii,:) - surf_pts(jj,:);
            sqr_mag_r_i_j = r_i_j*r_i_j';
            
            %%%%% Temporal Intregal %%%%%%%%
            temporal_int(ii) = temporal_int(ii) +...
                (norm_temp*total_vel_rate_change(ii,:)')*del_l*log(1/sqrt(sqr_mag_r_i_j));
            
            %%%%% Viscous Integral %%%%%%%%%
            %Not added yet, since the kinematic viscosity should be 0, and
            %also because the notation in the Master's thesis appears to be
            %messed up
        end
    end             %End stepping thru jj
end                 %end ii

%}
%%%%%%%%%%%%%%%% Solving for Enthalpy %%%%%%%%%%%%%%%%%%%%%%%%%%%
%Right hand side
%RHS = vol_int + visc_int + temporal_int;
%RHS = vol_int + temporal_int;
RHS = vol_int;

%Solving matrix for enthalpy
enthalpy = I\RHS;


%%%%%%%%%%%%%%% Finding Outputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Finding pressure coefficients
for ii = 1:2*N*num_blade        %Steps through all surface points
    %Grabbing the correct free stream velocity
    if ii <= N*num_blade
        U_temp = free_stream_vel(ii,:);
    else
        U_temp = free_stream_vel(ii-N*num_blade,:);
    end
    %%%%%%%%%%%% Pressure Coefficients %%%%%%%%%%%%
    %Stagnation pressure
    %NOTE: SHOULD THIS JUST BE THE PIVOT POINT FREE STREAM?
    %p_stag = 0.5*density*(U_temp*U_temp');
    %C_p_vec(ii) = density*(enthalpy(ii) - 0.5*((total_vel(ii)*total_vel(ii)') -...
    %    (U_temp*U_temp')))/p_stag;
    %Can remove density, since it cancels out
    p_stag = 0.5*(U_temp*U_temp');
    C_p_vec(ii) = (enthalpy(ii) - 0.5*((total_vel(ii,:)*total_vel(ii,:)') -... %Should be total_vel(ii,:)*total_vel(ii,:)')
        (U_temp*U_temp')))/p_stag;
    
end

%%%%%%%%%%%%%%% Lift and Drag %%%%%%%%%%%%%%%%%%%%
%Finds pressure difference between the top and bottom of the blade
%(positive difference = lift along blade's normal vector)

%This finds the actual lift
for mm = 1:N*num_blade
    %{
    sqr_mag_free_stream = free_stream_vel(mm,:)*free_stream_vel(mm,:)';
    p_stag = 0.5*density*sqr_mag_free_stream;
    p_dif(mm) = p_stag*(C_p_vec(N*num_blade+mm) - C_p_vec(mm));
    %}
    delta_C_p(mm) = C_p_vec(N*num_blade+mm) - C_p_vec(mm);
end
%{
for nn = 1:num_blade
    blade_lift(nn) = del_l*sum(p_dif(((nn-1)*N+1):nn*N));   %should it be len_blade?
end
%}
%Finding the lift coefficient
for nn = 1:num_blade
    blade_lift(nn) = ([0,1]*norm_vec(nn,:)')*(1/N)*sum(delta_C_p(((nn-1)*N+1):nn*N));
end         %end function