clear; clc; close all;
%A program to visualize the LESP parameters versions

%Set up Re vector
Re = [10000, 20000, 40000, 80000];
Re_min = min(Re);
Re_max = max(Re);
Re_smooth = Re_min-0.1*Re_max:(Re_max - Re_min)/100:1.1*Re_max;

%================== Run 2 =============================================%
% TE = 1.25, small del zone
%Run 2 LESP
run_2_LESP = [0.05484532304, 0.1167827846, 0.007703423116297, 0.054415642932676];
r2L_spline = pchip(Re,run_2_LESP,Re_smooth);

%Run 2 Error
run_2_err = [6.38928477942718, 13.6203, 7.236968649, 5.096181512];
r2err_spline = pchip(Re,run_2_err,Re_smooth);

%Run 2, by particle swarm
run_2_part_LESP = [-1,-1,0.01,-1];
run_2_part_err = [-1,-1,6.529043122,-1];

%======================== Run 3 =========================================%
%large del zone

%Plot LESP
figure;
plot(Re,run_2_LESP,'o',Re,run_2_part_LESP,'s',Re_smooth,r2L_spline);
axis([-inf,inf,0,inf]);
xlabel('Re');
ylabel('LESP');
legend('Run 2','Run 2, particle swarm','Run 2 spline');
title('LESP');

%Plot error
figure;
plot(Re,run_2_err,'o',Re,run_2_part_err,'s',Re_smooth,r2err_spline);
axis([-inf,inf,0,inf]);
xlabel('Re');
ylabel('Error');
title('Error');
legend('Run 2','Run 2, particle swarm','Run 2 spline');