classdef VortexVec
    %This class defines sets of vortecies
    properties          %These are public
        %This one is public so that the strengths of the bound vortecies
        %can be updated by the RotorObj
        vor_strength    %strengths of vortecies
    end
    properties (SetAccess = private)    %Can read them, but not write to them
        vor_loc         %position vectors of vortecies
        num_vor         %number of vortecies stored
        space_size      %size of space already allocated  
    end
    methods
        %Constructor function - makes an object of this
        function obj = VortexVec(pos_vec,strength_vec,initialize,num_init)
            %Initialize to given size
            if initialize
                obj.vor_loc = zeros(num_init,2);
                obj.vor_strength = zeros(num_init,1);
                obj.num_vor = 0;                %no currently stored vortecies
                obj.space_size = num_init;      %storage space
            else %Otherwise, start with the given vortecies
                %Checking sizes - want column vector/matrix
                if VorSizeCheck(pos_vec,strength_vec)
                    obj.vor_loc = pos_vec;
                    obj.vor_strength = strength_vec;
                    [num,~] = size(pos_vec);
                    obj.num_vor = num;
                    obj.space_size = num;
                end
            end
        end         %End constructor function
        
        %Get functions - allows external stuff to get private data
        %Note: maybe only give the full part of location and strength data
        %{
        function loc = get.vor_loc(obj)
            loc = obj.vor_loc;
            %loc = loc(1:num_vor, :);
        end
        
        function stren = get.vor_strength(obj)
            stren = obj.vor_strength;
            %stren = stren(1:num_vor);
        end
        
        function num = get.num_vor(obj)
            num = obj.num_vor;
        end
        
        function space = get.space_size(obj)
            space = obj.space_size;
        end         %End of the getters
        %}
        
        %Sets the strength of the vortecies to the given values
        %Note: only a subset of the vortecies can be specified
        function obj_out = VorStrengthSet(obj,stren,all_vor,loc)
            %all_vor indicates whether or not all of the vortecies will
            %have their strengths updated or not. If false, only the 
            %vortecies at the indecies in the loc vector will be updated
            if ~ isa(obj,'VortexVec')
                error('Input a VortexVec object');
            end
            
            %Initialize output to input
            obj_out = obj;
            
            if all_vor              %Update them all
                if SameSize(obj.vor_strength,stren)
                    obj_out.vor_strength = stren;
                else
                    error('Wrong number of vortex strengths given. If you want to only update a subset, set all_vor to false');
                end
            else                    %Update subset
                %First, make sure stuff isn't reset twice
                loc2 = unique(loc);
                if length(loc2) ~= length(loc)
                    error('Make the vector of the location of your new strengths unique');
                end
                
                if SameSize(stren,loc2)
                    stren_temp = obj.vor_strength;
                    for ii = 1:length(loc2)
                        %loc_replace = loc(ii)
                        stren_temp(loc2(ii)) = stren(ii);
                    end
                    obj_out.vor_strength = stren_temp;
                else
                    error('The number of vortex strengths is different than the number of locations');
                end
            end                     %End checking how many to update
        end                         %End strength resetting function
        
        %Adds (concatanates) the given vortecies to the stored vector
        function obj = VorAdd(obj,pos_vec,strength_vec,num_new_vor,concat)
            %Concatenate says whether to concatanate the new vortecies or
            %not. If yes, it just sticks them on the end, if no, it tries
            %to fit them in the preallocated space
            if concat       %Stick on end
                if VorSizeCheck(pos_vec,strength_vec)
                    obj.vor_loc = [obj.vor_loc; pos_vec];
                    obj.vor_strength = [obj.vor_strength; strength_vec];
                    [num,~] = size(pos_vec);
                    obj.num_vor = obj.space_size + num;     %everything to end of new vortecies is now data
                    obj.space_size = obj.space_size + num;
                end
            else                            %Fit into premade space, if possible
                if VorSizeCheck(pos_vec,strength_vec)
                    %[num,~] = size(pos_vec);    %number of new vortecies to fit
                    
                    %Temporary storage variables
                    loc_temp = obj.vor_loc;
                    stren_temp = obj.vor_strength;
                    num_fit = obj.space_size - obj.num_vor;
                    
                    if num_new_vor <= num_fit      %if it fits...
                        loc_temp((obj.num_vor+1):(obj.num_vor+num_new_vor),:) = pos_vec(1:num_new_vor,:);
                        stren_temp((obj.num_vor+1):(obj.num_vor+num_new_vor)) = strength_vec(1:num_new_vor);
                        %Note: the allocated space doesn't change
                    else                   %if it doesn't all fit...
                        %First fit in all that you can, the concatenate on
                        %the rest
                        loc_temp((obj.num_vor+1):end,:) = pos_vec(1:num_fit,:);
                        loc_temp = [loc_temp; pos_vec((num_fit+1):num_new_vor,:)];
                        
                        stren_temp((obj.num_vor+1):end) = strength_vec(1:num_fit);
                        stren_temp = [stren_temp; strength_vec((num_fit+1):num_new_vor)];
                        
                        obj.space_size = obj.space_size + num_new_vor - num_fit;
                    end
                    
                    obj.vor_loc = loc_temp;
                    obj.vor_strength = stren_temp;
                    obj.num_vor = obj.num_vor + num_new_vor;
                end         %End if for fitting in
            end             %End if concat
            
        end         %End concatonator
        
        %Overloads the plus function to make it concatanate them
        %Note: order is important!
        function obj_tot = plus(obj1, obj2)
            %Going to interpret the plus as VorAdd, without concatination
            %Picking the larger one as the default size
            if obj1.space_size >= obj2.space_size
                new_size = obj1.space_size;
            else
                new_size = obj2.space_size;
            end
            %Initializing to the new size
            obj_tot = VortexVec([0,0],0,true,new_size);
            
            %Inserting the data
            obj_tot = VorAdd(obj_tot,obj1.vor_loc,obj1.vor_strength,obj1.num_vor,false);
            obj_tot = VorAdd(obj_tot,obj2.vor_loc,obj2.vor_strength,obj2.num_vor,false);

        end         %end plus function
        
        %Deletes a vector of vortecies and shifts them back
        function obj1 = VorDelete(obj,delete_loc)
            %Making sure nothing is "deleted twice"
            delete_loc = unique(delete_loc);
            if obj.num_vor == 0
                %If there is nothing to delete, do nothing
                obj1 = obj;
            else
                %Making a temporary VortexVec object with the same size as the
                %original
                obj_temp = VortexVec([0,0],0,true,obj.num_vor);
                
                num_delete = length(delete_loc);
                new_num_vor = obj.num_vor - num_delete;
                
                %Creates temporary variables to mess with
                loc_temp = obj.vor_loc;
                stren_temp = obj.vor_strength;
                
                for ll = 1:num_delete
                    %Have to take into account shifting from previous deletions
                    delete_pt = delete_loc(ll) - ll + 1;
                    loc_temp(delete_pt:end, :) = [loc_temp((delete_pt+1):end, :); 0,0];
                    stren_temp(delete_pt:end) = [stren_temp(delete_pt+1:end); 0];
                end
                obj1 = VortexVec(loc_temp(1:new_num_vor,:),stren_temp(1:new_num_vor),false,0);
                %obj1.vor_loc = loc_temp;
                %obj1.vor_strength = stren_temp;
                
                obj1 = obj1 + obj_temp;
                if ~(obj1.num_vor == new_num_vor)
                    error('you did it wrong');
                end
            end
        end         %End vortex deletion function
        
        
        
        %==========================================================%
        %Finds the veloctiy vectors at the given locations
        %Note: do I want to add a direct-solver option?
        function vel_vec = VorVelocity(obj,other_obj,velocity_loc,self)
            if ~ isa(obj,'VortexVec')
                error('Input a VortexVec object');
            end
            
            %What to do with the other vortecies
            
            %If it's another VortexVec,add it on
            if isa(other_obj,'VortexVec')
                obj_temp = obj + other_obj;
            elseif islogical(other_obj)         %If it's false
                obj_temp = obj;                 %Initialize
            else
                error('Input another VortexVec object or false');
            end
            
            %check if self is true, if it isn't, add the velocity locations
            %to the list and return the velocity there. If it is true,
            %return the velocity at all the vortex locations in obj
            if ~ self          %find velocity at the given points
                [num,vel_w] = size(velocity_loc);
                if vel_w ~= 2
                    error('Input a matrix of position vectors');
                else
                    %Make an object of vortecies with zero strength and add
                    %it to the current object
                    %vel_pts = VortexVec(velocity_loc,zeros(num,1),false,0)
                    obj_temp = VorAdd(obj_temp,velocity_loc,zeros(num,1),num,false);
                end
            end
            
            %Getting the data containing parts
            pos = obj_temp.vor_loc;
            pos = pos(1:obj_temp.num_vor,:);
            
            stren = obj_temp.vor_strength;
            stren = stren(1:obj_temp.num_vor);
            
            %Finding with FMM
            U = zfmm2dpart(4,obj_temp.num_vor,pos',1i*stren',1,0,0);
            vel_vec = zeros(obj_temp.num_vor,2);
            vel_vec(:,1) = real(U.pot/(2*pi));
            vel_vec(:,2) = -imag(U.pot/(2*pi));
            
            if ~ self          %If want velocity at the given points
                vel_vec = vel_vec((obj_temp.num_vor-num+1):obj_temp.num_vor, :);
            else
                vel_vec = vel_vec(1:obj.num_vor,:);
            end
            
        end     %End finding velocity
        
        
        
        
        
        
        %=========================================================%
        %Wake rollup
        function wake_out = VorWakeRollup(wake_in,rotor,del_t)
            %Errors
            if ~ isa(wake_in,'VortexVec')
                error('Input a VortexVec object');
            elseif ~isa(rotor,'RotorObj')
                error('Input a RotorObj');
            else
                %Putting all vortecies into a temporary VortexVec object
                %Note: this depends on SingleBlade objects adding to
                %VortexVec objects (they currently due)
                num_init = 0;
                for ii = 1:rotor.num_blades
                    num_init = num_init + rotor.blades{ii}.N_sample;
                end
                
                other_vor = VortexVec([0,0],0,true,num_init);
                for ii = 1:rotor.num_blades
                    other_vor = other_vor + rotor.blades{ii};
                end
                
                vel = wake_in.VorVelocity(other_vor,0,true);
                wake_out = wake_in;
                
                %Move with velocity
                padding = zeros((wake_in.space_size - wake_in.num_vor),2);
                wake_out.vor_loc = wake_in.vor_loc + [vel;padding]*del_t;
            end                 %End error-checking if
        end                     %End VorWakeRollup
                    
        
    end         %End methods      
end