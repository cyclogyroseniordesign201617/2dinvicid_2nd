clear; clc; close all;
%A main function to test the effect of the LESP parameter.

N = 50;

LESP_min = 0.08;
LESP_max = 0.45;

LESP_in = LESP_min:(LESP_max-LESP_min)/(N-1):LESP_max;
length(LESP_in)

thrust_tot = zeros(N,2);
torque_tot = zeros(N,1);

for ii = 1:N
    close all;
    [thrust_vec,torque] = LESP_param_func(LESP_in(ii));
    
    thrust_tot(ii,:) = thrust_vec;
    torque_tot(ii) = torque;
end

thrust_mag = sqrt(thrust_tot(:,1).^2 + thrust_tot(:,2).^2);

figure;
plot(LESP_in,thrust_mag);
xlabel('LESP');
ylabel('Thrust, N');
    