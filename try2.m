clear all;
close all;
clc;

prompt = 'Number of sources, N, to be allocated in the unit box [def = 10,000]: ';
N = input(prompt);        

m = rand(N,3);            
% Sources matrix, m = [x, y, q].
n = [m(:,1:2),(1:N)'];
%[a,b,c]=FMM_DIR(m,n,1e-6);
[a,b]=FMM(m,n,1e-1);
source = m(:,1:2); source = source';
strength = m(:,3); strength = strength'+i*zeros(1,N);
tic
U=zfmm2dpart(4,N,source,strength,1,0,0);
toc
