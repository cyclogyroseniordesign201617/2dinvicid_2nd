function [not_enough_data,avg_mat,new_data] = MatMoveAvg(new_mat,old_data,avg_len)
%This function takes a matrix of new data and a 3D array of old data and
%provides a moving average of the data and a new data array

%The data arrays have the data matricies as the first 2 dimensions, and
%time as the 3rd dimension. The new data is the old data with the newest
%data stuck on the beginning and the oldest data removed.

%Type checking
if ~ismatrix(new_mat)
    error('You must input a new matrix');
end
if ~isscalar(avg_len)
    error('The averaging length must be a scalar');
end
[~,~,num_dat] = size(old_data);

%First, average the data
if num_dat < avg_len - 1
    %There is not enough data for the moving average
    not_enough_data = true;
    avg_mat = 0;
    
    new_data = new_mat;
    if ~isempty(old_data)
        new_data(:,:,2:num_dat+1) = old_data;
    end
else
    %average the data
    not_enough_data = false;
    avg_mat = new_mat;
    for ii = 1:avg_len-1
        avg_mat = avg_mat + old_data(:,:,ii);
    end
    avg_mat = avg_mat/avg_len;
    
    new_data = new_mat;
    new_data(:,:,2:avg_len-1) = old_data(:,:,1:avg_len-2);
end

end