clear; clc;
%Testing the SameSize program

a = zeros(3,1);
b = ones(3,2);
c = zeros(4,1);
d = rand(3,8);
e = rand(3,1);

[a,b] = SameSize(a,b,c,d,e,'diagnose')