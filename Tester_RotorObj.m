clear; clc;
%A program to test the RotorObj object

%From Tester_RotorPosition
len_blade = 5;
num_blade = 1;
num = 1;
pivLoc = @(time) [-time, -0*0.019*len_blade*sin(16*time)];
pivVel = @(time) [-1, -0*0.019*16*len_blade*cos(16*time)];
thetaFun = @(time) [5*ones(num_blade,1)];
thetaDotFun = @(time) 0*(180/pi)*ones(num_blade,1);
num_wake = 0;

rot_pos = RotorPosition(num,1,pivLoc,pivVel,thetaFun,thetaDotFun);


%Generate an empty set of other vortecies
other_vor = VortexVec([1,3; 6,7],[3;8],true,2);

%Starting the actual testing
N_pts = 5;
piv = 0;

rotor1 = RotorObj(N_pts,len_blade,piv,rot_pos,num_wake);

rotor2 = rotor1.RotorUpdate(other_vor,5,'steady_bernoulli');
blah = pi*rotor2.influence;
%rotor2.RHS_tot;
yada = rotor2.blades{1}.BladeLift('steady_bernoulli');
yada.C_normal
