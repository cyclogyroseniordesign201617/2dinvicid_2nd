function varargout = a_gui1(varargin)
% A_GUI1 MATLAB code for a_gui1.fig
%      A_GUI1, by itself, creates a new A_GUI1 or raises the existing
%      singleton*.
%
%      H = A_GUI1 returns the handle to a new A_GUI1 or the handle to
%      the existing singleton*.
%
%      A_GUI1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in A_GUI1.M with the given input arguments.
%
%      A_GUI1('Property','Value',...) creates a new A_GUI1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before a_gui1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to a_gui1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help a_gui1

% Last Modified by GUIDE v2.5 27-Mar-2017 22:16:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @a_gui1_OpeningFcn, ...
                   'gui_OutputFcn',  @a_gui1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before a_gui1 is made visible.
function a_gui1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to a_gui1 (see VARARGIN)

%Create tab group
handles.tgroup = uitabgroup('Parent', handles.figure1,'TabLocation', 'top');
handles.tab1 = uitab('Parent', handles.tgroup, 'Title', 'Run and Output');
handles.tab2 = uitab('Parent', handles.tgroup, 'Title', 'Physical Parameters');
handles.tab3 = uitab('Parent', handles.tgroup, 'Title', 'Numerical Parameters');

%Place panels into each tab
set(handles.P1,'Parent',handles.tab1)
set(handles.P2,'Parent',handles.tab2)
set(handles.P3,'Parent',handles.tab3)

%Reposition each panel to same location as panel 1
set(handles.P2,'position',get(handles.P1,'position'));
set(handles.P3,'position',get(handles.P1,'position'));


%Graph handles
%bound_pt_graph
%LESP_graph
%air_flow
%lift_and_torque


%The following is just an initial getting of all the default values. It is
%here because the other ones only activate after the user types something

%I think that instead each run function should just grab the ones it needs.
%Then they are always updated, and there aren't two names running around.
%This will just be a bank of the names
%{
%Number of blade points
handles.blade_num_pts = str2double(get(handles.num_points,'String'));

%Air density
handles.air_density = str2double(get(handles.density,'String'));

%Air viscosity
handles.air_viscosity = str2double(get(handles.viscosity,'String'));

%Radius
handles.rot_radius = str2double(get(handles.radius,'String'));

%Arm length
handles.arm_len = str2double(get(handles.arm_length,'String'));

%blade pivot distance (for 4-bar linkage)
handles.blade_piv_dis = str2double(get(handles.blade_piv_dist,'String'));

%Eccentricity (distance)
handles.ecc_len = str2double(get(handles.ecc_length,'String'));

%Angle to eccentricity point
handles.ecc_angle = str2double(get(handles.ecc_ang,'String'));

%Blade span
handles.blade_span = str2double(get(handles.span,'String'));

%Rotational speed
handles.rot_speed = str2double(get(handles.omega,'String'));

%LE wake separation distance
handles.LE_dis = str2double(get(handles.LE_distance,'String'));

%TE wake distance
handles.TE_dis = str2double(get(handles.TE_distance,'String'));

%piv (fraction of blade length to pivot location)
handles.piv = str2double(get(handles.piv,'String'));                %Need to rename this, so it doesn't conflict

%Blade length
handles.blade_len = str2double(get(handles.blade_len,'String'));

%Alpha value for bound point distribution
handles.alpha_val = str2double(get(handles.alpha_val,'String'));

%Beta value for bound point distribution
handles.beta_val = str2double(get(handles.beta_val,'String'));


%Wake setting
%Getting the selected values
contents = cellstr(get(handles.wake_setting,'String'));
wake_set = contents{get(handles.wake_setting,'Value')};

%Put a switch statement here to convert to the correct string
switch wake_set
    case 'LESP'
        %Do nothing
        wake_set = 'LESP';
    case 'Trailing Edge (Only)'
        wake_set = 'TE';
    case 'Both LE and TE'
        wake_set = 'LE';
    otherwise
        error('Input is an unknown wake setting. How did you do that?');
end

handles.wake_set = wake_set;



%Lift setting
%Getting the selected values
contents = cellstr(get(handles.lift_method,'String'));
lift_set = contents{get(handles.lift_method,'Value')};

%Put a switch statement here to convert to the correct string
switch lift_set
    case 'Unsteady Bernoulli'
        lift_set = 'unsteady_bernoulli';
    case 'Steady Bernoulli'
        lift_set = 'steady_bernoulli';
    case 'Uhlman'
        lift_set = 'uhlman';
    otherwise
        error('Input is an unknown lift setting. How did you do that?');
end

handles.lift_set = lift_set;


%Deletion zone size
handles.del_zone = str2double(get(handles.del_zone_size,'String'));

%Surface offset (for uhlman method)
handles.surf_offset = str2double(get(handles.surf_offset,'String'));

%Number of rotations (run duration)
handles.num_rot = str2double(get(handles.num_rotations,'String'));

%Time step
handles.del_t = str2double(get(handles.del_t_size,'String'));

%Number of blades
handles.num_blades = str2double(get(handles.num_blades,'String'));

%Number of rotations until data is taken for averaging
handles.time_start_rot = str2double(get(handles.avg_start_rot,'String'));


%Display air flow?
disp_air_flow = get(hObject,'Value');
if disp_air_flow == 1
    handles.disp_air_flow = true;
else
    handles.disp_air_flow = false;
end


%Display lift and torque?
disp_L_and_T = get(hObject,'Value');
if disp_L_and_T == 1
    handles.disp_L_and_T = true;
else
    handles.disp_L_and_T = false;
end


%User-supplied LESP value
handles.LESP_crit = str2double(get(handles.LESP_crit,'String'));

%}


% Choose default command line output for a_gui1
handles.output = hObject;

% Update handles structure
% Note: this must be done after modifying handles
guidata(hObject, handles);



% UIWAIT makes a_gui1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = a_gui1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton1.
function pushbutton1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over popupmenu1.
function popupmenu1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function num_points_Callback(hObject, eventdata, handles)
% hObject    handle to num_points (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of num_points as text
%        str2double(get(hObject,'String')) returns contents of num_points as a double




% --- Executes during object creation, after setting all properties.
function num_points_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_points (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function density_Callback(hObject, eventdata, handles)
% hObject    handle to density (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of density as text
%        str2double(get(hObject,'String')) returns contents of density as a double




% --- Executes during object creation, after setting all properties.
function density_CreateFcn(hObject, eventdata, handles)
% hObject    handle to density (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function viscosity_Callback(hObject, eventdata, handles)
% hObject    handle to viscosity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of viscosity as text
%        str2double(get(hObject,'String')) returns contents of viscosity as a double




% --- Executes during object creation, after setting all properties.
function viscosity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to viscosity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function radius_Callback(hObject, eventdata, handles)
% hObject    handle to radius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of radius as text
%        str2double(get(hObject,'String')) returns contents of radius as a double




% --- Executes during object creation, after setting all properties.
function radius_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function arm_length_Callback(hObject, eventdata, handles)
% hObject    handle to arm_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of arm_length as text
%        str2double(get(hObject,'String')) returns contents of arm_length as a double




% --- Executes during object creation, after setting all properties.
function arm_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to arm_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function blade_piv_dist_Callback(hObject, eventdata, handles)
% hObject    handle to blade_piv_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of blade_piv_dist as text
%        str2double(get(hObject,'String')) returns contents of blade_piv_dist as a double




% --- Executes during object creation, after setting all properties.
function blade_piv_dist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blade_piv_dist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecc_length_Callback(hObject, eventdata, handles)
% hObject    handle to ecc_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecc_length as text
%        str2double(get(hObject,'String')) returns contents of ecc_length as a double



% --- Executes during object creation, after setting all properties.
function ecc_length_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecc_length (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecc_ang_Callback(hObject, eventdata, handles)
% hObject    handle to ecc_ang (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecc_ang as text
%        str2double(get(hObject,'String')) returns contents of ecc_ang as a double



% --- Executes during object creation, after setting all properties.
function ecc_ang_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecc_ang (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function span_Callback(hObject, eventdata, handles)
% hObject    handle to span (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of span as text
%        str2double(get(hObject,'String')) returns contents of span as a double



% --- Executes during object creation, after setting all properties.
function span_CreateFcn(hObject, eventdata, handles)
% hObject    handle to span (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function omega_Callback(hObject, eventdata, handles)
% hObject    handle to omega (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of omega as text
%        str2double(get(hObject,'String')) returns contents of omega as a double



% --- Executes during object creation, after setting all properties.
function omega_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LE_distance_Callback(hObject, eventdata, handles)
% hObject    handle to LE_distance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LE_distance as text
%        str2double(get(hObject,'String')) returns contents of LE_distance as a double



% --- Executes during object creation, after setting all properties.
function LE_distance_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LE_distance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TE_distance_Callback(hObject, eventdata, handles)
% hObject    handle to TE_distance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TE_distance as text
%        str2double(get(hObject,'String')) returns contents of TE_distance as a double



% --- Executes during object creation, after setting all properties.
function TE_distance_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TE_distance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function piv_Callback(hObject, eventdata, handles)
% hObject    handle to piv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of piv as text
%        str2double(get(hObject,'String')) returns contents of piv as a double



% --- Executes during object creation, after setting all properties.
function piv_CreateFcn(hObject, eventdata, handles)
% hObject    handle to piv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function blade_len_Callback(hObject, eventdata, handles)
% hObject    handle to blade_len (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of blade_len as text
%        str2double(get(hObject,'String')) returns contents of blade_len as a double



% --- Executes during object creation, after setting all properties.
function blade_len_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blade_len (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alpha_val_Callback(hObject, eventdata, handles)
% hObject    handle to alpha_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alpha_val as text
%        str2double(get(hObject,'String')) returns contents of alpha_val as a double




% --- Executes during object creation, after setting all properties.
function alpha_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alpha_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function beta_val_Callback(hObject, eventdata, handles)
% hObject    handle to beta_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of beta_val as text
%        str2double(get(hObject,'String')) returns contents of beta_val as a double



% --- Executes during object creation, after setting all properties.
function beta_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to beta_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in wake_setting.
function wake_setting_Callback(hObject, eventdata, handles)
% hObject    handle to wake_setting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns wake_setting contents as cell array
%        contents{get(hObject,'Value')} returns selected item from wake_setting


        
        


% --- Executes during object creation, after setting all properties.
function wake_setting_CreateFcn(hObject, eventdata, handles)
% hObject    handle to wake_setting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lift_method.
function lift_method_Callback(hObject, eventdata, handles)
% hObject    handle to lift_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lift_method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lift_method



% --- Executes during object creation, after setting all properties.
function lift_method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lift_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in calc_LESP.
function calc_LESP_Callback(hObject, eventdata, handles)
% hObject    handle to calc_LESP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Copied from RotorOptions
%Find Reynold's number
rho =   str2double(get(handles.density,'String'));
R =     str2double(get(handles.radius,'String'));
mu =    str2double(get(handles.viscosity,'String'));
L =     str2double(get(handles.blade_len,'String'));
omega = str2double(get(handles.omega,'String'));

omega = omega*(2*pi/60);    %Convert to rads/sec

Re = (rho*omega*R*L)/mu;

%Reading the csv file in the range, starting at index zero:
read_range = [1, 0, inf, 1];
LESP_data = csvread('LESP_crit.csv',read_range(1),read_range(2));

%Could use cubic spline, but pchip seems better for
%non-oscillatory data.
%May need to use a polynomial fit if least-squares is needed
%LESP_crit_val = spline(LESP_data(:,1),LESP_data(:,2),Re);
LESP_crit_val = pchip(LESP_data(:,1),LESP_data(:,2),Re);

%Set it to the text box
set(handles.LESP_crit, 'String', num2str(LESP_crit_val));

%Stuff for plotting
Re_min = min(LESP_data(:,1));
Re_max = max(LESP_data(:,1));

%Making the spline curve
spline_x = Re_min-0.1*Re_max:(Re_max - Re_min)/100:1.1*Re_max;
spline_y = pchip(LESP_data(:,1),LESP_data(:,2),spline_x);

%Plot it
plot(handles.LESP_graph, Re,LESP_crit_val,'rx', LESP_data(:,1),LESP_data(:,2),'go', spline_x,spline_y,'k:');

title(handles.LESP_graph,'Interpolating LESP Value');
xlabel(handles.LESP_graph,'Reynolds Number');
ylabel(handles.LESP_graph,'LESP');
grid(handles.LESP_graph,'on');




function del_zone_size_Callback(hObject, eventdata, handles)
% hObject    handle to del_zone_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of del_zone_size as text
%        str2double(get(hObject,'String')) returns contents of del_zone_size as a double



% --- Executes during object creation, after setting all properties.
function del_zone_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to del_zone_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function surf_offset_Callback(hObject, eventdata, handles)
% hObject    handle to surf_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of surf_offset as text
%        str2double(get(hObject,'String')) returns contents of surf_offset as a double



% --- Executes during object creation, after setting all properties.
function surf_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to surf_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in show_distribution.
function show_distribution_Callback(hObject, eventdata, handles)
% hObject    handle to show_distribution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%This shows the pdf of the beta distribution and the points on the blade.
%The x-axis is normalized to the blade length

%Note: this is largely copied from the SingleBlade constructor
N = str2double(get(handles.num_points,'String'));
even_dis = 0:(1/N):1;

alpha_val = str2double(get(handles.alpha_val,'String'));
beta_val = str2double(get(handles.beta_val,'String'));
x = betainc(even_dis,alpha_val,beta_val);     %Cumulative beta distribution

%Note: using the sample points to display
dx = x(2:end)-x(1:end-1);
sample_pts = x(2:N+1)-0.25*dx;

%Wake setting
%Getting the selected values
contents = cellstr(get(handles.wake_setting,'String'));
wake_set = contents{get(handles.wake_setting,'Value')};
%Put a switch statement here to convert to the correct string
switch wake_set
    case 'LESP'
        %Do nothing
        wake_set = 'LESP';
    case 'Trailing Edge (Only)'
        wake_set = 'TE';
    case 'Both LE and TE'
        wake_set = 'LE';
    otherwise
        error('Input is an unknown wake setting. How did you do that?');
end

TE_dis = str2double(get(handles.TE_distance,'String'));
LE_dis = str2double(get(handles.LE_distance,'String'));

%Make a switch statement to determine how far away the wake points go
%(LE_distance and TE_distance don't work without LESP)
%Note: the zeros are for hight on the graph
switch wake_set
    case 'LESP'
        %TE at 1, LE at 0
        wake_pts = [1 + TE_dis/N, 0; -LE_dis/N, 0];
    case 'TE'
        wake_pts = [1+ 0.25/N, 0];
    case 'LE'
        wake_pts = [1+ 0.25/N, 0; -0.25/N, 0];
    otherwise
        error('Unrecognized wake setting');
end

%Plotting
%x_pdf = 0:0.01:1;
%y_pdf = betapdf(x_pdf,alpha_val,beta_val);

%Plotting to air_flow area
%plot(handles.bound_pt_graph,sample_pts,zeros(1,length(sample_pts)),'go',...
%    wake_pts(:,1),wake_pts(:,2),'r.', x_pdf,y_pdf,'k:', sample_pts,N*dx,'gx');

plot(handles.bound_pt_graph,sample_pts,zeros(1,length(sample_pts)),'go',...
    wake_pts(:,1),wake_pts(:,2),'r*', sample_pts,dx,'kx:');
title(handles.bound_pt_graph,'Bound Point Distribution');
xlabel(handles.bound_pt_graph,'x/Chord');
ylabel(handles.bound_pt_graph,'Interpoint Distance/Chord');
grid(handles.bound_pt_graph,'on');






% --- Executes on button press in run_sim.
function run_sim_Callback(hObject, eventdata, handles)
% hObject    handle to run_sim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%This is the big one
%Setting up rotor options
%Number of blades
num_blade = str2double(get(handles.num_blades,'String'));
rotor_op = RotorOptions(num_blade);

%=============== Getting all of the data ========================%
%Number of blade points
rotor_op.num_pt_vec = ones(num_blade,1)*str2double(get(handles.num_points,'String'));
%Air density
rotor_op.density = str2double(get(handles.density,'String'));
%Air viscosity
rotor_op.viscosity = str2double(get(handles.viscosity,'String'));
%Radius
rotor_op.radius = ones(num_blade,1)*str2double(get(handles.radius,'String'));
%Arm length
rotor_op.arm_len = ones(num_blade,1)*str2double(get(handles.arm_length,'String'));
%blade pivot distance (for 4-bar linkage)
rotor_op.blade_piv_dis = ones(num_blade,1)*str2double(get(handles.blade_piv_dist,'String'));
%Eccentricity (distance)
rotor_op.ecc_len = ones(num_blade,1)*str2double(get(handles.ecc_length,'String'));
%Angle to eccentricity point
rotor_op.ecc_ang = (pi/180)*ones(num_blade,1)*str2double(get(handles.ecc_ang,'String'));    %Convert to radians
%Blade span
rotor_op.span = str2double(get(handles.span,'String'));
%Rotational speed - convert to rad/sec
rotor_op.omega = (2*pi/60)*str2double(get(handles.omega,'String'));
%LE wake separation distance
rotor_op.LE_dis = ones(num_blade,1)*str2double(get(handles.LE_distance,'String'));
%TE wake distance
rotor_op.TE_dis = ones(num_blade,1)*str2double(get(handles.TE_distance,'String'));
%piv (fraction of blade length to pivot location)
rotor_op.piv = ones(num_blade,1)*str2double(get(handles.piv,'String'));
%Blade length
rotor_op.len_blade = ones(num_blade,1)*str2double(get(handles.blade_len,'String'));
%Alpha value for bound point distribution
rotor_op.alpha_vec = ones(num_blade,1)*str2double(get(handles.alpha_val,'String'));
%Beta value for bound point distribution
rotor_op.beta_vec = ones(num_blade,1)*str2double(get(handles.beta_val,'String'));
%User-supplied LESP value
rotor_op.LESP_crit = ones(num_blade,1)*str2double(get(handles.LESP_crit,'String'));     %Here is where to change if want to use RotorOptions version


%Wake setting
%Getting the selected values
contents = cellstr(get(handles.wake_setting,'String'));
wake_set = contents{get(handles.wake_setting,'Value')};
%Put a switch statement here to convert to the correct string
switch wake_set
    case 'LESP'
        %Do nothing
        wake_set = 'LESP';
    case 'Trailing Edge (Only)'
        wake_set = 'TE';
    case 'Both LE and TE'
        wake_set = 'LE';
    otherwise
        error('Input is an unknown wake setting. How did you do that?');
end
rotor_op.wake_setting = wake_set;


%Lift setting
%Getting the selected values
contents = cellstr(get(handles.lift_method,'String'));
lift_set = contents{get(handles.lift_method,'Value')};
%Put a switch statement here to convert to the correct string
switch lift_set
    case 'Unsteady Bernoulli'
        lift_set = 'unsteady_bernoulli';
    case 'Steady Bernoulli'
        lift_set = 'steady_bernoulli';
    case 'Uhlman'
        lift_set = 'uhlman';
    otherwise
        error('Input is an unknown lift setting. How did you do that?');
end
rotor_op.lift_method = lift_set;


%Deletion zone size
rotor_op.del_zone_size = ones(num_blade,1)*str2double(get(handles.del_zone_size,'String'));
%Surface offset (for uhlman method)
rotor_op.surf_offset = ones(num_blade,1)*str2double(get(handles.surf_offset,'String'));
%Time to end
time_f = (2*pi/rotor_op.omega)*str2double(get(handles.num_rotations,'String'));
%Time step
del_t = str2double(get(handles.del_t_size,'String'));
%When to start taking data for averaging
time_dat_start = (2*pi/rotor_op.omega)*str2double(get(handles.avg_start_rot,'String'));
num_dat_start = ceil(time_dat_start/del_t);
num_t_step = ceil(time_f/del_t);

%Display air flow?
getter_disp_air_flow = get(handles.air_flow_disp,'Value');
if getter_disp_air_flow == 1
    disp_air_flow = 'on';
else
    disp_air_flow = 'off';
end

%Display lift and torque?
getter_disp_L_and_T = get(handles.L_T_disp,'Value');
if getter_disp_L_and_T == 1
    disp_L_and_T = 'on';
else
    disp_L_and_T = 'off';
end



%================ Initializing Stuff =============================%
%For display
flow_axis = [-0.5,0.5,-0.7,0.3];                                                            %Here is the spot to resize the figure
c_p_axis = [0,1,-2,2];                                                                      %Here is the spot to resize the figure
name = get(handles.file_name,'String');
%name = 'first_unsteady_test';

%Symmetric pitching
[pivLocFun,pivVelFun,thetaFunSym,thetaDotFunSym] = RotorBladePitchingSym(num_blade,rotor_op);
pivLoc = @(time) pivLocFun(time);
pivVel = @(time) pivVelFun(time);
thetaFun = @(time) thetaFunSym(time);
thetaDotFun = @(time) thetaDotFunSym(time);

time_init = 0;
rot_pos = RotorPosition(num_blade,time_init,pivLoc,pivVel,thetaFun,thetaDotFun);
rotor = RotorObj(rot_pos,rotor_op);

%Initializing the wake
wake = VortexVec([0,0],0,true,1);
%Initializing old wake to the current wake
old_wake = wake;

plot_obj = DispObj(rotor,wake,flow_axis,c_p_axis,name,num_t_step,true,...
    'C_p','off','C_L','off','stream_func','off','vel_mag','off','air_flow',disp_air_flow,'L_D_plot',disp_L_and_T);

%Stick in appropriate handles to the DispObj
plot_obj.flow_fig = handles.air_flow;
plot_obj.L_D_hand = handles.lift_and_torque;
tic;
%Looping through time
for new_time = 0:del_t:time_f
    [rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,rotor_op);
    old_wake = wake;
    wake = VorWakeRollup(wake,rotor,del_t);
    if rotor_op.wake_setting == 3
        rotor = LastWakePosUpdater(rotor,wake);
    end
    plot_obj = plot_obj.StoreData(rotor,rotor_op,wake,false,new_time);
end

%Close the movies
plot_obj.MovieClose;

%Show the lift
plot_obj = plot_obj.LiftPlot(rotor_op,num_dat_start);


%Spit out average data to text boxes
thrust_mag_avg = sqrt( sum( plot_obj.thrust_avg.^2 ) );
set(handles.avg_lift_out, 'String', num2str(thrust_mag_avg));
set(handles.avg_torque_out, 'String', num2str(plot_obj.torque_avg));

time = toc;
disp('time');
disp(time);









function num_rotations_Callback(hObject, eventdata, handles)
% hObject    handle to num_rotations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of num_rotations as text
%        str2double(get(hObject,'String')) returns contents of num_rotations as a double



% --- Executes during object creation, after setting all properties.
function num_rotations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_rotations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function del_t_size_Callback(hObject, eventdata, handles)
% hObject    handle to del_t_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of del_t_size as text
%        str2double(get(hObject,'String')) returns contents of del_t_size as a double



% --- Executes during object creation, after setting all properties.
function del_t_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to del_t_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function num_blades_Callback(hObject, eventdata, handles)
% hObject    handle to num_blades (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of num_blades as text
%        str2double(get(hObject,'String')) returns contents of num_blades as a double



% --- Executes during object creation, after setting all properties.
function num_blades_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_blades (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function avg_start_rot_Callback(hObject, eventdata, handles)
% hObject    handle to avg_start_rot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of avg_start_rot as text
%        str2double(get(hObject,'String')) returns contents of avg_start_rot as a double



% --- Executes during object creation, after setting all properties.
function avg_start_rot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to avg_start_rot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function avg_lift_out_Callback(hObject, eventdata, handles)
% hObject    handle to avg_lift_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of avg_lift_out as text
%        str2double(get(hObject,'String')) returns contents of avg_lift_out as a double


% --- Executes during object creation, after setting all properties.
function avg_lift_out_CreateFcn(hObject, eventdata, handles)
% hObject    handle to avg_lift_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function avg_torque_out_Callback(hObject, eventdata, handles)
% hObject    handle to avg_torque_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of avg_torque_out as text
%        str2double(get(hObject,'String')) returns contents of avg_torque_out as a double


% --- Executes during object creation, after setting all properties.
function avg_torque_out_CreateFcn(hObject, eventdata, handles)
% hObject    handle to avg_torque_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in air_flow_disp.
function air_flow_disp_Callback(hObject, eventdata, handles)
% hObject    handle to air_flow_disp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of air_flow_disp



% --- Executes on button press in L_T_disp.
function L_T_disp_Callback(hObject, eventdata, handles)
% hObject    handle to L_T_disp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of L_T_disp




function LESP_crit_Callback(hObject, eventdata, handles)
% hObject    handle to LESP_crit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LESP_crit as text
%        str2double(get(hObject,'String')) returns contents of LESP_crit as a double



% --- Executes during object creation, after setting all properties.
function LESP_crit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LESP_crit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function air_flow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to air_flow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate air_flow



function file_name_Callback(hObject, eventdata, handles)
% hObject    handle to file_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of file_name as text
%        str2double(get(hObject,'String')) returns contents of file_name as a double


% --- Executes during object creation, after setting all properties.
function file_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to file_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
