# 2D Invicid Cyclogyro Simulation #

This is a two-dimensional invicid discrete vortex method simulation of a cyclogyro. It is the creation of Cedarville University's 2016-2017 Cyclogyro senior design team.

### A second go-around at the 2D invicid program - now object oriented! ###

* This program models the blades of a cyclogyro as flat plates using the discrete vortex method
     * [What is a cyclogyro?](https://en.wikipedia.org/wiki/Cyclogyro)
* This program is constantly being updated, and so is not ready for download yet. Consider it version 0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This program requires Matlab, including a few toolboxes
     1. Optimization toolbox

### Contribution guidelines ###

Contribution to this project is currently closed. It will be further developed by the future Cedarville Cyclogyro senior design team.

Someday there will be a document which will describe the code, its components, and how everything works together. Until then, you will have to read the code.

### Who do I talk to? ###

For any questions about this code, email [aellicott@cedarville.edu](aellicott@cedarville.edu)