clear; clc;
%Velocity tester program: tests the usage of the FMM code vs. direct solve
%for sample points (without vortecies)

format long

S_N = 5;
%{
pos = [0,0; 1,0; -1,0; 0,1; 0,-1];
strength = zeros(5,1);
strength(1) = 2*pi;
%}
s_pos = 5*rand(S_N,2);
s_strength = rand(S_N,1);

M = 5;
sample_pos = 5*rand(M,2);
sample_stren = zeros(M,1);

%Putting them together
N = S_N + M;
pos = [s_pos;sample_pos];
strength = [s_strength;sample_stren];


%Finding with FMM
U = zfmm2dpart(4,N,pos',1i*strength',1,0,0);
vel = zeros(N,2);
vel(:,1) = real(U.pot/(2*pi));
vel(:,2) = -imag(U.pot/(2*pi));
vel

%Direct solver
vel_direct = zeros(N,2);
for ii = 1:N
    for jj = 1:N
        if ii ~= jj
            [u,w] = VorInfluence2D(pos(ii,1), pos(ii,2), pos(jj,1), pos(jj,2));
            vel_direct(ii,:) = vel_direct(ii,:) + strength(jj)*[u,w];
        end
    end
end
vel_direct
err = (vel - vel_direct)*1E06

%direct solve 2
sample_vel = zeros(M,2);
for ii = 1:M
    for jj = 1:S_N
        [u,w] = VorInfluence2D(sample_pos(ii,1), sample_pos(ii,2), s_pos(jj,1), s_pos(jj,2));
        sample_vel(ii,:) = sample_vel(ii,:) + s_strength(jj)*[u,w];
    end
end

vortecies = VortexVec(s_pos,s_strength,false,0)
fmm_sample_vel = vortecies.VorVelocity(false,sample_pos,false);

err2 = fmm_sample_vel - sample_vel
err3 = fmm_sample_vel - vel(S_N+1:end,:)