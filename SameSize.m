function [ok,varargout] = SameSize(in1,varargin)

%A function which determines if things are the same height

num_extra = length(varargin);
if strcmp('diagnose',varargin{end})
    %Provide extra output to help diagnose the problem
    diagnose = true;
    num_extra = num_extra - 1;
else
    diagnose = false;
end

if num_extra == 0           %Trivial case
    ok = true;
else                        %Base case
    [m1,n1] = size(in1);
    [m2,n2] = size(varargin{1});
    if m1 == m2
        ok = true;
        if diagnose
            varargout{1} = [0];
        end
    else
        ok = false;
        if diagnose
            varargout{1} = [1];
        end
    end
end

if num_extra > 1        %Recursion
    if diagnose
        [same_size,vec_out] = SameSize(varargin{1},varargin{2:end});
        varargout{1} = [varargout{1},vec_out];
        ok = ok && same_size;
    else
        ok = ok & SameSize(varargin{1},varargin{2:end});
    end  
end
end