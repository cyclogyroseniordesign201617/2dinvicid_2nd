clear; clc;
%A main program to optimize the LESP parameter

problem = createOptimProblem('fmincon','objective',@LinearLESPFunc,'x0',0.11,'lb',0,'ub',1);
gs = GlobalSearch;

[xg,fg,flg,og] = run(gs,problem);