classdef BladeLiftObj
    %A class to store the lift and C_p data for a SingleBlade
    properties (SetAccess = protected)
        C_p_upper           %A vector of the C_p values on the upper part of the blade
        C_p_lower           %Same thing, lower part
        C_normal            %The lift force coefficient (global Z)
        C_drag              %The drag force coefficient (global X)
        lift_method         %The method used to get the lift
        lift                %The lift vector of the blade 
        
    end
    methods
        %Constructor method: shouldn't be used
        function obj = BladeLift(C_p_up,C_p_low,C_norm)
            warning('You really should not use this constructor');
            if SameSize(C_p_up,C_p_low)
                if isscalar(C_norm)
                    obj.C_p_upper = C_p_up;
                    obj.C_p_lower = C_p_low;
                    obj.C_normal = C_norm;
                else
                    error('C_norm should be a scalar');
                end
            else
                error('C_p_up and C_p_low should be the same size');
            end
        end
    end             %End methods
end                 %End class
        