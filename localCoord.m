function local_coord = localCoord(piv_loc,theta,abs_coord)

[N,~] = size(abs_coord);

coord_m = [cosd(theta), sind(theta); -sind(theta), cosd(theta)];

local_coord = coord_m*(abs_coord'-[piv_loc(1)*ones(1,N); piv_loc(2)*ones(1,N)]);
local_coord = local_coord';

end
