clear; clc; close all;
%A main program to run the steady-state case
format short;


%========== First, setup rotor position ========================%
num_blade = 6;                                          %Number of blades
num_pts = 10*ones(num_blade,1);                         %Number of sample points in each blade
len_blade = 0.05*ones(num_blade,1);                     %Length of each blade, m

%================= Initialize RotorOptions object ================%
rotor_op = RotorOptions(num_blade);
rotor_op.num_pt_vec = num_pts;

%Note: I think the following are all defaults. Most, but not all of the
%options that can be modified are listed here for your convience. For
%example, the CSV file name where you get the LESP_crit data is not here.
%However, I doubt you will modify that

rotor_op.len_blade = len_blade;                         %Lenth of the blade
rotor_op.piv = 0.25*ones(num_blade,1);                  %Location of pivot point as fraction of chord from leading edge
rotor_op.wake_setting = 'LESP';                         %Method to determine separation
%rotor_op.LESP_crit = 0.2*ones(num_blade,1);            %Set LESP_crit manually
rotor_op.lift_method = 'unsteady_bernoulli';            %Method of calculating lift
rotor_op.del_zone_size = 0.525*(len_blade/num_pts)*ones(num_blade,1);   %Size of zone (above and below blade) in which vortecies are deleted, m
%rotor_op.surf_offset = 0.25*(len_blade/num_pts)*ones(num_blade,1);     %Surface offset for Uhlman lift method. Legacy
rotor_op.alpha_vec = 4*ones(num_blade,1);               %Alpha parameter for beta distribution of sample points
rotor_op.beta_vec = 6*ones(num_blade,1);                %Beta parameter for beta distribution
rotor_op.LE_dis = 0.1*ones(num_blade,1);                %Distance from leading edge of the wake vortex release point (fraction of length/num_points)
rotor_op.TE_dis = 0.8*ones(num_blade,1);                %Distance from trailing edge ...

%Physical rotor parameters
rotor_op.radius = 0.1*ones(num_blade,1);                %Radius of rotor, m
rotor_op.arm_len = 0.1028*ones(num_blade,1);            %Length of pivoting arm, m
rotor_op.blade_piv_dis = 0.03*ones(num_blade,1);        %Distance between pivot points on the blade connector, m
rotor_op.ecc_len = 0.01981*ones(num_blade,1);           %Length of eccentricity distance between rotation points of connecting arms
rotor_op.ecc_ang = 0.2698*ones(num_blade,1);            %Angle of pivoter, radians
rotor_op.omega = 1200*(2*pi/60);                        %Rotational speed of rotor, rad/sec
rotor_op.span = 0.20;                                   %Span of blades, m
rotor_op = rotor_op.LESPFromCSV;                        %Find correct LESP from Reynold's number



%====================== Set up time ==============================%
time_init = 0;
time_f = 5*(2*pi/rotor_op.omega);                       %Time simulation will finish (calculated from # of rotations), sec
del_t = 0.0001;                                         %Time step size, sec
num_t_step = ceil(time_f/del_t);                        %Number of time steps

time_dat_start = 3*(2*pi/rotor_op.omega);               %Time to start taking (and averaging) data, sec
num_dat_start = ceil(time_dat_start/del_t);             %Number of time steps

%For display
flow_axis = [-0.5,0.5,-0.7,0.3];                        %Size of graph for flow, and also the contour and vector plots
c_p_axis = [0,1,-2,2];                                  %Size of coefficient of pressure plot (for Uhlman lift)
name = 'regular run';                                   %Base name to save videos & figures (it automatically adds name of what the video is)



%Initializing everything
%Symmetric pitching
[pivLocFun,pivVelFun,thetaFunSym,thetaDotFunSym] = RotorBladePitchingSym(num_blade,rotor_op);
pivLoc = @(time) pivLocFun(time);
pivVel = @(time) pivVelFun(time);
thetaFun = @(time) thetaFunSym(time);
thetaDotFun = @(time) thetaDotFunSym(time);


%This is another way to make the movement functions. This one is linear,
%or could be oscillatory. This has 2 blades
%{
pivLoc = @(time) [-time, 0*0.019*len_blade(1)*sin(16*time)+3; -time, 0*0.019*len_blade(2)*sin(16*time)-3];
pivVel = @(time) [-1, 0*0.019*16*len_blade(1)*cos(16*time);-1, 0*0.019*16*len_blade(2)*cos(16*time)];
thetaFun = @(time) [-5;-45];
thetaDotFun = @(time) -0*ones(num_blade,1);
%}

%Initialize position and RotorObj
rot_pos = RotorPosition(num_blade,time_init,pivLoc,pivVel,thetaFun,thetaDotFun);
rotor = RotorObj(rot_pos,rotor_op);

%Initializing the wake
wake = VortexVec([0,0],0,true,1);
%Initializing old wake to the current wake
old_wake = wake;

%========================= Initialize Display Stuff ====================%
%This is where you turn displays off. All displays default to on. Leave C_p
%off - it is for the Uhlman lift method
plot_obj = DispObj(rotor,wake,flow_axis,c_p_axis,name,num_t_step,false,'C_p','off','C_L','off');
plot_obj.grid_res = [60,60];                %Sampling grid for streamfunction, etc. movies
plot_obj.roll_avg_len = 10;                 %Number of time steps (into the past) to average over for movies



%====================== Run the Simulation ============================%
%Looping through time using 1st orde time stepping
for new_time = 0:del_t:time_f
    [rotor,wake] = rotor.RotorUpdate(wake,old_wake,new_time,del_t,rotor_op);
    old_wake = wake;
    wake = VorWakeRollup(wake,rotor,del_t);
    %If using LESP_wake (you are)
    if rotor_op.wake_setting == 3
        rotor = LastWakePosUpdater(rotor,wake);
    end
    plot_obj = plot_obj.StoreData(rotor,rotor_op,wake,false,new_time);
end

%Close the movies
plot_obj.MovieClose;

%Display the stuff
plot_obj = plot_obj.LiftPlot(rotor_op,num_dat_start);


%This stuff is legacy - turn it on if you want to compare to impulsive
%start for 1 blade in linear motion at 5 degree angle of attack
%{
t = 0:del_t:time_f;
%Data from 
l_data = [0.0111857,0.495495;
          0.117450,0.522523;
          0.251678,0.551802;
          0.413870,0.583333;
          0.508949,0.599099;
          0.631991,0.621622;
          0.782998,0.641892;
          0.939597,0.664414;
          1.07383,0.677928;
          1.21365,0.695946;
          1.38143,0.716216;
          1.61074,0.736486;
          1.83445,0.752252;
          2.04139,0.770270;
          2.30425,0.786036;
          2.53915,0.801802;
          2.86353,0.817568;
          3.09284,0.831081;
          3.39485,0.846847;
          3.66890,0.860360;
          3.93736,0.867117;
          4.15548,0.871622;
          4.41834,0.873874;
          4.68121,0.880631;
          4.90492,0.887387;
          4.99441,0.887387];

figure;
plot(l_data(:,1),l_data(:,2),'k',t(1:25:end),plot_obj.C_L_data(1,1:25:end)/0.5476,'ko');
legend('Wagner','DVM');
xlabel('V_{inf}t/c');
ylabel('L(t)/L_{inf}');
grid on;
axis([0,5,0,1.5]);
%}
