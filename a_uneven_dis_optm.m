clear; clc;

%A main program to optimize the uneven distribution parameters

%param_vec = [alpha,beta,LE_dis,TE_dis,del_zone_size];
%Initialize to even distribution - NOT
param_vec = [2.7,3,2,1,0.5];
%blah = uneven_dis_param_func(param_vec)
options = optimoptions(@fmincon,'Display','iter');
%param_vec_new = fminunc(@uneven_dis_param_func,param_vec);

%x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
lb = [0.01,0.01,0.01,0.01,0.01];
ub = [Inf,Inf,Inf,Inf,Inf];
param_vec_new = fmincon(@uneven_dis_param_func,param_vec,[],[],[],[],lb,ub,[],options);